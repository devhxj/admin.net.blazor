<div align="center"><h1 align="center">Admin.NET.Blazor</h1></div>
<div align="center"><h3 align="center">前后端分离架构，开箱即用，紧随前沿技术</h3></div>

<div align="center">

![今日诗词](https://v2.jinrishici.com/one.svg?font-size=20&spacing=2&color=Chocolate)
</div>

### 🍟 概述

* 基于.NET 6实现的通用权限管理平台（RBAC模式）。前后端分离模式，开箱即用。
* 前端基于MASA.Blazor/BootstrapBlazor组件，前后端分析模式。
* 后台完全基于Admin.NET(EFCore版本)，Furion框架，EFcore、多租户、分库读写分离、缓存、数据校验、鉴权、动态API、gRPC等众多黑科技集一身。
* 模块化架构设计，层次清晰，业务层推荐写到单独模块，框架升级不影响业务!
* 核心模块包括：用户、角色、职位、组织机构、菜单、字典、日志、多应用管理、文件管理、定时任务等功能。
* 代码量少、通俗易懂、功能强大、易扩展，轻松开发从现在开始！

```
如果对您有帮助，您可以点右上角 “Star” 收藏一下 ，获取第一时间更新，谢谢！
```

### 😎 原始版本（基于EFCore）

【Admin.NET】

- 👉 原始版本：[https://gitee.com/zuohuaijun/Admin.NET](https://gitee.com/zuohuaijun/Admin.NET)

### ⚡ 更新日志

- 提交基本功能框架代码

### 🍄 快速启动

需要安装：.NET 6.0.2 SDK、VS2022（最新版）

* 启动：打开src/Admin.NET.sln解决方案，直接运行即可启动（数据库默认SQLite）
* 访问：Admin.NET.Web.Hybrid 的appsettings中配置ExecuteMode。默认ServerWasm，即使用Server预加载wasm
* API：`https://localhost:9000`
* APP：Admin.NET.Web.Hybrid中配置端口，默认https://localhost:7275/ （默认为Server模式）
* 注意：前后端分离模式，必须同时启动Admin.NET.Web.Entry(api)、Admin.NET.Web.Hybrid(app host) 2个项目
<table>
    <tr>
        <td><img src="https://gitee.com/devhxj/admin.net.blazor/raw/dev-EnjoyBlazor/doc/img/0.png"/></td>
    </tr>
</table>

### 🏀 分层说明
```
├─Admin.NET.Application             ->业务应用层，在此写您具体业务代码
├─Admin.NET.Const                   ->系统静态变量定义，包括前后端共用的 枚举，辅助方法等（从原Admin.NET.Application分离出来）
├─Admin.NET.Dto                     ->前后端共用的DTO（从原Admin.NET.Application分离出来）
├─Admin.NET.Core                    ->框架核心层
├─Admin.NET.Database.Migrations     ->架构维护层，主要存放迁移中间文件
├─Admin.NET.EntityFramework.Core    ->EF Core配置层，主要配置数据库及相关
├─App
├─├─Admin.NET.Web.Core              ->Webapi核心层，主要是服务注册及鉴权
├─├─Admin.NET.Web.Entry             ->Webapi入口层/启动层，可任意更换
├─├─Admin.NET.Web.Wasm              ->wasm客户端
├─├─Admin.NET.Web.Hybrid            ->host客户端（同时支撑wasm、server）
├─├─Admin.NET.Web.Monitor           ->OpenTelemetry的简单expoter实现，试验，后期会重构
├─├─Admin.NET.Web.Shared            ->Web业务层，Server/Wasm共用
├─├─├─Auth                          ->基于Token的验证实现封装
├─├─├─Components                    ->自定义组件封装
├─├─├─Misc                          ->杂项工具类
├─├─├─Pages                         ->业务页面
├─├─├─Locales                       ->多语言
├─├─├─Service                       ->业务服务
├─├─├─├─HttpApi                     ->基于WebApiClientCore的client定义
├─├─├─├─Interface                         ->业务服务接口定义
├─├─├─├─System                      ->业务服务实现
├─├─├─Shared                        ->母版页和通用组件
├─├─├─├─Global                      ->通用组件（登录、登出、token、init等）
├─├─├─wwwroot                       ->样式及资源
├─Blazor
├─├─Enjoy.blazor                    ->自定义封装组件库，基于BootstrapBlazor组件库标准，后期会重构
├─├─Enjoy.Blazor.SourceGenerator    ->自定义代码生成器

注：
* Admin.NET.Web.Shared、Enjoy.blazor  使用BuildBundlerMinifier主动打包Components文件夹下的js和css。
* Admin.NET.Dto 中集成ValueInjecter
```


### 📖 帮助文档

👉后台文档：
* Furion后台框架文档 [https://dotnetchina.gitee.io/furion/docs/source](https://dotnetchina.gitee.io/furion/docs/source)

👉前端文档：
* 前端MASA.Blazor组件库文档 [https://www.masastack.com/blazor](https://www.masastack.com/blazor)
* 前端BootstrapBlazor组件库文档 [https://www.blazor.zone/introduction](https://www.blazor.zone/introduction)
* [Blazor官方文档](https://docs.microsoft.com/zh-cn/aspnet/core/blazor/?WT.mc_id=DT-MVP-5004174)
* [使用Blazor WebAssembly和Visual Studio Code生成Web应用](https://docs.microsoft.com/zh-cn/learn/modules/build-blazor-webassembly-visual-studio-code/?WT.mc_id=DT-MVP-5004174)
* [什么是Blazor ](https://docs.microsoft.com/zh-cn/learn/modules/build-blazor-webassembly-visual-studio-code/2-understand-blazor-webassembly?WT.mc_id=DT-MVP-5004174)
* [Blazor练习-配置开发环境](https://docs.microsoft.com/zh-cn/learn/modules/build-blazor-webassembly-visual-studio-code/3-exercise-configure-enviromnent?WT.mc_id=DT-MVP-5004174)
* [Blazor组件](https://docs.microsoft.com/zh-cn/learn/modules/build-blazor-webassembly-visual-studio-code/4-blazor-components?WT.mc_id=DT-MVP-5004174)
* [Blazor练习-添加组件](https://docs.microsoft.com/zh-cn/learn/modules/build-blazor-webassembly-visual-studio-code/5-exercise-add-component?WT.mc_id=DT-MVP-5004174)
* [Blazor-数据绑定和事件](https://docs.microsoft.com/zh-cn/learn/modules/build-blazor-webassembly-visual-studio-code/6-csharp-razor-binding?WT.mc_id=DT-MVP-5004174)
* [Blazor练习-数据绑定和事件](https://docs.microsoft.com/zh-cn/learn/modules/build-blazor-webassembly-visual-studio-code/7-exercise-razor-binding?WT.mc_id=DT-MVP-5004174)
* [Blazor总结](https://docs.microsoft.com/zh-cn/learn/modules/build-blazor-webassembly-visual-studio-code/8-summary?WT.mc_id=DT-MVP-5004174)


### 🍎 效果图

<table>
    <tr>
        <td><img src="https://gitee.com/devhxj/admin.net.blazor/raw/dev-EnjoyBlazor/doc/img/1.png"/></td>
        <td><img src="https://gitee.com/devhxj/admin.net.blazor/raw/dev-EnjoyBlazor/doc/img/2.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/devhxj/admin.net.blazor/raw/dev-EnjoyBlazor/doc/img/3.png"/></td>
        <td><img src="https://gitee.com/devhxj/admin.net.blazor/raw/dev-EnjoyBlazor/doc/img/4.png"/></td>
    </tr>
</table>

### 🍖 详细功能

*  功能复刻Admin.NET，具体请参考：  [https://gitee.com/zuohuaijun/Admin.NET](https://gitee.com/zuohuaijun/Admin.NET)
1. 主控面板、控制台页面，可进行工作台，分析页，统计等功能的展示。
2. 用户管理、对企业用户和系统管理员用户的维护，可绑定用户职务，机构，角色，数据权限等。
3. 应用管理、通过应用来控制不同维度的菜单展示。
4. 机构管理、公司组织架构维护，支持多层级结构的树形结构。
5. 职位管理、用户职务管理，职务可作为用户的一个标签，职务目前没有和权限等其他功能挂钩。
6. 菜单管理、菜单目录，菜单，和按钮的维护是权限控制的基本单位。
7. 角色管理、角色绑定菜单后，可限制相关角色的人员登录系统的功能范围。角色也可以绑定数据授权范围。
8. 字典管理、系统内各种枚举类型的维护。
9. 访问日志、用户的登录和退出日志的查看和管理。
10. 操作日志、用户的操作业务的日志的查看和管理。
11. 服务监控、服务器的运行状态，CPU、内存、网络等信息数据的查看。
12. 在线用户、当前系统在线用户的查看。
13. 公告管理、系统的公告的管理。
14. 文件管理、文件的上传下载查看等操作，文件可使用本地存储，阿里云oss，腾讯cos接入，支持拓展。
15. 定时任务、定时任务的维护，通过cron表达式控制任务的执行频率。
16. 系统配置、系统运行的参数的维护，参数的配置与系统运行机制息息相关。

### ⚡ 近期计划

- [x] Admin.NET Vue2版本 基础框架功能复刻（80%）
- [ ] 实现暗黑模式主题
- [ ] 国际化与本地化
- [ ] 框架重构，完善测试及文档
- [ ] 实现Blazor版代码生成器
- [ ] 实现Blazor版表单设计器
- [ ] 重写集成CAS服务端
- [ ] 集成工作流

### 🥦 补充说明
* 基于.NET 6平台 Furion 开发框架与Blazor相结合，实时跟随基架升级而升级！
* 基于 Admin.NET(EFCore版本) 
* 基于 MASAStack 的企业级组件库MASA.Blazor
* 使用集高性能高可扩展性于一体的声明式http客户端库WebApiClientCore
* 持续集百家所长，完善与丰富本框架基础设施，为.NET生态增加一种选择！

### 💐 特别鸣谢
- 👉 Furion：  [https://dotnetchina.gitee.io/furion](https://dotnetchina.gitee.io/furion)
- 👉 Admin.NET：  [https://gitee.com/zuohuaijun/Admin.NET](https://gitee.com/zuohuaijun/Admin.NET)
- 👉 MASA.Blazor：  [https://www.masastack.com/blazor]
- 👉 BootstrapBlazor：  [https://gitee.com/LongbowEnterprise/BootstrapBlazor](https://gitee.com/LongbowEnterprise/BootstrapBlazor)
- 👉 WebApiClient：  [https://github.com/dotnetcore/WebApiClient](https://github.com/dotnetcore/WebApiClient)
- 👉 xiaonuo：[https://gitee.com/xiaonuobase/snowy](https://gitee.com/xiaonuobase/snowy)


如果对您有帮助，您可以点 "Star" 支持一下，这样才有持续下去的动力，谢谢！！！