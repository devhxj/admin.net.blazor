﻿namespace Enjoy.Blazor;

public sealed class NotNullRef<TItem>
{
    [NotNull]
    public TItem Value { get; }

    public NotNullRef(TItem value)
    {
        Value = value ?? throw new ArgumentNullException(nameof(value));
    }
}