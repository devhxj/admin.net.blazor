﻿using System.Text.Json.Serialization;

namespace Enjoy.Blazor;

public sealed class FloatingOptions
{
    /// <summary>
    /// 获得/设置 当前组件ID
    /// </summary>
    public string Id { get;}

    /// <summary>
    /// 获得/设置 浮动层的锚点元素
    /// </summary>
    public string? Anchor { get; set; } 

    /// <summary>
    /// 获得/设置 浮动层相对于它的锚点元素的位置
    /// </summary>
    public Placement Placement { get; set; } = Placement.Bottom;

    /// <summary>
    /// 获得/设置 浮动层显示层级，对应z-index
    /// </summary>    
    public int? ZIndex { get; set; }

    /// <summary>
    /// 获得/设置 当与锚点元素不在同一个剪辑上下文中时是否自动隐藏
    /// </summary>
    public bool AutoHide { get; set; }

    /// <summary>
    /// 获得/设置 是否自动更新位置，默认false
    /// </summary>
    public bool AutoUpdate { get; set; }

    /// <summary>
    /// 获得/设置 浮动元素和参考元素之间的距离，例如left。
    /// </summary>
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public int? MainOffset { get; set; }

    /// <summary>
    /// 获得/设置 浮动元素和参考元素之间的距离，例如top。
    /// </summary>
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public int? CrossOffset { get; set; }

    /// <summary>
    /// 获得/设置 是否自动更改浮动元素的位置以使其处于可见状态。
    /// </summary>
    public bool UseFlip { get; set; }

    /// <summary>
    /// 获得/设置 配置需要填充的宽度，沿指定轴移动浮动元素以使其保持在视图中。
    /// </summary>
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public int? ShiftPadding { get; set; }

    /// <summary>
    /// 获得/设置 是否使用 提示箭头，浮动层必须预先设置为绝对定位
    /// </summary>
    public bool UseArrow { get; set; }

    /// <summary>
    /// 获得/设置 提示箭头的偏移量
    /// </summary>
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public int? ArrowOffset { get; set; }

    /// <summary>
    /// 获得/设置 提示箭头的样式
    /// </summary>
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? ArrowStyleClass { get; set; }

    /// <summary>
    /// 获得/设置 提示箭头的样式
    /// </summary>
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? ArrowClassStyle { get; set; }

    /// <summary>
    /// 获得/设置 组件样式类
    /// </summary>
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? @Class { get; set; }

    /// <summary>
    /// 获得/设置 组件样式
    /// </summary>
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public string? @Style { get; set; }

    /// <summary>
    /// 获得/设置 是否使用遮罩层
    /// </summary>
    public bool ShowOverlay { get; set; }

    /// <summary>
    /// 获得/设置 内层子组件内容 
    /// </summary>
    [JsonIgnore]
    public RenderFragment<IFloating>? ChildContent { get; set; }

    [JsonIgnore]
    internal Func<Task>? OnClearAutoUpdate;

    public FloatingOptions()
    {
        Id = $"b-{Guid.NewGuid():N}";
    }
}