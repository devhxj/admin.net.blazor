﻿function createEventArgs(event) {
    var target = event.target;
    var name = target.getAttribute('name');
    var postbacks = target.getAttribute('postbacks');
    var attributes = [];
    if (!!postbacks) {
        postbacks.split(',').forEach(attr => {
            if (!!attr) {
                var value = target.getAttribute(attr);
                attributes.push({ key: attr, value: value });
            }
        });
    }
    return {
        targetName: name,
        targetAttributes: attributes.length > 0 ? attributes : null
    }
}
export function beforeStart(options, extensions) {
}
export function afterStarted(blazor) {
    //注册自定义drop事件
    Blazor.registerCustomEventType('customdrop', {
        browserEventName: 'drop',
        createEventArgs: event => {
            var args = createEventArgs(event);
            event.target.classList.remove('overed');
            args.transferText = event.dataTransfer.getData("text/plain");
            return args;
        }
    });

    //注册自定义mouseup事件
    Blazor.registerCustomEventType('custommouseup', {
        browserEventName: 'mouseup',
        createEventArgs: createEventArgs
    });
}