window.Enjoy = {
    //全局缓存
    Clay: {},
    Maps: new Map(),
    debounce: function (fn, wait) {
        let timer = null;
        return function (...args) {
            if (timer) clearTimeout(timer);
            timer = setTimeout(() => {
                fn.apply(this, args)
            }, wait)
        }
    },
    resizeObserver: function (elementId, invoker) {
        const resizeObserver = new ResizeObserver((entries => {
            if (entries.length > 0) {
                var border = entries[0].borderBoxSize[0];
                var content = entries[0].contentBoxSize[0];
                invoker.invokeMethodAsync('Invoke', {
                    borderBoxBlockSize: border.blockSize,
                    borderBoxInlineSize: border.inlineSize,
                    contentBoxBlockSize: content.blockSize,
                    contentBoxInlineSize: content.inlineSize,
                });
            }
        }));

        var element = document.getElementById(elementId);
        var rect = element?.getBoundingClientRect();
        if (element) resizeObserver.observe(element);
        return rect;
    },
    intersectionObserver: function (elementId, invoker) {
        const observer = new IntersectionObserver((entries) => {
            if (entries.some(e => e.isIntersecting)) {
                invoker.invokeMethodAsync('Invoke')
            }
        })
        observer.observe(document.getElementById(elementId))
    },
    //dragleave状态设置
    dragLeave: function () {
        var event = window.event || arguments[0];
        if (event.target.classList.contains('dropable'))
            event.target.classList.remove('overed');
    },
    //dragenter状态设置
    dragEnter: function () {
        var event = window.event || arguments[0];
        if (event.target.classList.contains('dropable'))
            event.target.classList.add('overed');
    },
    //GridView组件列放置拖拽开始
    startColumnDrag: function () {
        var event = window.event || arguments[0];
        var name = event.target.getAttribute('name');
        var ghost = document.createElement('div');
        ghost.id = 'ghost_' + name;
        ghost.className = event.target.classList.contains('theme--dark')
            ? 'e-ghost theme--dark'
            : 'e-ghost theme--light';
        ghost.innerHTML = event.target.nextElementSibling.innerHTML;
        document.body.appendChild(ghost);
        event.dataTransfer.setData('text/plain', name);
        event.dataTransfer.setDragImage(ghost, 0, 0);
    },
    //GridView组件列放置拖拽结算
    endColumnDrag: function () {
        var event = window.event || arguments[0];
        var name = event.target.getAttribute('name');
        var ghost = document.getElementById('ghost_' + name)
        if (ghost !== null)
            document.body.removeChild(ghost);
    },
    //GridView组件列宽度拖拽
    startColumnResize: function () {
        var event = window.event || arguments[0];
        var element = event.target;
        var cell = element.parentNode;
        var name = element.getAttribute('name');
        var col = document.getElementById(name + '-col');

        Enjoy.Clay[element] = {
            clientX: event.clientX,
            width: cell.getBoundingClientRect().width,
            mouseUpHandler: function (e) {
                if (Enjoy.Clay[element]) {
                    document.removeEventListener('mousemove', Enjoy.Clay[element].mouseMoveHandler);
                    document.removeEventListener('mouseup', Enjoy.Clay[element].mouseUpHandler);
                    document.removeEventListener('touchmove', Enjoy.Clay[element].touchMoveHandler)
                    document.removeEventListener('touchend', Enjoy.Clay[element].mouseUpHandler);
                    Enjoy.Clay[element] = null;

                    //如果鼠标移出了resize区域后，需要手动触发
                    var e = document.createEvent("MouseEvents");
                    e.initEvent("mouseup", true, true);
                    element.dispatchEvent(e);
                }
            },
            mouseMoveHandler: function (e) {
                if (Enjoy.Clay[element]) {
                    var width = parseInt(Enjoy.Clay[element].width - (Enjoy.Clay[element].clientX - e.clientX));
                    if (col && width >= 60) {
                        col.style.width = width + 'px';
                        element.setAttribute('colWidth', width);
                    }
                }
            },
            touchMoveHandler: function (e) {
                if (e.targetTouches[0]) Enjoy.Clay[element].mouseMoveHandler(e.targetTouches[0]);
            }
        };

        document.addEventListener('mousemove', Enjoy.Clay[element].mouseMoveHandler);
        document.addEventListener('mouseup', Enjoy.Clay[element].mouseUpHandler);
        document.addEventListener('touchmove', Enjoy.Clay[element].touchMoveHandler, { passive: true })
        document.addEventListener('touchend', Enjoy.Clay[element].mouseUpHandler, { passive: true });
    },
    //清空自由浮动
    clearFloating: function (elementId) {
        if (Enjoy.Maps.has(elementId)) {
            Enjoy.Maps.get(elementId).call(globalThis);
            Enjoy.Maps.delete(elementId);
        }
    },
    //设置浮动
    floatingElement: function (options, htmlElement) {
        let reference = document.getElementById(options.anchor), arrow = null;
        if (!reference) {
            console.info(`Floating Reference anchor ${options.anchor} not found.`);
            return;
        }

        if (!htmlElement) {
            console.info(`Floating ${htmlElement.id} not found.`);
            return;
        }

        //配置中间件
        let config = {
            strategy: 'absolute',
            placement: 'auto',
            zIndex: options.zIndex,
            mainOffset: options.mainOffset,
            crossOffset: options.crossOffset,
            useFlip: options.useFlip,
            shiftPadding: options.shiftPadding,
            useArrow: options.useArrow,
            arrowOffset: options.arrowOffset,
            autoUpdate: options.autoUpdate,
            autoHide: options.autoHide,
            middleware: []
        };

        //放置位置
        switch (options.placement) {
            case 1: config.placement = 'auto-start'; break;
            case 2: config.placement = 'auto-end'; break;
            case 3: config.placement = 'top'; break;
            case 4: config.placement = 'top-star'; break;
            case 5: config.placement = 'top-end'; break;
            case 6: config.placement = 'right'; break;
            case 7: config.placement = 'right-start'; break;
            case 8: config.placement = 'auto-end'; break;
            case 9: config.placement = 'bottom'; break;
            case 10: config.placement = 'bottom-start'; break;
            case 11: config.placement = 'bottom-end'; break
            case 12: config.placement = 'left'; break;
            case 13: config.placement = 'left-start'; break;
            case 14: config.placement = 'left-end'; break;
        }

        //设置偏移量
        if (config.mainOffset && config.crossOffset) {
            config.middleware.push(FloatingUIDOM.offset({
                mainAxis: parseInt(config.mainOffset), crossAxis: parseInt(config.crossOffset)
            }));
        } else if (config.mainOffset) {
            config.middleware.push(FloatingUIDOM.offset({ mainAxis: parseInt(config.mainOffset) }));

        } else if (config.crossOffset) {
            config.middleware.push(FloatingUIDOM.offset({ crossAxis: parseInt(config.crossAxis) }));
        }

        //设置自动放置，与自动翻动不能同时存在
        if (config.placement == 'auto') {
            config.middleware.push(FloatingUIDOM.autoPlacement());
        }
        else if (config.placement == 'auto-start') {
            config.middleware.push(FloatingUIDOM.autoPlacement({ alignment: 'start' }));
        }
        else if (config.placement == 'auto-end') {
            config.middleware.push(FloatingUIDOM.autoPlacement({ alignment: 'end' }));
        }
        //设置自动翻动来保持可见
        else if (config.useFlip) {
            config.middleware.push(FloatingUIDOM.flip());
        }

        //设置保持内容完整显示
        if (config.shiftPadding) {
            config.middleware.push(FloatingUIDOM.shift({ padding: parseInt(config.shiftPadding) }));
        }

        //设置指示箭头
        if (config.useArrow) {
            arrow = htmlElement.querySelector('[data-element=arrow]');
            if (arrow != null) {
                config.middleware.push(FloatingUIDOM.arrow({ element: arrow }));
            }
        }

        //设置自动隐藏
        if (config.autoHide) {
            config.middleware.push(FloatingUIDOM.hide());
        }

        //定义更新方法
        async function update() {
            await FloatingUIDOM.computePosition(reference, htmlElement, config)
                .then(({ x, y, placement, strategy, middlewareData }) => {
                    const left = `${Math.round(x)}px`;
                    const top = `${Math.round(y)}px`;

                    if (htmlElement.style.left == left && htmlElement.style.top == top) {
                        return;
                    }

                    if (arrow != null) {
                        const p = placement.split('-')[0];
                        const side = { top: 'bottom', right: 'left', bottom: 'top', left: 'right' }[p] ?? 'bottom';
                        const offset = `${(config.arrowOffset || '0')}px`;
                        let left1 = '', top1 = '';
                        if (middlewareData.arrow != undefined) {
                            if (middlewareData.arrow.x != undefined) {
                                left1 = `${Math.round(middlewareData.arrow.x)}px`;
                            }
                            if (middlewareData.arrow.y != undefined) {
                                top1 = `${Math.round(middlewareData.arrow.y)}px`;
                            }
                        }

                        Object.assign(arrow.style, { position: strategy, top: top1, left: left1, [side]: offset });
                    }

                    Object.assign(htmlElement.style, {
                        position: strategy,
                        left: left,
                        top: top,
                        opacity: 1,
                        transform: 'scale(1)',
                        visibility: 'visible',
                        'z-index': config.zIndex
                    });

                    if (htmlElement.dataset.autoHide) {
                        Object.assign(htmlElement.style, {
                            visibility: middlewareData.hide.referenceHidden ? 'hidden' : 'visible'
                        });
                    }
                });
        }

        //第一次需要手动调用
        update();

        //注册自动更新
        if (config.autoUpdate) {
            Enjoy.Maps.set(htmlElement.id, FloatingUIDOM.autoUpdate(reference, htmlElement, update));
        }
    },

    initGridView: function (element, interop, mobileBreakWidth) {

        //单击代理
        element.querySelectorAll('e-grid-view__wrapper>table>tbody>tr>td')
            .addEventListener('click', event => {
                console.info(event.target);
                console.info(`${event.target.getAttribute('name')} clicked`);
                //event.stopPropagation();
                //event.preventDefault();
                if (event.target.hasAttribute('effective')) {
                    var postbacks = event.target.getAttribute('postbacks');
                    var attributes = [];
                    if (!!postbacks) {
                        postbacks.split(',').forEach(attr => {
                            if (!!attr) {
                                var value = event.target.getAttribute(attr);
                                attributes.push({ key: attr, value: value });
                            }
                        });
                    }
                    interop.invokeMethodAsync('Effective', attributes);
                }
            });

        //宽度监听
        if (mobileBreakWidth > 0) {
            var getWidth = function (el) {
                return el.clientWidth == 0 ? (el.parentElement ? getWidth(el.parentElement) : 0) : el.clientWidth;
            }

            //获取宽度
            var width = getWidth(element);
            var isMobileBreaked = width <= mobileBreakWidth;

            //监听大小
            new ResizeObserver((entries => {
                if (entries.length > 0) {
                    var size = entries[0].contentBoxSize[0].inlineSize;
                    var breaked = size <= mobileBreakWidth && size > 0;
                    if (isMobileBreaked != breaked) {
                        isMobileBreaked = breaked;
                        interop.invokeMethodAsync('Breaked', breaked);
                    }
                }
            })).observe(element);

            //返回初始大小
            return isMobileBreaked;
        }

        return false;
    }
};

