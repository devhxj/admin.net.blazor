﻿namespace Enjoy.Blazor;

/// <summary>
/// 改变刷新状态
/// </summary>
public interface IRefreshState : IHandleEvent
{
    /// <summary>
    /// 刷新状态
    /// </summary>
    /// <returns></returns>
    Task RefreshState();

    /// <summary>
    /// 刷新状态
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="value"></param>
    /// <returns></returns>
    Task RefreshState<TValue>(TValue value) => RefreshState();
}