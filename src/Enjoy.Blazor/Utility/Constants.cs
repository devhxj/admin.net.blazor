﻿namespace Enjoy.Blazor;

/// <summary>
/// 常量定义
/// </summary>
internal static class Constants
{
    /// <summary>
    /// 指示符号
    /// </summary>
    public const string Symbol = "Symbol";

    /// <summary>
    /// I18n键
    /// </summary>
    public const string I18nKey = "I18nKey";
}
