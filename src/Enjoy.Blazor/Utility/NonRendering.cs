﻿namespace Enjoy.Blazor;

/// <summary>
/// 事件不触发渲染
/// </summary>
public static class NonRendering
{
    private record ReceiverBase : IHandleEvent
    {
        public Task HandleEventAsync(EventCallbackWorkItem item, object? arg)
        {
            return item.InvokeAsync(arg);
        }
    }
    private record SyncReceiver(Action callback) : ReceiverBase
    {
        public void Invoke() => callback();
    }
    private record SyncReceiver<T>(Action<T> callback) : ReceiverBase
    {
        public void Invoke(T arg) => callback(arg);
    }
    private record AsyncReceiver(Func<Task> callback) : ReceiverBase
    {
        public Task Invoke() => callback();
    }
    private record AsyncReceiver<T>(Func<T, Task> callback) : ReceiverBase
    {
        public Task Invoke(T arg) => callback(arg);
    }
    public static Action CreateEventCallback(Action callback) => new SyncReceiver(callback).Invoke;
    public static Action<TValue> CreateEventCallback<TValue>(Action<TValue> callback) => new SyncReceiver<TValue>(callback).Invoke;
    public static Func<Task> CreateEventCallback(Func<Task> callback) => new AsyncReceiver(callback).Invoke;
    public static Func<TValue, Task> CreateEventCallback<TValue>(Func<TValue, Task> callback) => new AsyncReceiver<TValue>(callback).Invoke;
}
