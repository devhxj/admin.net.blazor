﻿namespace Enjoy.Blazor;

public class JsDotNetInvoker : IDisposable
{
    private readonly IJSRuntime _jsRuntime;
    private readonly List<IDisposable> _invokers = new();

    public JsDotNetInvoker(IJSRuntime jsRuntime)
    {
        _jsRuntime = jsRuntime;
    }

    public async Task<BoundingClientRect?> ResizeObserver(string? elementId, Func<ResizeObserverOptions, Task> func)
    {
        if (string.IsNullOrWhiteSpace(elementId))
        {
            return null;
        }

        var reference = DotNetObjectReference.Create(new ResizeObserverInvoker(func));

        _invokers.Add(reference);

        return await _jsRuntime.InvokeAsync<BoundingClientRect?>("Enjoy.resizeObserver", elementId, reference);
    }

    public async Task IntersectionObserver(string? elementId, Func<Task> func)
    {
        if (string.IsNullOrWhiteSpace(elementId))
        {
            return;
        }

        var invoker = DotNetObjectReference.Create(new Invoker(func));

        _invokers.Add(invoker);

        await _jsRuntime.InvokeVoidAsync("Enjoy.intersectionObserver", elementId, invoker);
    }

    public async Task FloatingElement(FloatingOptions option, ElementReference reference)
    {
        await _jsRuntime.InvokeVoidAsync("Enjoy.floatingElement", option, reference);
    }

    public async Task ClearFloating(string? elementId)
    {
        if (string.IsNullOrWhiteSpace(elementId))
        {
            return;
        }

        await _jsRuntime.InvokeVoidAsync("Enjoy.clearFloating", elementId);
    }

    public void Dispose()
    {
        foreach (var invoker in _invokers)
        {
            invoker.Dispose();
        }
    }

    private class ResizeObserverInvoker
    {
        private readonly Func<ResizeObserverOptions, Task>? _func;

        public ResizeObserverInvoker(Func<ResizeObserverOptions, Task> func) => _func = func;

        [JSInvokable]
        public async Task Invoke(ResizeObserverOptions entry)
        {
            if (_func is not null)
            {
                await _func.Invoke(entry);
            }
        }
    }

    private class IntersectionObserverInvoker
    {
        private readonly Func<Task>? _func;

        public IntersectionObserverInvoker(Func<Task> func) => _func = func;

        [JSInvokable]
        public async Task Invoke()
        {
            if (_func is not null)
            {
                await _func.Invoke();
            }
        }
    }
}
