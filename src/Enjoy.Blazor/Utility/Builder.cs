﻿namespace Enjoy.Blazor;

public static class Builder
{
    public static string? StyleClass(bool use, string? @class)
        => use ? @class : null;

    public static string? StyleClass(Func<bool> func, string? @class)
        => func() ? @class : null;

    public static string? StyleClass(bool use, string? tureClass, string? falseClass)
        => use ? tureClass : falseClass;

    public static string? StyleClass(Func<bool> func, string? tureClass, string? falseClass)
        => func() ? tureClass : falseClass;
}
