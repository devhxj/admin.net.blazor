﻿using System.ComponentModel;

namespace Enjoy.Blazor;

/// <summary>
/// 
/// </summary>
public enum Placement
{
    /// <summary>
    /// 
    /// </summary>
    [Description("auto")]
    Auto = 0,

    /// <summary>
    /// 
    /// </summary>
    [Description("auto-start")]
    AutoStart = 1,

    /// <summary>
    /// 
    /// </summary>
    [Description("auto-end")]
    AutoEnd = 2,

    /// <summary>
    /// 
    /// </summary>
    [Description("top")]
    Top = 3,

    /// <summary>
    /// 
    /// </summary>
    [Description("top-start")]
    TopStart = 4,

    /// <summary>
    /// 
    /// </summary>
    [Description("top-end")]
    TopEnd = 5,

    /// <summary>
    /// 
    /// </summary>
    [Description("right")]
    Right = 6,

    /// <summary>
    /// 
    /// </summary>
    [Description("right-start")]
    RightStart = 7,

    /// <summary>
    /// 
    /// </summary>
    [Description("right-end")]
    RightEnd = 8,

    /// <summary>
    /// 
    /// </summary>
    [Description("bottom")]
    Bottom = 9,

    /// <summary>
    /// 
    /// </summary>
    [Description("bottom-start")]
    BottomStart = 10,

    /// <summary>
    /// 
    /// </summary>
    [Description("bottom-end")]
    BottomEnd = 11,

    /// <summary>
    /// 
    /// </summary>
    [Description("left")]
    Left = 12,

    /// <summary>
    /// 
    /// </summary>
    [Description("left-start")]
    LeftStart = 13,

    /// <summary>
    /// 
    /// </summary>
    [Description("left-end")]
    LeftEnd = 14
}
