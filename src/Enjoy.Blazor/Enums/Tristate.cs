﻿using System.ComponentModel;

namespace Enjoy.Blazor;

/// <summary>
/// 三态值
/// </summary>
public enum Tristate
{
    /// <summary>
    /// 无
    /// </summary>
    [Description("无")]
    None,

    /// <summary>
    /// 单
    /// </summary>
    [Description("单")]
    Single,

    /// <summary>
    /// 多
    /// </summary>
    [Description("多")]
    Multiple,
}
