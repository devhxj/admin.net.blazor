﻿namespace Enjoy.Blazor;

public enum Boolean0
{
    [Mapper(Constants.I18nKey, "$enjoyBlazor.boolean0.false")]
    False = 0,

    [Mapper(Constants.I18nKey, "$enjoyBlazor.boolean0.true")]
    True = 1,
}
