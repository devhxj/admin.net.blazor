﻿namespace Enjoy.Blazor;

/// <summary>
/// Dictionary 扩展类
/// </summary>
public static class DictionaryExtensions
{
    public static bool TryGetValue<TValue>(this IEnumerable<KeyValuePair<string, string?>> array, string key, out TValue value)
        where TValue : struct, IParsable<TValue>
    {
        var pair = array.FirstOrDefault(x => x.Key == key);
        if (!string.IsNullOrWhiteSpace(pair.Value) && TValue.TryParse(pair.Value, null, out TValue val))
        {
            value = val;
            return true;
        }

        value = default;
        return false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="prop"></param>
    /// <param name="merge"></param>
    /// <returns></returns>
    public static IDictionary<string, object> Merge(this IEnumerable<KeyValuePair<string, object>> prop, IEnumerable<KeyValuePair<string, object>> merge)
    {
        var rest = new Dictionary<string, object>();
        foreach (var pair in prop)
        {
            rest.Add(pair.Key, pair.Value);
        }

        foreach (var pair in merge)
        {
            if (rest.ContainsKey(pair.Key))
            {
                rest[pair.Key] = pair.Value;
            }
            else
            {

                rest.Add(pair.Key, pair.Value);
            }
        }

        return rest;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="prop"></param>
    /// <param name="merge"></param>
    /// <returns></returns>
    public static IDictionary<string, object> Merge(this IEnumerable<KeyValuePair<string, object>> prop, KeyValuePair<string, object> merge)
    {
        var rest = new Dictionary<string, object>();
        foreach (var pair in prop)
        {
            rest.Add(pair.Key, pair.Value);
        }

        if (rest.ContainsKey(merge.Key))
        {
            rest[merge.Key] = merge.Value;
        }
        else
        {

            rest.Add(merge.Key, merge.Value);
        }

        return rest;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="prop"></param>
    /// <param name="merge"></param>
    /// <returns></returns>
    public static IDictionary<string, object> Merge(this IEnumerable<KeyValuePair<string, object>> prop, string key, object value)
    {
        var rest = new Dictionary<string, object>();
        foreach (var pair in prop)
        {
            rest.Add(pair.Key, pair.Value);
        }

        if (rest.ContainsKey(key))
        {
            rest[key] = value;
        }
        else
        {

            rest.Add(key, value);
        }

        return rest;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="prop"></param>
    /// <param name="merge"></param>
    /// <returns></returns>
    public static void Append(this IDictionary<string, object?> prop, IEnumerable<KeyValuePair<string, object>> merge)
    {
        foreach (var pair in merge)
        {
            if (!prop.ContainsKey(pair.Key))
            {

                prop.Add(pair.Key, pair.Value);
            }
        }
    }

    /// <summary>
    /// 阻止冒泡
    /// </summary>
    /// <param name="prop"></param>
    /// <returns></returns>
    public static IDictionary<string, object> StopPropagation(this IEnumerable<KeyValuePair<string, object>> prop)
    {
        var rest = new Dictionary<string, object>();
        foreach (var pair in prop)
        {
            rest.Add(pair.Key, pair.Value);
        }

        if (rest.ContainsKey("__internal_stopPropagation_onclick"))
        {
            rest["__internal_stopPropagation_onclick"] = true;
        }
        else
        {

            rest.Add("__internal_stopPropagation_onclick", true);
        }

        return rest;
    }
}
