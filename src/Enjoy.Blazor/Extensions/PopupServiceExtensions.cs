﻿namespace Enjoy.Blazor;

public static class PopupServiceExtensions
{
    public static Task<object?> DialogAsync(this IPopupService service, StringNumber maxWidth, RenderFragment<IPopupHandle>? childContent, bool isScrolling)
        => service.OpenAsync(typeof(EDialog), new Dictionary<string, object?>()
        {
            {nameof(EDialog.MaxWidth),maxWidth },
            {nameof(EDialog.ChildContent),childContent },
            {nameof(EDialog.IsScrolling),isScrolling }
        });

    public static Task<object?> EditDialogAsync<TModel>(this IPopupService service, Dictionary<string, object?> parameters)
        => service.OpenAsync(typeof(EEditDialog<TModel>), parameters);

    public static Task<object?> FloatingAsync(this IPopupService service, Action<FloatingOptions> action)
    {
        var options = new FloatingOptions();
        action(options);

        var attributes = new Dictionary<string, object?>() 
        {
            { nameof(EFloatingContent.Options), options }
        };

        return service.OpenAsync(typeof(EFloatingContent), attributes);
    }
}
