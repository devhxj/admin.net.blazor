﻿using System.Text;

namespace Enjoy.Blazor;

internal static class StyleBuilderExtensions
{
    public static StringBuilder AddClass(this StringBuilder builder, string? value)
    {
        if (!string.IsNullOrWhiteSpace(value)) builder.AppendFormat(" {0}", value);
        return builder;
    }

    public static StringBuilder AddClass(this StringBuilder builder, string? value, bool when = true)
        => when ? AddClass(builder, value) : builder;

    public static StringBuilder AddClass(this StringBuilder builder, string? value, Func<bool> when)
        => AddClass(builder, value, when());

    public static StringBuilder AddClass(this StringBuilder builder, Func<string?> value, bool when = true)
        => when ? AddClass(builder, value()) : builder;

    public static StringBuilder AddClass(this StringBuilder builder, Func<string?> value, Func<bool> when)
        => AddClass(builder, value, when());

    public static StringBuilder AddClass(this StringBuilder builder, string? trueValue, string falseValue, bool when)
    {
        if (when)
            return AddClass(builder, trueValue);
        else
            return AddClass(builder, falseValue);
    }

    public static StringBuilder AddClassFromAttributes(this StringBuilder builder, IDictionary<string, object>? additionalAttributes)
    {
        if (additionalAttributes is not null && additionalAttributes.TryGetValue("class", out var @class))
            AddClass(builder, @class?.ToString());

        return builder;
    }

    public static StringBuilder AddStyle(this StringBuilder builder, string? value)
    {
        if (!string.IsNullOrEmpty(value)) builder.AppendFormat(";{0}", value);
        return builder;
    }

    public static StringBuilder AddStyle(this StringBuilder builder, string? value, bool when = true)
        => when ? AddStyle(builder, value) : builder;

    public static StringBuilder AddStyle(this StringBuilder builder, string? value, Func<bool> when)
        => AddStyle(builder, value, when());

    public static StringBuilder AddStyle(this StringBuilder builder, Func<string?> value, bool when = true)
        => when ? AddStyle(builder, value()) : builder;

    public static StringBuilder AddStyle(this StringBuilder builder, Func<string?> value, Func<bool> when)
        => AddStyle(builder, value, when());

    public static StringBuilder AddStyle(this StringBuilder builder, string? trueValue, string falseValue, bool when)
    {
        if (when)
        {
            return AddStyle(builder, trueValue);
        }
        else
        {
            return AddStyle(builder, falseValue);
        }
    }

    public static StringBuilder AddStyleFromAttributes(this StringBuilder builder, IDictionary<string, object>? additionalAttributes)
    {
        if (additionalAttributes is not null && additionalAttributes.TryGetValue("style", out var style))
            AddStyle(builder, style?.ToString());

        return builder;
    }
}
