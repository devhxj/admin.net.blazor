﻿namespace Enjoy.Blazor;

public static class BooleanExtensions
{
    public static IList<SelectItem<bool>> GetI18nSelectItems(I18n? i18n = null)
    {
        return new SelectItem<bool>[]
        {
            new(i18n?.T("$enjoyBlazor.boolean0.true",false) ?? "true", true),
            new(i18n?.T("$enjoyBlazor.boolean0.false",false) ?? "false", false)
        };
    }

    public static IList<SelectItem<bool?>> GetI18nNullableSelectItems(I18n? i18n = null)
    {
        return new SelectItem<bool?>[]
        {
            new(i18n?.T("$enjoyBlazor.null",false) ?? "null", null),
            new(i18n?.T("$enjoyBlazor.boolean0.true",false) ?? "true", true),
            new(i18n?.T("$enjoyBlazor.boolean0.false",false) ?? "false", false)
        };
    }
}
