﻿namespace Enjoy.Blazor;

[EventHandler("oncustomdrop", typeof(DragCustomEventArgs), true, true)]
[EventHandler("oncustommouseup", typeof(MouseCustomEventArgs), true, true)]
public static class EventHandlers
{
}

/// <summary>
/// 自定义参数接口
/// </summary>
public interface ICustomEventArgs
{
    /// <summary>
    /// 获取 目标元素name属性
    /// </summary>
    string? TargetName { get; }

    /// <summary>
    /// 获取 目标元素其他属性
    /// </summary>
    KeyValuePair<string, string?>[]? TargetAttributes { get; }
}

public class DragCustomEventArgs : DragEventArgs, ICustomEventArgs
{
    /// <summary>
    /// 获取 目标元素name属性
    /// </summary>
    public string? TargetName { get; set; }

    /// <summary>
    /// 获取/设置 目标元素其他属性
    /// </summary>
    public KeyValuePair<string, string?>[]? TargetAttributes { get; set; }

    /// <summary>
    /// 获取/设置 拖拽传递值
    /// </summary>
    public string? TransferText { get; set; }
}

public class MouseCustomEventArgs : MouseEventArgs, ICustomEventArgs
{
    /// <summary>
    /// 获取 目标元素name属性
    /// </summary>
    public string? TargetName { get; set; }

    /// <summary>
    /// 获取/设置 目标元素其他属性
    /// </summary>
    public KeyValuePair<string, string?>[]? TargetAttributes { get; set; }
}
