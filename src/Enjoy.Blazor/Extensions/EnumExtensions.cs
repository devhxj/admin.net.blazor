﻿using System.ComponentModel;
using System.Reflection;

namespace Enjoy.Blazor;

public static class EnumExtensions
{
    private static readonly ConcurrentDictionary<Enum, AttributeConfig> _cachedEnumAttributes = new();

    internal static AttributeConfig ReadAttribute(this Enum value)
    {
        if (_cachedEnumAttributes.TryGetValue(value, out var cacheValue))
        {
            return cacheValue;
        }

        
        var fieldInfo = value.GetType().GetFields().FirstOrDefault(p => p.Name == value.ToString());
        if (fieldInfo is not null)
        {
            var displayName = fieldInfo.GetCustomAttribute<DisplayNameAttribute>(true)?.DisplayName;
            var description = fieldInfo.GetCustomAttribute<DescriptionAttribute>(true)?.Description;
            var keyValuePairs = fieldInfo
                .GetCustomAttributes<MapperAttribute>(true)?
                .ToDictionary(x => x.Key, x => x.Value);

            var config = new AttributeConfig(displayName, description, keyValuePairs);
            _cachedEnumAttributes.TryAdd(value, config);

            return config;
        }

        return new AttributeConfig();
    }

    public static bool TryParseToEnum<TEnum>(this StringNumber? value, out TEnum result)
        where TEnum : struct, Enum
    {
        result = default;

        if (value is null)
        {
            return false;
        }

        if (Enum.TryParse(value.ToString(), out TEnum @enum))
        {
            result = @enum;
            return true;
        }
        else
        {
            return false;
        }
    }

    public static IList<SelectItem<TEnum>> GetI18nSelectItems<TEnum>(I18n? i18n = null) where TEnum : struct, Enum
    {
        var list = new List<SelectItem<TEnum>>();
        foreach (var value in Enum.GetValues<TEnum>())
        {
            var config = value.ReadAttribute();
            var name = i18n?.T(config[Constants.I18nKey], false)
                ?? config.DisplayName
                ?? config.Description
                ?? value.ToString();

            list.Add(new(name, value));
        }
        return list;
    }

    public static IList<SelectItem<TEnum?>> GetI18nNullableSelectItems<TEnum>(I18n? i18n = null) where TEnum : struct, Enum
    {
        var list = new List<SelectItem<TEnum?>>
        {
            new(i18n?.T("$enjoyBlazor.null") ?? "null", null)
        };

        foreach (var value in Enum.GetValues<TEnum>())
        {
            var config = value.ReadAttribute();
            var name = i18n?.T(config[Constants.I18nKey], false)
                ?? config.DisplayName
                ?? config.Description
                ?? value.ToString();

            list.Add(new(name, value));
        }

        return list;
    }
}
