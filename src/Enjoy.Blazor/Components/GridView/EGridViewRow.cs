﻿using Microsoft.Extensions.Logging;

namespace Enjoy.Blazor;

public class EGridViewRow<TItem> : ComponentBase, IDisposable, IGridViewRow
{
    [Parameter]
    [NotNull]
    public TItem? Item { get; set; }

    [Parameter]
    public int Index { get; init; }

    [Parameter]
    public EGridViewAdd<TItem>? Inserted { get; init; }

    [Parameter]
    public RenderFragment<EGridViewRow<TItem>>? ChildContent { get; init; }

    [Inject][NotNull] ILogger<EGridViewRow<TItem>>? Logger { get; set; }

    public string Identifier { get; }

    public bool Adding { get; private set; }

    public bool Editing { get; private set; }

    internal bool EditLoading { get; private set; }

    internal TItem? EditItem { get; private set; }

    internal bool TreeLoading { get; set; }

    internal EGridViewForm? EditForm { get; set; }

    internal GridViewTreeNode<TItem>? TreeNode { get; set; }

    internal GridViewRowRenderEventArgs<TItem>? RenderArgs { get; set; }

    internal int PreventClickCount { get; set; }

    public EGridViewRow()
    {
        Identifier = Guid.NewGuid().ToString("N");
    }

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();
        if (Inserted is not null)
        {
            Inserted.RegisterRow(this);
            Adding = true;
        }
    }

    protected override void BuildRenderTree(RenderTreeBuilder builder) => builder.AddContent(0, ChildContent, this);

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        Logger.LogInformation($"{DateTime.Now}:{Item}");
        await base.OnAfterRenderAsync(firstRender);
        if (EditLoading)
        {
            EditLoading = false;
            StateHasChanged();
        }
    }

    public Task ChangStateAsync() => InvokeAsync(StateHasChanged);

    public Task CancelEditingAsync()
    {
        Editing = EditLoading = false;
        EditItem = default;

        return InvokeAsync(StateHasChanged);
    }

    public Task UpdateEditedAsync()
    {
        Item = EditItem;
        return CancelEditingAsync();
    }

    public async Task ResetEditFormAsync()
    {
        EditItem = Item.TryDeepClone();
        await InvokeAsync(StateHasChanged);
    }

    public void ClearEditForm() => EditForm?.Reset();

    public bool? ValidateEditForm() => EditForm?.Validate();

    Task IGridViewRow.UpdateEditingAsync()
    {
        Editing = EditLoading = true;
        EditItem = Item.TryDeepClone();

        return InvokeAsync(StateHasChanged);
    }

    object? IGridViewRow.EditItem => EditItem;

    object IGridViewRow.Item => Item;

    void IDisposable.Dispose()
    {
        if (TreeNode is not null)
        {
            TreeNode.StateHasChanged -= ChangStateAsync;
            TreeNode = null;
        }

        if (Inserted is not null)
        {
            Inserted.RemoveRow(this);
            Adding = false;
        }

        GC.SuppressFinalize(this);
    }
}
