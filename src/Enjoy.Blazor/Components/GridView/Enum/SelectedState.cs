﻿using System.ComponentModel;

namespace Enjoy.Blazor;

internal enum SelectedState
{
    /// <summary>
    /// 未选中
    /// </summary>
    [Description("未选中")]
    UnSelected,

    /// <summary>
    /// 选中部分
    /// </summary>
    [Description("选中部分")]
    Indeterminate,

    /// <summary>
    /// 选中所有
    /// </summary>
    [Description("选中所有")]
    SelectedAll,
}
