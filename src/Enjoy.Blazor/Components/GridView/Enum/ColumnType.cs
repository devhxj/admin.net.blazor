﻿using System.ComponentModel;

namespace Enjoy.Blazor;

public enum ColumnType
{    
    /// <summary>
    /// 数据列
    /// </summary>
    [Description("数据列")]
    Data,

    /// <summary>
    /// 分组
    /// </summary>
    [Description("分组")]
    Group,

    /// <summary>
    /// 序号列
    /// </summary>
    [Description("序号列")]
    Index,

    /// <summary>
    /// 选择列
    /// </summary>
    [Description("选择列")]
    Select,

    /// <summary>
    /// 工具列
    /// </summary>
    [Description("工具列")]
    Tool
}
