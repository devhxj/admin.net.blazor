﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    void IGridViewTopbar.SetFilterKeywords(string? value) => _filterKeywords = value;

    Task IGridViewTopbar.ShowDynamicFilter() => PopupService.DialogAsync(720, RenderFilterDialog, false);

    RenderFragment IGridViewTopbar.RenderCustomFilter()
    {
        if (_setting?.FilterTemplate is not null)
        {
            return new RenderFragment(builder =>
            {
                builder.OpenComponent<EGridViewCustomFilter>(0);
                builder.AddAttribute(1, nameof(EGridViewCustomFilter.Model), _setting.FilterModel);
                builder.AddAttribute(2, nameof(EGridViewCustomFilter.ChildContent), 
                    new RenderFragment<FormContext>(x => y => y.AddContent(3, _setting.FilterTemplate)));
                builder.AddComponentReferenceCapture(3, x => _filterForm = (EGridViewCustomFilter)x);
                builder.CloseComponent();
            });
        }
        else if (FilterTemplate is not null)
        {
            return new RenderFragment(builder =>
            {
                builder.OpenComponent<EGridViewCustomFilter>(4);
                builder.AddAttribute(5, nameof(EGridViewCustomFilter.Model), _filterModel);
                builder.AddAttribute(6, nameof(EGridViewCustomFilter.ChildContent), 
                    new RenderFragment<FormContext>(x => y => y.AddContent(3, FilterTemplate(this))));
                builder.AddComponentReferenceCapture(7, x => _filterForm = (EGridViewCustomFilter)x);
                builder.CloseComponent();
            });
        }
        else
        {
            return new RenderFragment(builder =>
            {
                builder.OpenElement(8, "b");
                builder.AddContent(9, "Please configure FilterTemplate slot or the SettingColumn's FilterTemplate in the columns slot.");
                builder.CloseElement();
            });
        }
    }

    Task IGridViewTopbar.ForceRenderAsync() => InvokeAsync(() =>
    {
        _shouldRender = true;
        StateHasChanged();
    });

    Task IGridViewTopbar.ShowSettingAsync() => _settingSection.SafeChangStateAsync();



    IReadOnlyCollection<IGridViewColumn> IGridViewTopbar.GroupedColumns => _cachedGroupedColumns;

    async Task IGridViewTopbar.EndColumnDropToGroup(DragCustomEventArgs args)
    {
        if (string.IsNullOrWhiteSpace(args.TransferText))
            return;

        var column = _allColumns.FirstOrDefault(x => x.Identifier == args.TransferText);
        if (column is null)
            return;

        if (column.GetGroupable())
        {
            await AddGroup(column);
        }
    }

    async Task IGridViewTopbar.RemoveGroup(IGridViewColumn column)
    {
        if (_cachedGroupedColumns.Contains(column))
        {
            _cachedGroupedColumns.Remove(column);

            column.SetVisible(true);

            _groupedView = null;

            CaptureColumns();

            await RefreshContentAsync();
        }
    }



    IReadOnlyCollection<IGridViewColumn> IGridViewTopbar.SettableColumns => _cachedSettableColumns;

    async Task IGridViewTopbar.EndColumnSettingDrop(DragCustomEventArgs args)
    {
        if (string.IsNullOrWhiteSpace(args.TransferText))
        {
            return;
        }

        var column = _allColumns.Find(x => x.Identifier == args.TransferText);
        if (column is null)
        {
            return;
        }

        if (column.GetSettable() && !_cachedSettableColumns.Contains(column))
        {
            _cachedSettableColumns.Add(column);
            await _topbarSection.SafeChangStateAsync();
        }
    }

    async Task IGridViewTopbar.OnSettingClick(IGridViewColumn column, string type)
    {
        if (type == "sort")
        {
            if (column.GetSortable())
            {
                column.SetDirection(Direction.None);
                column.SetSortable(false);
            }
            else
            {
                column.SetSortable(true);
            }
        }
        else if (type == "order")
        {
            column.SetReorderable(!column.GetReorderable());
        }
        else if (type == "size")
        {
            column.SetResizable(!column.GetResizable());
        }
        else if (type == "group")
        {
            column.SetGroupable(!column.GetGroupable());
        }
        else if (type == "frozen")
        {
            var frozen = column.GetFixed();
            if (frozen == (false, false))
            {
                column.SetFixed((true, false));
            }
            else if (frozen == (true, false))
            {
                column.SetFixed((false, true));
            }
            else if (frozen.Tail)
            {
                column.SetFixed((false, false));
            }
        }

        await _tableSection.SafeChangStateAsync();
    }

    async Task IGridViewTopbar.RemoveSettableColumn(IGridViewColumn column)
    {
        if (_cachedSettableColumns.Contains(column))
        {
            _cachedSettableColumns.Remove(column);
            await _settingSection.SafeChangStateAsync();
        }
    }
}
