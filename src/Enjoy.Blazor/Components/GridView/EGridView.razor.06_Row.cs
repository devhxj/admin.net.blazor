﻿using System.Text;

namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    public async Task RowClickCallback(object data, MouseEventArgs args)
    {
        if (OnRowClick is not null)
            await OnRowClick(new(data, args));
    }

    public async Task RowDoubleClickCallback(object data, MouseEventArgs args)
    {
        if (OnRowDoubleClick is not null)
            await OnRowDoubleClick(new(data, args));
    }

    private string GetLineNo(EGridViewRow<TItem> row)
    {
        if (StateOption.IsTreeGridView)
        {
            return $"{row.TreeNode?.Level + 1}.{row.Index + 1}";
        }
        else
        {
            return $"{PagerOption.Skip + row.Index + 1}";
        }
    }

    private string? GetRowCssClass(EGridViewRow<TItem> row)
    {
        return new StringBuilder()
            .AddClass("e-grid-view__mobile-table-row", IsMobile)
            .AddClass("selected", RowSelectMode != Tristate.None && IsRowSelected(row.Item))
            .AddClass("editing", row.Editing)
            .AddClass(row.RenderArgs?.CssClass)
            .ToString();
    }

    private string? GetRowCssStyle(EGridViewRow<TItem> row)
    {
        return new StringBuilder()
            .AddClass(row.RenderArgs?.CssStyle)
            .AddClass("display:none;", row.EditLoading)
            .ToString();
    }

    private string? GetCellCssClass(IGridViewColumn column)
    {
        return new StringBuilder()
            .AddClass(column.GetCellCssClass())
            .AddClass(GetFrozenColumnClass(column))
            .AddClass("e-grid-view__mobile-row", IsMobile)
            .AddClass($"text-{column.GetTextAlign().ToString().ToLower()}", "text-center", column.GetCategory() == ColumnType.Data)
            .ToString();
    }

    private string? GetCellCssStyle(IGridViewColumn column)
    {
        return new StringBuilder()
            .AddStyle("min-height:32px;height:32px;", IsDense)
            .AddStyle(column.BodyDataCellFixedStyle, !IsMobile)
            .ToString();
    }

    private bool GetIndeterminate(EGridViewRow<TItem> row)
    {
        if (row.Editing || row.Adding)
            return false;

        if (StateOption.IsTreeGridView)
        {
            var selectedCount = GetChildSelectedCount(row.TreeNode!);
            return selectedCount > 0 && selectedCount < row.TreeNode!.ChildNodes.Count;
        }

        return false;
    }

    private bool GetSelected(EGridViewRow<TItem> row)
    {
        if (row.Adding)
        {
            return _cachedAddSelectedItems.Contains(row.Item);
        }

        return IsRowSelected(row.Item);
    }

    private async Task SetSelected(bool value, EGridViewRow<TItem> row)
    {
        if (row.Adding)
        {
            var selected = _cachedAddSelectedItems.Contains(row.Item);

            if (value && !selected)
            {
                _cachedAddSelectedItems.Add(row.Item);
            }
            else if (!value && selected)
            {
                _cachedAddSelectedItems.Remove(row.Item);
            }

            await _tableSection.SafeChangStateAsync();
        }
        else
        {
            await (StateOption.IsTreeGridView ? SelectTreeRow(row.TreeNode!, value) : SelectRow(row.Item, value));
        }
    }

    private Task HandleOnRowExpand(EGridViewRow<TItem> row) => ExpandItem(row.Item);

    private async Task HandleOnRowClick(MouseEventArgs args, EGridViewRow<TItem> row)
    {
        //单击延时，避免和双击冲突
        if (StateOption.IsRowDoubleClickable)
        {
            await Task.Delay(300);
            if (row.PreventClickCount > 0)
            {
                row.PreventClickCount--;
                return;
            }
        }

        try
        {
            if (StateOption.IsRowClickable)
                await RowClickCallback(row.Item, args);

            //SelectRow 中会执行 UpdateTableAsync
            if (ClickToSelect || RowSelectMode == Tristate.Single)
            {
                var selected = IsRowSelected(row.Item);
                await SelectRow(row.Item, !selected);
            }
        }
        catch { }
    }

    private async Task HandleOnRowDoubleClick(MouseEventArgs args, EGridViewRow<TItem> row)
    {
        try
        {
            //dblclick需要阻止click2次
            row.PreventClickCount = 2;

            if (StateOption.IsRowDoubleClickable)
                await RowDoubleClickCallback(row.Item, args);
        }
        catch { }
    }

    private async Task HandleOnTreeRowExpand(MouseEventArgs e, EGridViewRow<TItem> row)
    {
        if (row.TreeLoading)
            return;

        try
        {
            row.TreeLoading = true;

            // 增加延迟，避免快速重复点击
            if (!row.TreeNode!.Loaded)
            {
                await Task.Delay(500);
            }

            // 更新展开状态
            row.TreeNode.Expanded = !row.TreeNode.Expanded;

            // 加载子节点
            if (row.TreeNode.Expanded && !row.TreeNode.Loaded)
            {
                try
                {
                    if (row.TreeNode.Children is null)
                    {
                        if (OnLoadChildData is null)
                        {
                            row.TreeNode.Children = Enumerable.Empty<TItem>();
                        }
                        else
                        {
                            row.TreeNode.Children = await OnLoadChildData(row.Item);
                        }
                    }
                }
                finally
                {
                    row.TreeNode.Loaded = true;
                }

                //构造子节点
                if (row.TreeNode.Children is not null)
                {
                    foreach (var item in row.TreeNode.Children)
                    {
                        if (item is not null)
                        {
                            row.TreeNode.AddChildNode(GetTreeNode(item));
                        }
                    }
                }
            }

            // 刷新当前行
            StateHasChanged();

            // 如果当前节点被选中，展开时则子节点也要被选中
            // 如果当前节点被折叠，则选中的子节点要被取消
            await CascadeExpandSelectChildTreeRow(row.TreeNode);

            // 单展开模式，需要关闭其他节点
            if (row.TreeNode.Expanded && ExpandMode == Tristate.Single)
            {
                await CollapseOtherTreeNode(row.TreeNode);
            }
        }
        finally
        {
            row.TreeLoading = false;
        }
    }

    private decimal GetExpandIndent() => Math.Max(1, _props.TreeExpandIndent);

    private string GetExpandIcon(EGridViewRow<TItem> row)
    {
        return row.TreeNode!.Expanded
            ? _props.TreeExpandedIcon ?? "mdi-menu-down"
            : _props.TreeExpandableIcon ?? "mdi-menu-right";
    }

    protected string GetNodeExpandIconStyle(EGridViewRow<TItem> row)
    {
        return new StringBuilder()
            .AddStyle("visibility:hidden", !row.TreeNode!.Children?.Any() == true)
            .AddStyle($"margin-left: {(row.TreeNode.Level * GetExpandIndent())}rem", row.TreeNode.Level > 0)
            .ToString();
    }

    private int GetChildSelectedCount(GridViewTreeNode<TItem> node)
    {
        var selectedCount = 0;
        foreach (var child in node.ChildNodes)
            if (IsRowSelected(child.Data))
                selectedCount++;
        return selectedCount;
    }
}
