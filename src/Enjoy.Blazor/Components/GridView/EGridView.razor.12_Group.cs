﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    IEnumerable<GroupResult>? _groupedView;

    IEnumerable<GroupResult> GroupedView
    {
        get
        {
            if (_groupedView is null)
            {
                var view = _view.AsQueryable();
                var order = _cachedGroupedColumns.Where(c => c.GetDirection() != Direction.None);
                var query = order.Any()
                    ? view.OrderBy(string.Join(',', order.Select(c => $"np(it.{c.GetPropertyName()}) {c.GetDirectionAsString()}")))
                    : view;
                _groupedView = query.GroupByMany(_cachedGroupedColumns.Select(c => $"np(it.{c.GetPropertyName()})").ToArray()).ToList();
            }
            return _groupedView;
        }
    }

    async Task AddGroup(IGridViewColumn column)
    {
        if (!_cachedGroupedColumns.Contains(column))
        {
            _cachedGroupedColumns.Add(column);

            column.SetVisible(false);

            _groupedView = null;

            CaptureColumns();

            await RefreshContentAsync();
        }
    }

    public async Task ApplyGroup(string keyOrProperty)
    {
        if (string.IsNullOrWhiteSpace(keyOrProperty))
            return;

        var columns = _orphanColumns.Where(c => c.GetGroupable());
        var column = columns.FirstOrDefault(c => c.GetKey() == keyOrProperty)
            ?? columns.FirstOrDefault(c => c.GetPropertyName() == keyOrProperty);

        if (column is not null)
            await AddGroup(column);
    }
}
