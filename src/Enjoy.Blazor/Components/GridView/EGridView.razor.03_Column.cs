﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    private readonly List<IGridViewColumn> _allColumns = new();//所有列
    private readonly List<IGridViewColumn> _orphanColumns = new();//孤列，没有父列
    private readonly List<IGridViewColumn> _leafColumns = new();//可见叶子列
    private readonly List<IGridViewColumn> _cachedFilterableColumns = new();
    private readonly List<IGridViewColumn> _cachedSettableColumns = new();
    private readonly List<IGridViewColumn> _cachedGroupedColumns = new();
    private readonly List<IGridViewColumn> _cachedSortedColumns = new();

    IReadOnlyList<IGridViewColumn> IGridView.LeafColumns => _leafColumns;

    void IGridView.AddColumn(IGridViewColumn column)
    {
        var category = column.GetCategory();
        var hasParent = column.GetParent() is not null;

        if (!_orphanColumns.Contains(column) && !hasParent)
        {
            if (category == ColumnType.Index)
            {
                _orphanColumns.Insert(0, column);
            }
            else if (category == ColumnType.Select)
            {
                var index = 0;
                if (_orphanColumns.Count > 0 && _orphanColumns[0].GetCategory() == ColumnType.Index)
                {
                    index = 1;
                }

                _orphanColumns.Insert(index, column);
            }
            else
            {
                _orphanColumns.Add(column);
            }
        }

        if (hasParent)
        {
            //更新显示层级
            var level = column.GetLevel();
            if (level > StateOption.ColumnDeepestLevel)
            {
                StateOption.ColumnDeepestLevel = level;
            }
        }

        if (!_allColumns.Contains(column))
        {
            _allColumns.Add(column);
        }
    }

    void IGridView.UpdateSettings(IGridViewSetting setting)
    {
        _setting = setting;
    }

    private void CaptureColumns()
    {
        _leafColumns.Clear();

        //判断是否显示 序号列 和 选择列
        StateOption.HasIndexColumn = _allColumns.Exists(x => x.GetCategory() == ColumnType.Index);
        StateOption.HasSelectColumn = _allColumns.Exists(x => x.GetCategory() == ColumnType.Select);

        //统计列信息
        FlattenColumn(_orphanColumns);
        StateOption.CaptureColspan = _leafColumns.Count + _cachedGroupedColumns.Count + (ExpandTemplate is null ? 0 : 1);
        StateOption.CaptureSetable = _leafColumns.Exists(x => x.GetSettable());
        StateOption.CaptureGroupable = _cachedGroupedColumns.Count > 0 || _leafColumns.Exists(x => x.GetGroupable());
        StateOption.CaptureHasFooter = _leafColumns.Exists(x => x.GetFooterTemplate() is not null);
        StateOption.CaptureHasGroupFooter = _leafColumns.Exists(x => x.GetGroupFooterTemplate() is not null);

        //计算冻结
        for (var index = 0; index < _leafColumns.Count; index++)
        {
            var column = _leafColumns[index];
            var category = column.GetCategory();
            if (column.IsFixed() && category != ColumnType.Group)
            {
                if (category == ColumnType.Group)
                {
                    continue;
                }

                var headerFooterIndex = 2;
                var bodyDataCellIndex = 1;
                if (column.IsTailFixed())
                {

                    if (category == ColumnType.Tool)
                    {
                        headerFooterIndex += 1;
                        bodyDataCellIndex += 1;
                    }

                    var width = _leafColumns
                        .Where((c, i) => index < i && c.IsTailFixed())
                        .Sum(c => c.GetWidth());

                    column.UpdateFixedStyle(
                        $"z-index:{bodyDataCellIndex};right:{width}px;",
                        $"z-index:{headerFooterIndex};right:{width}px;");
                }
                else
                {
                    headerFooterIndex += 1;
                    bodyDataCellIndex += 1;

                    if (category == ColumnType.Index || category == ColumnType.Select)
                    {
                        headerFooterIndex += 1;
                        bodyDataCellIndex += 1;
                    }

                    var width = _leafColumns
                        .Where((c, i) => index > i && c.IsHeadFixed())
                        .Sum(c => c.GetWidth());

                    foreach (var g in _cachedGroupedColumns)
                    {
                        width += 36;
                    }

                    if (ExpandTemplate is not null)
                    {
                        width += 36;
                    }

                    column.UpdateFixedStyle(
                        $"z-index:{bodyDataCellIndex};left:{width}px;",
                        $"z-index:{headerFooterIndex};left:{width}px;");
                }
            }
        }

        void FlattenColumn(IEnumerable<IGridViewColumn> columns)
        {
            foreach (var column in columns)
            {
                if (!column.GetVisible())
                {
                    continue;
                }

                if (column.Children.Any())
                {
                    FlattenColumn(column.Children);
                }
                else
                {
                    _leafColumns.Add(column);
                }
            }
        }
    }

    private async Task HandleOnColumnResize(MouseCustomEventArgs args)
    {
        var column = _allColumns.Find(x => x.Identifier == args.TargetName);
        if (column is null || args.TargetAttributes is null)
        {
            return;
        }

        if (args.TargetAttributes.TryGetValue("colWidth", out int width))
        {
            if (width != column.GetWidth())
            {
                column.SetWidth(width);

                CaptureColumns();

                if (OnColumnResized is not null)
                {
                    await OnColumnResized(new(column, width));
                }

                //因为有滚动冻结，所以需要刷新
                await _tableSection.SafeChangStateAsync();
            }
        }
    }

    private async Task HandleOnColumnReorder(DragCustomEventArgs args)
    {
        if (string.IsNullOrWhiteSpace(args.TransferText))
        {
            return;
        }

        //判断位置是否发生变化
        if (args.TransferText == args.TargetName)
        {
            return;
        }

        var reorderFrom = _leafColumns.Find(x => x.Identifier == args.TransferText);
        var reorderTo = _leafColumns.Find(x => x.Identifier == args.TargetName);

        if (reorderFrom == reorderTo)
        {
            return;
        }

        if (reorderFrom is not null && reorderTo is not null)
        {
            var indexFrom = _leafColumns.IndexOf(reorderFrom);
            var indexTo = _leafColumns.IndexOf(reorderTo);

            var parentFrom = reorderFrom.GetParent();
            var parentTo = reorderTo.GetParent();

            if (parentFrom is not null)
            {
                if (parentFrom.Children.Count > 1)
                {
                    reorderFrom.SetParent(null);
                    parentFrom.RemoveColumn(reorderFrom);
                }
                else
                {
                    await PopupService.ConfirmAsync(
                        I18n.T("$enjoyBlazor.confirm.warning"),
                        I18n.T("$enjoyBlazor.gridView.columnReorder"),
                        AlertTypes.Warning);
                    return;
                }
            }
            else
            {
                _orphanColumns.Remove(reorderFrom);
            }

            if (parentTo is null)
            {
                _orphanColumns.Insert(_orphanColumns.IndexOf(reorderTo), reorderFrom);
            }
            else
            {
                reorderFrom.SetParent(parentTo);
                parentTo.InsertColumn(reorderFrom, reorderTo);
            }

            CaptureColumns();

            if (OnColumnReordered is not null)
            {
                await OnColumnReordered(new GridViewColumnReorderedEventArgs(reorderFrom, indexFrom, indexTo));
            }

            await _tableSection.SafeChangStateAsync();
        }
    }
}
