﻿using System.ComponentModel;

namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    [CascadingParameter(Name = "IsDark")]
    private bool CascadingIsDark { get; set; }

    [Parameter]
    [Description("是否深色模式")]
    public bool Dark { get; set; }

    [Parameter]
    [Description("是否明亮模式")]
    public bool Light { get; set; }

    [Parameter]
    [Description("是否紧凑显示")]
    public bool Dense { get; init; }

    [Parameter]
    [Description("是否显示边框")]
    public bool Border { get; init; }

    [Parameter]
    [Description("是否显示斑马纹")]
    public bool Stripe { get; init; }

    [Parameter]
    [Description("文字超出宽度是否自动换行")]
    public bool TextWrap { get; init; }

    [Parameter]
    [Description("自定义显示样式")]
    public string? Class { get; set; }

    [Parameter]
    [Description("自定义显示式样")]
    public string? Style { get; set; }

    [Parameter]
    [Description("用于设置移动视图切换宽度Pixels(px)")]
    public double MobileBreakWidth { get; set; } = 540;

    [Parameter]
    [Description("标题")]
    public string? Title { get; init; }

    [Parameter]
    [Description("默认列宽 Pixels(px)，默认200px，最小60px")]
    public int DefaultColumnWidth { get; init; } = 200;

    [Parameter]
    [Description("用于显示可本地化数据（数字、日期）的区域性")]
    public CultureInfo DefaultCulture { get; init; } = CultureInfo.CurrentCulture;

    [Parameter]
    [Description("默认日期格式，默认值为：yyyy-MM-dd")]
    public string DefaultDateFormat { get; init; } = "yyyy-MM-dd";

    [Parameter]
    [Description("默认时间格式，默认值为：HH:mm:ss")]
    public string DefaultTimeFormat { get; init; } = "HH:mm:ss";

    [Parameter]
    [Description("默认逻辑运算，默认为或")]
    public Logical DefaultLogicaler { get; init; } = Logical.And;

    /// <summary>
    /// 获得/设置 弹窗宽度 默认为 480px
    /// </summary>
    [Parameter]
    public int PopupDialogWidth { get; set; } = 480;

    [Parameter]
    [Description("是否虚拟化滚动")]
    public bool AllowVirtualization { get; init; }

    [Parameter]
    [Description("当数据为空集合时显示的内容")]
    public RenderFragment? EmptyTemplate { get; init; }

    [Parameter]
    [Description("展开模式")]
    public Tristate ExpandMode { get; init; }

    [Parameter]
    [Description("是否支持表头筛选")]
    public bool AllowHeadFiltering { get; init; }

    [Parameter]
    [Description("是否支持关键字筛选")]
    public bool AllowKeywordsFiltering { get; init; }

    [Parameter]
    [Description("是否支持动态筛选")]
    public bool AllowDynamicFiltering { get; init; }

    [Parameter]
    [Description("是否支持自定义筛选")]
    public bool AllowCustomFiltering { get; init; }

    [Parameter]
    [Description("是否支持行间筛选")]
    public bool AllowRowFiltering { get; init; }

    [Parameter]
    [Description("是否禁用行间筛选操作符")]
    public bool HideRowFilterAction { get; init; }

    [Parameter]
    [Description("是否可顶部分页")]
    public bool AllowTopPaging { get; init; }

    [Parameter]
    [Description("是否可底部分页")]
    public bool AllowBottomPaging { get; init; }

    [Parameter]
    [Description("每页显示记录数，分页时有效，默认10条")]
    public int ItemsPerPage
    {
        get => PagerOption.ItemsPerPage;
        set => PagerOption.ItemsPerPage = value;
    }

    [Parameter]
    [Description("可点击页码数量，分页时有效，默认5个")]
    public int PaginationLength
    {
        get => PagerOption.PaginationLength;
        set => PagerOption.PaginationLength = value;
    }

    [Parameter]
    [Description("圆形分页按钮，分页时有效，默认圆形")]
    public bool CircularPagingButton
    {
        get => PagerOption.CircularPagingButton;
        set => PagerOption.CircularPagingButton = value;
    }

    [Parameter]
    [Description("每页显示记录数选项，分页时有效，默认10、20、30")]
    public IList<int> ItemsPerPageOptions
    {
        get => PagerOption.ItemsPerPageOptions;
        set => PagerOption.ItemsPerPageOptions = value;
    }

    [Parameter]
    [Description("数据行选择模式")]
    public Tristate RowSelectMode { get; init; } = Tristate.None;

    [Parameter]
    [Description("是否点击行即选中本行")]
    public bool ClickToSelect { get; init; }

    [Parameter]
    [Description("是否支持多列排序")]
    public bool MultiSort { get; init; }

    /// <summary>
    /// 获取/设置 列模板配置。
    /// </summary>
    [Parameter]
    public RenderFragment<TItem>? Columns { get; init; }

    /// <summary>
    /// 获得/设置 独立编辑（非行内编辑）模板
    /// </summary>
    [Parameter]
    public RenderFragment<IEditor<TItem>>? EditTemplate { get; init; }

    /// <summary>
    /// 获得/设置 自定义筛选模板
    /// </summary>
    [Parameter]
    public RenderFragment<IGridViewFilterContext<TItem>>? FilterTemplate { get; init; }

    [Parameter]
    [Description("分组头模板")]
    public RenderFragment<GridViewGroup?>? GroupHeaderTemplate { get; set; }

    /// <summary>
    /// 获得/设置 显示数据项。
    /// </summary>
    [Parameter]
    public IEnumerable<TItem>? Data
    {
        get => _data;
        set
        {
            if (_data != value)
            {
                _data = value;
                _processDataFlag = true;
            }
        }
    }

    /// <summary>
    /// 获得/设置 比较数据是否相同回调方法 默认为 null
    /// </summary>
    [Parameter]
    public Func<TItem, TItem, bool>? EqualityComparer { get; set; }

    /// <summary>
    /// 获取/设置 树节点转换器
    /// </summary>
    [Parameter]
    public Action<GridViewTreeNode<TItem>>? TreeNodeConverter { get; init; }

    /// <summary>
    /// 获取/设置 子节点属性名称
    /// </summary>
    [Parameter]
    public string? ChildrenProperty { get; init; }

    /// <summary>
    /// 获取/设置 树节点选择框
    /// </summary>
    [Parameter]
    public bool TreeCheckable { get; init; }

    /// <summary>
    /// 获得/设置 内容模板。
    /// </summary>
    [Parameter]
    public RenderFragment<TItem>? ExpandTemplate { get; set; }

    /// <summary>
    /// 获取/设置 当前勾选数据项。
    /// </summary>
    [Parameter]
    public List<TItem>? Value
    {
        get => _value;
        set
        {
            if (_value != value)
            {
                _value = value;
                _updateValueFlag = true;
            }
        }
    }

    /// <summary>
    /// 获取/设置 勾选数据项变化回调。
    /// </summary>
    [Parameter]
    public EventCallback<List<TItem>?> ValueChanged { get; set; }

    /// <summary>
    /// 获得/设置 工具栏操作按钮模板
    /// </summary>
    [Parameter]
    public RenderFragment? Actions { get; set; }






    /// <summary>
    /// 获取/设置 远程数据加载方法
    /// </summary>
    [Parameter]
    public Func<GridViewLoadDataArgs, Task<(int, IEnumerable<TItem>)>>? OnLoadData { get; init; }

    /// <summary>
    /// 获得/设置 权限键值加载方法
    /// </summary>
    [Parameter]
    public Func<Task<IEnumerable<string>>>? OnLoadPermissionAsync { get; set; }

    /// <summary>
    /// 获取/设置 加载子数据回调
    /// </summary>
    [Parameter]
    public Func<TItem, Task<IEnumerable<TItem>?>>? OnLoadChildData { get; init; }

    /// <summary>
    /// 获得/设置 保存按钮异步回调方法
    /// </summary>
    [Parameter]
    public Func<IEnumerable<TItem>, bool, Task<bool>>? OnSaveAsync { get; set; }

    /// <summary>
    /// 获得/设置 删除按钮异步回调方法
    /// </summary>
    [Parameter]
    public Func<IEnumerable<TItem>, Task<bool>>? OnDeleteAsync { get; set; }

    /// <summary>
    /// 获得/设置 导出按钮异步回调方法
    /// </summary>
    [Parameter]
    public Func<GridViewLoadDataArgs, Task>? OnExportAsync { get; set; }

    /// <summary>
    /// 获取/设置 拖拽调整列顺序回调。
    /// </summary>
    [Parameter]
    public Func<GridViewColumnReorderedEventArgs, Task>? OnColumnReordered { get; set; }

    /// <summary>
    /// 获取/设置 拖拽调整列宽回调。
    /// </summary>
    [Parameter]
    public Func<GridViewColumnResizedEventArgs, Task>? OnColumnResized { get; set; }

    /// <summary>
    /// 获取/设置 行单击回调。
    /// </summary>
    [Parameter]
    public Func<GridViewRowMouseEventArgs<TItem>, Task>? OnRowClick { get; set; }

    /// <summary>
    /// 获取/设置 行双击回调。
    /// </summary>
    [Parameter]
    public Func<GridViewRowMouseEventArgs<TItem>, Task>? OnRowDoubleClick { get; set; }

    /// <summary>
    /// 获取/设置 行渲染回调，用于设置行组件属性。
    /// </summary>
    [Parameter]
    public Action<GridViewRowRenderEventArgs<TItem>>? OnBeforeRowRender { get; set; }

    /// <summary>
    /// 获取/设置 渲染时的回调。
    /// </summary>
    [Parameter]
    public Action<GridViewRenderEventArgs>? OnAfterRenderComplete { get; set; }

    /// <summary>
    /// 获取/设置 单元格渲染回调，用于设置单元格属性。
    /// </summary>
    [Parameter]
    public Action<GridViewCellRenderEventArgs<TItem>>? OnBeforeCellRender { get; set; }

    /// <summary>
    /// 获取/设置 GridView表头单元格渲染回调，用于设置GridView表头单元格属性。
    /// </summary>
    [Parameter]
    public Action<GridViewHeaderCellRenderEventArgs>? OnBeforeHeaderCellRender { get; set; }

    /// <summary>
    /// 获取/设置 GridView表尾单元格渲染回调，用于设置GridView表尾单元格属性。
    /// </summary>
    [Parameter]
    public Action<GridViewFooterCellRenderEventArgs>? OnBeforeFooterCellRender { get; set; }

    /// <summary>
    /// 获取/设置 列显示控制已更改回调。
    /// </summary>
    [Parameter]
    public EventCallback<GridViewColumnPickedEventArgs> OnColumnPicked { get; set; }

    /// <summary>
    /// 获取/设置 行展开回调。
    /// </summary>
    [Parameter]
    public Func<TItem, Task>? OnRowExpand { get; set; }

    /// <summary>
    /// 获取/设置 行折叠模式。
    /// </summary>
    [Parameter]
    public Func<TItem, Task>? OnRowCollapse { get; set; }

    /// <summary>
    /// 获取/设置 行选择回调。
    /// </summary>
    [Parameter]
    public Func<TItem, Task>? OnRowSelect { get; set; }

    /// <summary>
    /// 获取/设置 行取消选择回调。
    /// </summary>
    [Parameter]
    public Func<TItem, Task>? OnRowDeselect { get; set; }

    /// <summary>
    /// 获取/设置 拖拽分组行渲染回调，用于设置行属性。
    /// </summary>
    [Parameter]
    public Action<GridViewGroupFirstRenderEventArgs>? OnBeforeGroupRowFirstRender { get; set; }

    [Parameter]
    [Description("细节属性控制")]
    public Action<GridViewSetting>? SettingProps { get; init; }
}
