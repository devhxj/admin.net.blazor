﻿namespace Enjoy.Blazor;

public class GridViewFilter : Filter
{
    private IGridViewColumn? _column;

    internal IGridViewColumn? Column
    {
        get => _column;
        set
        {
            if (_column != value)
            {
                _column = value;
                Reset();
            }
        }
    }

    public override string? Key => _column?.GetPropertyName();

    public override Type? Type => _column?.GetPropertyType();

    public override bool? CaseInsensitive => _column?.GetCaseInsensitive();
}