﻿namespace Enjoy.Blazor;

public class GridViewPagerOption
{
    internal int ItemsPerPage { get; set; } = 10;

    internal int PaginationLength { get; set; } = 5;

    internal bool CircularPagingButton { get; set; } = true;

    internal IList<int> ItemsPerPageOptions { get; set; } = new int[] { 10, 20, 50, 500, 5000, 50000 };

    internal int Skip { get; set; }

    internal int Page { get; set; }

    internal int NumberOfPages { get; set; }

    internal int StartPage { get; set; }

    internal int EndPage { get; set; }

    internal int TotalRecordCount { get; set; }
}
