﻿namespace Enjoy.Blazor;

public class GridViewStateOption
{
    internal int ColumnDeepestLevel { get; set; }

    internal bool HasIndexColumn { get; set; }
    internal bool HasSelectColumn { get; set; }
    internal bool HasExpandTemplate { get; set; }
    internal bool IsDenseStyle { get; set; }
    internal bool IsMobileMode { get; set; }
    internal bool IsCustomFilterExpanded { get; set; }
    internal bool IsTreeGridView { get; set; }
    internal bool IsRowClickable { get; set; }
    internal bool IsRowDoubleClickable { get; set; }
    internal bool IsAccessControl { get; set; }
    internal bool SettingVisible { get; set; }

    internal bool CaptureSetable { get; set; }
    internal bool CaptureGroupable { get; set; }
    internal bool CaptureHasFooter { get; set; }
    internal bool CaptureHasGroupFooter { get; set; }

    internal int CaptureColspan { get; set; }
}
