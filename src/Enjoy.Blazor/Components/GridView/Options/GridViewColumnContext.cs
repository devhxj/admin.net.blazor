﻿namespace Enjoy.Blazor;

/// <summary>
/// GridViewColumn 上下文类
/// </summary>
public class GridViewColumnContext<TItem, TValue>
{
    public GridViewColumnContext(TItem model, TValue value)
    {
        Row = model ?? throw new ArgumentNullException(nameof(model));
        Value = value;
    }

    /// <summary>
    /// 获得 行数据实例
    /// </summary>
    [NotNull]
    public TItem Row { get; }

    /// <summary>
    /// 获得/设置 当前绑定字段数据实例
    /// </summary>
    public TValue Value { get; }
}

/// <summary>
/// GridViewColumn 上下文类
/// </summary>
public class GridViewColumnEditContext<TItem, TValue>
{
    private readonly Func<TItem, TValue> _getter;
    private readonly Action<TItem, TValue> _setter;

    public GridViewColumnEditContext(bool isAdd, TItem model, Func<TItem, TValue> getter, Action<TItem, TValue> setter)
    {
        IsAdd = isAdd;
        Row = model ?? throw new ArgumentNullException(nameof(model));
        _getter = getter;
        _setter = setter;
    }

    /// <summary>
    /// 获得/设置 是否新增
    /// </summary>
    public bool IsAdd { get; }

    /// <summary>
    /// 获得 行数据实例
    /// </summary>
    [NotNull]
    public TItem Row { get; }

    /// <summary>
    /// 获得/设置 当前绑定字段数据实例
    /// </summary>
    public TValue Value
    {
        get => _getter(Row);
        set => _setter(Row, value);
    }
}