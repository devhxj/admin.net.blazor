﻿namespace Enjoy.Blazor;

/// <summary>
/// 分组对象
/// </summary>
public class GridViewGroup
{
    public GridViewGroup(IGridViewColumn column, int level, GroupResult? data)
    {
        Column = column;
        Level = level;
        Data = data;
    }

    /// <summary>
    /// 获取/设置 分组数据
    /// </summary>
    public GroupResult? Data { get; }

    /// <summary>
    /// 获取/设置 分组列
    /// </summary>
    public IGridViewColumn Column { get; }

    /// <summary>
    /// 获取/设置 分组层级
    /// </summary>
    public int Level { get; }
}
