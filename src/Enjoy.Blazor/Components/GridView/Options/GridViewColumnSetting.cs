﻿namespace Enjoy.Blazor;

/// <summary>
/// <see cref="EGridView{TItem}" /> 组件列参数 
/// </summary>
public class GridViewColumnSetting
{
    /// <summary>
    /// 获得/设置 第1个筛选值
    /// </summary>
    public object? RowFilterValue { get; set; }

    /// <summary>
    /// 获得/设置 第1个筛选操作符
    /// </summary>
    public Comparer RowFilterComparator { get; set; }

    /// <summary>
    /// 获取 逻辑运算操作符
    /// </summary>
    public Logical HeadFilterLogicaler { get; set; }

    /// <summary>
    /// 获得/设置 第1个筛选值
    /// </summary>
    public object? HeadFilter1Value { get; set; }

    /// <summary>
    /// 获得/设置 第1个筛选操作符
    /// </summary>
    public Comparer HeadFilter1Comparator { get; set; }

    /// <summary>
    /// 获得/设置 第2个筛选值
    /// </summary>
    public object? HeadFilter2Value { get; set; }

    /// <summary>
    /// 获得/设置 第2个筛选操作符
    /// </summary>
    public Comparer HeadFilter2Comparator { get; set; }
}
