﻿namespace Enjoy.Blazor;

public sealed class GridViewFilterContext<TItem>
{
    private readonly IGridViewFilterContext _context;

    [NotNull]
    public TItem Value { get; }

    public GridViewFilterContext(IGridViewFilterContext context, TItem value)
    {
        _context = context;
        Value = value ?? throw new ArgumentNullException(nameof(value));
    }

    /// <summary>
    /// 重置筛选表单
    /// </summary>
    public EventCallback Reset() => _context.Reset();

    /// <summary>
    /// 应用筛选
    /// </summary>
    public EventCallback ApplyFilter() => _context.ApplyFilter();

    /// <summary>
    /// 清空筛选
    /// </summary>
    public EventCallback ClearFilter() => _context.ClearFilter();
}