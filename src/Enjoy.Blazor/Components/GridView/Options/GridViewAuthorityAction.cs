﻿namespace Enjoy.Blazor;

public class GridViewAuthorityAction
{
    internal GridViewAuthorityAction(MButton button,
        Action @lock,
        Action unLock,
        Func<bool> isLocking,
        EventCallback<MouseEventArgs> click)
    {
        Button = button;
        Lock = @lock;
        UnLock = unLock;
        IsLocking = isLocking;
        Click = click;
    }

    internal MButton Button { get; }

    internal Action Lock { get; }

    internal Action UnLock { get; }

    internal Func<bool> IsLocking { get; }

    internal EventCallback<MouseEventArgs> Click { get; }
}
