﻿namespace Enjoy.Blazor;

/// <summary>
/// <see cref="IGridViewColumn{TItem}"/> 表格列组件基类
/// </summary>
/// <typeparam name="TItem">绑定数据对象</typeparam>
public class ColumnGroup<TItem> : ColumnBase<TItem>
{
    /// <summary>
    /// 获取/设置 父列实例
    /// </summary>
    [CascadingParameter]
    public IGridViewColumn? Parent
    {
        get => _parent;
        init => _parent = value;
    }

    /// <summary>
    /// 获取/设置 当前列标题
    /// </summary>
    [Parameter]
    public string? Title
    {
        get => _title;
        init => _title = value;
    }

    /// <summary>
    /// 获取/设置 子列模板
    /// </summary>
    [Parameter]
    public RenderFragment? ChildContent
    {
        get => _columns;
        init => _columns = value;
    }

    /// <summary>
    /// 获取/设置 表头单元格样式
    /// </summary>
    [Parameter]
    public string? HeaderCellCssClass
    {
        get => _headerCellCssClass;
        init => _headerCellCssClass = value;
    }

    public ColumnGroup() : base()
    {
        _headerAlign = DataTableHeaderAlign.Center;
    }

    public override ColumnType GetCategory() => ColumnType.Group;
}
