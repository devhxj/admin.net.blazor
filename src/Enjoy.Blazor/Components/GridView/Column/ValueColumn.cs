﻿using System.ComponentModel;

namespace Enjoy.Blazor;

/// <summary>
/// 数据列组件，必须放置在 <see cref="EGridView{TItem}" />组件Column 模板中
/// </summary>
/// <typeparam name="TItem">数据对象类型</typeparam>
public abstract partial class ValueColumn<TItem> : ColumnBase<TItem>
{
    /// <summary>
    /// 获取/设置 当前列标题
    /// </summary>
    [Parameter]
    public string? Title
    {
        get => _title;
        init => _title = value;
    }

    /// <summary>
    /// 获取/设置 绑定属性名称
    /// </summary>
    [Parameter]
    public string? PropertyName
    {
        get => _propertyName;
        init => _propertyName = value;
    }

    /// <summary>
    /// 获取/设置 当前数据类型
    /// </summary>
    [Parameter]
    public Type? PropertyType
    {
        get => _propertyType;
        init => _propertyType = value;
    }

    /// <summary>
    /// 获取/设置 当前列是否可配置，默认为 false。
    /// </summary>
    [Parameter]
    public bool Settable
    {
        get => _settable;
        init => _settable = value;
    }

    /// <summary>
    /// 获取/设置 父列实例
    /// </summary>
    [CascadingParameter]
    public IGridViewColumn? Parent
    {
        get => _parent;
        init => _parent = value;
    }

    /// <summary>
    /// 获取/设置 子列模板
    /// </summary>
    [Parameter]
    public RenderFragment? Columns
    {
        get => _columns;
        init => _columns = value;
    }

    /// <summary>
    /// 获取/设置 当前列是否可见，默认为 true
    /// </summary>
    [Parameter]
    public bool Visible
    {
        get => _visible;
        init => _visible = value;
    }

    /// <summary>
    /// 获取/设置 是否支持冻结，默认不冻结。
    /// </summary>
    [Parameter]
    public bool Fixed
    {
        get => _fixed;
        init => _fixed = value;
    }

    /// <summary>
    /// 获取/设置 指示是否尾部冻结，默认不冻结。
    /// </summary>
    [Parameter]
    public bool Tail
    {
        get => _tail;
        init => _tail = value;
    }

    /// <summary>
    /// 获取/设置 数据单元格样式
    /// </summary>
    [Parameter]
    public string? CellCssClass
    {
        get => _cellCssClass;
        init => _cellCssClass = value;
    }

    /// <summary>
    /// 获取/设置 表头单元格样式
    /// </summary>
    [Parameter]
    public string? HeaderCellCssClass
    {
        get => _headerCellCssClass;
        init => _headerCellCssClass = value;
    }

    /// <summary>
    /// 获取/设置 表尾单元格样式
    /// </summary>
    [Parameter]
    public string? FooterCellCssClass
    {
        get => _footerCellCssClass;
        init => _footerCellCssClass = value;
    }

    /// <summary>
    /// 获取/设置 表头模板
    /// </summary>
    [Parameter]
    public RenderFragment? HeaderTemplate
    {
        get => _headerTemplate;
        init => _headerTemplate = value;
    }

    /// <summary>
    /// 获取/设置 表尾模板
    /// </summary>
    [Parameter]
    public RenderFragment? FooterTemplate
    {
        get => _footerTemplate;
        init => _footerTemplate = value;
    }

    /// <summary>
    /// 获取 业务标识，用户自定义使用
    /// </summary>
    [Parameter]
    public string? Key
    {
        get => _key;
        init => _key = value;
    }

    /// <summary>
    /// 获取/设置 当前列列宽 Pixels(px)。
    /// </summary>
    [Parameter]
    public int? Width
    {
        get => _width;
        init => _width = value;
    }

    /// <summary>
    /// 获取/设置 当前列最小列宽 Pixels(px)。
    /// </summary>
    [Parameter]
    public int? MinWidth
    {
        get => _minWidth;
        init => _minWidth = value;
    }

    /// <summary>
    /// 获取/设置 当前列最大列宽 Pixels(px)。
    /// </summary>
    [Parameter]
    public int? MaxWidth
    {
        get => _maxWidth;
        init => _maxWidth = value;
    }

    /// <summary>
    /// 获取/设置 表头单元格内容对齐方式
    /// </summary>
    [Parameter]
    public DataTableHeaderAlign HeaderAlign
    {
        get => _headerAlign;
        init => _headerAlign = value;
    }

    /// <summary>
    /// 获取/设置 内容对齐方式
    /// </summary>
    [Parameter]
    public DataTableHeaderAlign Align
    {
        get => _align;
        init => _align = value;
    }

    /// <summary>
    /// 获取/设置 是否支持调整列宽，默认为 false。
    /// </summary>
    [Parameter]
    public bool Resizable
    {
        get => _resizable;
        init => _resizable = value;
    }

    /// <summary>
    /// 获取/设置 是否支持重排序，默认为 false。
    /// </summary>
    [Parameter]
    public bool Reorderable
    {
        get => _reorderable;
        init => _reorderable = value;
    }

    /// <summary>
    /// 获取/设置 是否支持展开，默认为 false。
    /// </summary>
    [Parameter]
    public bool Expandable
    {
        get => _expandable;
        init => _expandable = value;
    }

    /// <summary>
    /// 获得/设置 格式化字符串 如时间类型设置 yyyy-MM-dd
    /// </summary>
    [Parameter]
    public string? FormatString
    {
        get => _formatString;
        init => _formatString = value;
    }

    /// <summary>
    /// 获取/设置 指示当前 <see cref="ValueColumn{TItem}"/> 是否支持筛选，默认为 true。
    /// </summary>
    [Parameter]
    public bool Filterable
    {
        get => _filterable;
        init => _filterable = value;
    }

    /// <summary>
    /// 获取/设置 指示当前 <see cref="ValueColumn{TItem}"/> 是否不区分大小写，默认为 true。
    /// </summary>
    [Parameter]
    public bool CaseInsensitive
    {
        get => _caseInsensitive;
        init => _caseInsensitive = value;
    }

    /// <summary>
    /// 获取/设置 筛选内容模板
    /// </summary>
    [Parameter]
    public RenderFragment<IGridViewColumnFilter>? FilterTemplate
    {
        get => _filterTemplate;
        init => _filterTemplate = value;
    }

    /// <summary>
    /// 获取/设置 逻辑运算操作符
    /// </summary>
    [Parameter]
    public Logical DefaultLogic
    {
        get => _logicaler;
        init => _logicaler = value;
    }

    /// <summary>
    /// 获取/设置 是否支持分组
    /// </summary>
    [Parameter]
    public bool Groupable
    {
        get => _groupable;
        init => _groupable = value;
    }

    /// <summary>
    /// 获取/设置 分组尾内容模板
    /// </summary>
    [Parameter]
    public RenderFragment<GridViewGroup>? GroupFooterTemplate
    {
        get => _groupFooterTemplate;
        init => _groupFooterTemplate = value;
    }

    /// <summary>
    /// 获取/设置  分组尾单元格样式
    /// </summary>
    [Parameter]
    public string? GroupFooterCellCssClass
    {
        get => _groupFooterCellCssClass;
        init => _groupFooterCellCssClass = value;
    }

    /// <summary>
    /// 获取/设置 是否支持排序
    /// </summary>
    [Parameter]
    public bool Sortable
    {
        get => _sortable;
        init => _sortable = value;
    }

    /// <summary>
    /// 获取/设置 当前列数据排序方式
    /// </summary>
    [Parameter]
    public Direction Order
    {
        get => _direction;
        init => _direction = value;
    }

    [Parameter]
    [Description("细节属性控制")]
    public Action<GridViewColumnSetting>? SettingProps
    {
        get => _settingProps;
        init => _settingProps = value;
    }

    protected override void OnInitialized()
    {
        //初始化绑定属性
        if (!string.IsNullOrWhiteSpace(_propertyName))
        {
            _propertyType ??= PropertyAccess.GetPropertyType(typeof(TItem), _propertyName);
        }

        base.OnInitialized();
    }
}
