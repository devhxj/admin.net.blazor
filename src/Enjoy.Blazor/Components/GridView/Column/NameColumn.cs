﻿namespace Enjoy.Blazor;

/// <summary>
/// 数据列组件，必须放置在 <see cref="EGridView{TItem}" />组件Column 模板中
/// </summary>
/// <typeparam name="TItem">数据对象类型</typeparam>
public partial class NameColumn<TItem> : ValueColumn<TItem>
{
    private Func<TItem, object?>? _valueGetter;
    private Action<TItem, object?>? _valueSetter;

    /// <summary>
    /// 获取/设置 内容模板，Context为 <see cref="{TItem}"/>
    /// </summary>
    [Parameter]
    public RenderFragment<GridViewColumnContext<TItem, object?>>? Template { get; init; }

    /// <summary>
    /// 获取/设置 编辑模板，Context为 <see cref="{TItem}"/>
    /// </summary>
    [Parameter]
    public RenderFragment<GridViewColumnEditContext<TItem, object?>>? EditTemplate { get; init; }

    /// <summary>
    /// 获得/设置 列格式化回调委托
    /// </summary>
    [Parameter]
    public Func<object?, string>? Formatter { get; init; }

    protected override void OnInitialized()
    {
        if (string.IsNullOrWhiteSpace(_propertyName))
        {
            throw new ArgumentNullException(nameof(_propertyName));
        }
        else
        {
            _valueGetter ??= PropertyAccess.Getter<TItem, object?>(_propertyName);
            _valueSetter ??= PropertyAccess.Setter<TItem, object?>(_propertyName);
        }

        base.OnInitialized();
    }

    public override string? GetStringValue(object data)
    {
        object? value = null;

        if (_valueGetter is null)
        {
            var propertyName = PropertyName;
            if (!string.IsNullOrWhiteSpace(propertyName))
            {
                value = PropertyAccess.GetPropertyValue(data, propertyName);
            }
        }
        else if (data is TItem item)
        {
            value = _valueGetter(item);
        }

        if (Formatter is not null)
        {
            return Formatter(value);
        }
        else if (!string.IsNullOrWhiteSpace(_formatString))
        {
            return string.Format(_formatString, value, GridView.DefaultCulture);
        }
        else
        {
            return value is null ? null : Convert.ToString(value, GridView.DefaultCulture);
        }
    }

    public override RenderFragment? InvokeTemplate(object data)
    {
        if (Template is null)
        {
            return new RenderFragment(builder => builder.AddContent(0, GetStringValue(data)));
        }

        var item = (TItem)data;
        return Template(new(item, _valueGetter!(item)));
    }

    public override RenderFragment? InvokeEditTemplate(IGridViewRow row)
    {
        if (EditTemplate is null)
        {
            return null;
        }

        if (row.Editing)
        {
            return EditTemplate(new(false, (TItem)row.EditItem!, _valueGetter!, _valueSetter!));
        }
        else if (row.Adding)
        {
            return EditTemplate(new(true, (TItem)row.Item, _valueGetter!, _valueSetter!));
        }
        else
        {
            return null;
        }
    }
}
