﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Enjoy.Blazor;

/// <summary>
/// 绑定列组件，必须放置在 <see cref="EGridView{TItem}" />组件Column 模板中
/// </summary>
/// <typeparam name="TItem">数据对象类型</typeparam>
/// <typeparam name="TType">绑定字段类型</typeparam>
public class BindColumn<TItem, TType> : ValueColumn<TItem>
{
    private Func<TItem, TType>? _valueGetter;
    private Action<TItem, TType>? _valueSetter;

    /// <summary>
    /// 获取/设置 绑定属性
    /// </summary>
    [Parameter]
    [MaybeNull]
    public TType? Property { get; set; }

    /// <summary>
    /// 获取/设置 绑定属性变化回调
    /// </summary>
    [Parameter]
    public EventCallback<TType> PropertyChanged { get; init; }

    /// <summary>
    /// 获得/设置 绑定属性表达式
    /// </summary>
    [Parameter]
    public Expression<Func<TType>>? PropertyExpression { get; set; }

    /// <summary>
    /// 获取/设置 内容模板，Context为 <see cref="{TItem}"/>
    /// </summary>
    [Parameter]
    public RenderFragment<GridViewColumnContext<TItem, TType>>? Template { get; init; }

    /// <summary>
    /// 获取/设置 编辑模板，Context为 <see cref="{TItem}"/>
    /// </summary>
    [Parameter]
    public RenderFragment<GridViewColumnEditContext<TItem, TType>>? EditTemplate { get; init; }

    /// <summary>
    /// 获得/设置 列格式化回调委托
    /// </summary>
    [Parameter]
    public Func<TType, string>? Formatter { get; init; }

    /// <inheritdoc />
    protected override void OnInitialized()
    {
        if (PropertyExpression is not null)
        {
            _fieldIdentifier = FieldIdentifier.Create(PropertyExpression);
        }

        _propertyType = typeof(TType);

        InitializeBindProperty();
        InitializeBindTitle();

        base.OnInitialized();
    }

    /// <summary>
    /// 初始化绑定字段
    /// </summary>
    private void InitializeBindProperty()
    {
        if (string.IsNullOrWhiteSpace(_propertyName))
        {
            var fields = new List<string>();
            Expression? express = PropertyExpression;

            while (express is LambdaExpression lambda)
            {
                express = lambda.Body;
            }

            while (express is MemberExpression member)
            {
                if (member.Expression is MemberExpression)
                {
                    fields.Add(member.Member.Name);
                }
                express = member.Expression;
            }

            if (fields.Any())
            {
                fields.Reverse();
                _propertyName = string.Join(".", fields);
            }
            else
            {
                _propertyName = _fieldIdentifier?.FieldName;
            }
        }

        if (string.IsNullOrWhiteSpace(_propertyName))
        {
            throw new ArgumentNullException(nameof(_propertyName));
        }

        _valueGetter ??= PropertyAccess.Getter<TItem, TType>(_propertyName);
        _valueSetter ??= PropertyAccess.Setter<TItem, TType>(_propertyName);
    }

    /// <summary>
    /// 初始化绑定标题
    /// </summary>
    private void InitializeBindTitle()
    {
        if (!string.IsNullOrWhiteSpace(Title))
        {
            return;
        }

        var propertyName = PropertyName;
        if (!string.IsNullOrWhiteSpace(propertyName))
        {
            var type = typeof(TItem);
            var valueType = Nullable.GetUnderlyingType(type) ?? type;

            //从绑定字段中读取
            if (_fieldIdentifier is not null)
            {
                if (valueType.IsEnum)
                {
                    var fieldInfo = valueType.GetFields().FirstOrDefault(p => p.Name == propertyName);
                    if (fieldInfo is not null)
                    {
                        _title = fieldInfo.GetCustomAttribute<DisplayAttribute>(true)?.Name
                            ?? fieldInfo.GetCustomAttribute<DisplayNameAttribute>(true)?.DisplayName
                            ?? fieldInfo.GetCustomAttribute<DescriptionAttribute>(true)?.Description;
                    }
                }
                else
                {
                    var props = valueType.GetProperties().AsEnumerable();

                    // 支持 MetadataType
                    var metadataType = valueType.GetCustomAttribute<MetadataTypeAttribute>(false);
                    if (metadataType is not null)
                    {
                        props = props.Concat(metadataType.MetadataClassType.GetProperties());
                    }

                    var propertyInfo = props.FirstOrDefault(p => p.Name == propertyName);
                    if (propertyInfo is not null)
                    {
                        _title = propertyInfo.GetCustomAttribute<DisplayAttribute>(true)?.Name
                            ?? propertyInfo.GetCustomAttribute<DisplayNameAttribute>(true)?.DisplayName
                            ?? propertyInfo.GetCustomAttribute<DescriptionAttribute>(true)?.Description;
                    }
                }
            }
        }

        _title ??= propertyName ?? "???";
    }

    public override string? GetStringValue(object data)
    {
        TType? value = default;

        if (data is TItem item)
        {
            value = _valueGetter!(item);
        }
        else
        {
            return null;
        }

        if (Formatter is not null)
        {
            return Formatter(value);
        }
        else if (!string.IsNullOrWhiteSpace(_formatString))
        {
            return string.Format(_formatString, value, GridView.DefaultCulture);
        }
        else
        {
            return Convert.ToString(value, GridView.DefaultCulture);
        }
    }

    public override RenderFragment? InvokeTemplate(object data)
    {
        if (Template is null)
        {
            return new RenderFragment(builder => builder.AddContent(0, GetStringValue(data)));
        }

        var item = (TItem)data;
        return Template(new(item, _valueGetter!(item)));
    }

    public override RenderFragment? InvokeEditTemplate(IGridViewRow row)
    {
        if (EditTemplate is null)
        {
            return null;
        }

        if (row.Editing)
        {
            return EditTemplate(new(false, (TItem)row.EditItem!, _valueGetter!, _valueSetter!));
        }
        else if (row.Adding)
        {
            return EditTemplate(new(true, (TItem)row.Item, _valueGetter!, _valueSetter!));
        }
        else
        {
            return null;
        }
    }
}
