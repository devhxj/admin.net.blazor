﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    private readonly List<TItem> _cachedExpandedItems = new();
    private readonly List<GridViewTreeNode<TItem>> _cachedTreeNodes = new(200);

    private async Task ExpandItem(TItem item)
    {
        var addRows = new List<TItem>();
        var removeRows = new List<TItem>();
        var expanded = _cachedExpandedItems.Contains(item);

        if (ExpandMode == Tristate.Single)
        {
            removeRows = _cachedExpandedItems.ToList();
            _cachedExpandedItems.Clear();
        }

        if (expanded)
        {
            removeRows.Remove(item);
            _cachedExpandedItems.Remove(item);
        }
        else
        {
            addRows.Add(item);
            _cachedExpandedItems.Add(item);

            //单展开时关闭其他项
           await _tableSection.SafeChangStateAsync();
        }

        if (OnRowCollapse is not null && removeRows.Count > 0)
        {
            foreach (var x in removeRows)
            {
                await OnRowCollapse(x);
            }
        }

        if (OnRowExpand is not null && addRows.Count > 0)
        {
            foreach (var x in addRows)
            {
                await OnRowExpand(x);
            }
        }
    }

    private async Task CascadeExpandSelectChildTreeRow(GridViewTreeNode<TItem> node)
    {
        var addItems = new List<TItem>();
        var removeItems = new List<TItem>();

        // 如果当前节点被选中，展开时则子节点也要被选中
        // 如果当前节点被折叠，则选中的子节点要被取消
        var selected = IsRowSelected(node.Data);

        //如果是单选模式，则直接取消父节点的选择状态
        if (RowSelectMode == Tristate.Single)
        {
            removeItems = _cacheSelectedItems.ToList();
            _cacheSelectedItems.Clear();
        }
        else
            CascadeExpandSelectChildren(node, selected);

        await UpdateSelectedState(true);
        await InvokeSelectedCallback(addItems, removeItems);

        void CascadeExpandSelectChildren(GridViewTreeNode<TItem> node, bool selected)
        {
            foreach (var child in node.ChildNodes)
            {
                var value = node.Expanded && selected;
                if (value)
                {
                    addItems.Add(child.Data);
                    _cacheSelectedItems.Add(child.Data);
                }
                else
                {
                    removeItems.Add(child.Data);
                    _cacheSelectedItems.RemoveAll(x => ComparerItem(x, child.Data));
                }

                CascadeExpandSelectChildren(child, value);
            }
        }
    }

    public async Task CollapseOtherTreeNode(GridViewTreeNode<TItem> node)
    {
        //为空则是根节点
        var items = node.Parent is null ? _view.AsEnumerable() : node.Parent.Children!;
        foreach (var item in items)
        {
            if (!ComparerItem(item, node.Data))
            {
                var treeNode = GetTreeNode(item);
                if (treeNode.Expanded)
                {
                    treeNode.Expanded = false;
                    if (treeNode.StateHasChanged is not null)
                        await treeNode.StateHasChanged();
                }
            }
        }
    }
}
