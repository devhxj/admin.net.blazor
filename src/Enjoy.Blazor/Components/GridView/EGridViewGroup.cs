﻿namespace Enjoy.Blazor;

public class EGridViewGroup : ComponentBase
{
    [Parameter]
    public int Level { get; init; }

    [Parameter]
    [NotNull]
    public IGridViewColumn? Column { get; init; }

    [Parameter]
    [NotNull]
    public GroupResult? Data { get; init; }

    [Parameter]
    public RenderFragment<EGridViewGroup>? ChildContent { get; init; }

    /// <summary>
    /// 获取/设置 拖拽分组行渲染回调，用于设置行属性。
    /// </summary>
    [Parameter]
    public Action<GridViewGroupFirstRenderEventArgs>? OnBeforeFirstRender { get; set; }

    internal bool Expanded { get; private set; } = true;

    internal GridViewGroup? Group { get; private set; }

    internal GridViewGroupFirstRenderEventArgs? RenderArgs { get; set; }

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();
        Group = new(Column, Level, Data);
        if (OnBeforeFirstRender is not null)
        {
            var args = new GridViewGroupFirstRenderEventArgs(Group);
            OnBeforeFirstRender(args);
            Expanded = args.Expanded;
        }

    }

    protected override void BuildRenderTree(RenderTreeBuilder builder) => builder.AddContent(0, ChildContent, this);

    internal void HandleOnRowExpand() => Expanded = !Expanded;
}
