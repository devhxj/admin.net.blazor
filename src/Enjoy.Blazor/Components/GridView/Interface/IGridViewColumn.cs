﻿namespace Enjoy.Blazor;

/// <summary>
/// 定义数据列标准
/// </summary>
public interface IGridViewColumn : IGridViewColumnProperty, IGridViewColumnChild, IGridViewColumnFixed, IGridViewColumnFilter
{
    string Identifier { get; }

    bool Initialized { get; }

    string? GetKey();

    ColumnType GetCategory();

    string? GetStringValue(object item);

    string? GetTitle();

    bool GetVisible();

    bool GetSettable();

    int GetWidth();

    bool GetResizable();

    bool GetReorderable();

    bool GetGroupable();

    bool GetSortable();

    Direction GetDirection();

    bool GetTreeExpandable();

    string? GetFormatString();

    bool GetCaseInsensitive();

    string GetDirectionAsString();

    DataTableHeaderAlign GetTextAlign();

    DataTableHeaderAlign GetHeaderAlign();

    string? GetHeaderCellCssClass();

    string? GetCellCssClass();

    string? GetFooterCellCssClass();

    string? GetGroupFooterCellCssClass();

    RenderFragment? InvokeTemplate(object data);

    RenderFragment? InvokeEditTemplate(IGridViewRow row);

    RenderFragment? InvokeGroupFooterTemplate(GridViewGroup? group);

    RenderFragment InvokeHeaderTemplate();

    RenderFragment? GetColumns();

    RenderFragment? GetFooterTemplate();

    RenderFragment<IGridViewColumnFilter>? GetFilterTemplate();

    RenderFragment<GridViewGroup>? GetGroupFooterTemplate();

    void SetVisible(bool value);

    void SetFilterable(bool value);

    void SetLogicaler(Logical value);

    void SetReorderable(bool value);

    void SetGroupable(bool value);

    void SetSortable(bool value);

    void SetDirection(Direction value);

    void SetResizable(bool value);

    void SetWidth(int? value);

    IList<SelectItem<string>> GetPropertyEnums();
}