﻿namespace Enjoy.Blazor;

public interface IGridViewAdder : IGridViewEditor
{
    IList AddItems { get; }

    IList AddSelectedItems { get; }

    Task InlineAddCallback();
}