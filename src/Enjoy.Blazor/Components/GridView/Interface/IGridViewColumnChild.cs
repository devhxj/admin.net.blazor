﻿namespace Enjoy.Blazor;

public interface IGridViewColumnChild
{
    IReadOnlyList<IGridViewColumn> Children { get; }

    void AddColumn(IGridViewColumn column);

    void RemoveColumn(IGridViewColumn column);

    void InsertColumn(IGridViewColumn column, IGridViewColumn target);

    bool SetParent(IGridViewColumn? parent);

    IGridViewColumn? GetParent();

    int GetLevel();

    int GetColSpan(bool isDataCell = false);

    int GetRowSpan(bool isDataCell = false);
}