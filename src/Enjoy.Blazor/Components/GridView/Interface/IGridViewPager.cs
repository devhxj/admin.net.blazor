﻿namespace Enjoy.Blazor;

public interface IGridViewPager : IGridView
{
    GridViewPagerOption PagerOption { get; }

    int GetViewCount();

    Task ChangPageSize(int value);

    Task GoToPage(int page, bool forceReload = false);

    Task FirstPage(bool forceReload = false);

    Task PrevPage(bool forceReload = false);

    Task NextPage(bool forceReload = false);

    Task LastPage(bool forceReload = false);
}