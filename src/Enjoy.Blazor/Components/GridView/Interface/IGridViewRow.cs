﻿namespace Enjoy.Blazor;

public interface IGridViewRow
{
    bool Editing { get; }

    bool Adding { get;}

    object? EditItem { get; }

    object Item { get; }
    void ClearEditForm();

    Task CancelEditingAsync();

    Task ResetEditFormAsync();

    Task UpdateEditedAsync();

    Task UpdateEditingAsync();

    bool? ValidateEditForm();
}