﻿namespace Enjoy.Blazor;

public interface IGridViewTopbar : IGridViewEditor
{
    string? Title { get;}

    RenderFragment? Actions { get; }

    bool AllowKeywordsFiltering { get; }

    bool AllowCustomFiltering { get; }

    bool AllowDynamicFiltering { get; }

    void SetFilterKeywords(string? value);

    RenderFragment RenderCustomFilter();

    Task ShowDynamicFilter();

    Task ForceRenderAsync();

    Task ShowSettingAsync();

    IReadOnlyCollection<IGridViewColumn> GroupedColumns { get; }

    Task EndColumnDropToGroup(DragCustomEventArgs args);

    Task RemoveGroup(IGridViewColumn column);


    IReadOnlyCollection<IGridViewColumn> SettableColumns { get; }

    Task EndColumnSettingDrop(DragCustomEventArgs args);

    Task OnSettingClick(IGridViewColumn column, string type);

    Task RemoveSettableColumn(IGridViewColumn column);
}