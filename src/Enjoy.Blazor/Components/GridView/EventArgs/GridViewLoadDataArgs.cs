﻿namespace Enjoy.Blazor;

/// <summary>
/// 加载数据回调事件参数
/// </summary>
public class GridViewLoadDataArgs
{
    /// <summary>
    /// 获取 是否需要分页
    /// </summary>
    public bool AllowPaging { get; internal set; }

    /// <summary>
    /// 获取 要跳过的记录数，仅需要分页时使用。
    /// </summary>
    public int Skip { get; internal set; }

    /// <summary>
    /// 获取 要提取的记录数，仅需要分页时使用。
    /// </summary>
    public int Top { get; internal set; }

    /// <summary>
    /// 获得 过滤关键字
    /// </summary>
    public string? FilterKeywords { get; internal set; }

    /// <summary>
    /// 获得 自定义筛选条件
    /// </summary>
    public IEnumerable<Filter>? CustomFilters { get; internal set; }

    /// <summary>
    /// 获取 筛选条件
    /// </summary>
    public Filter? Filter { get; internal set; }

    /// <summary>
    /// 获取 排序配置
    /// </summary>
    public IReadOnlyCollection<Sorter>? Orders { get; internal set; }
}
