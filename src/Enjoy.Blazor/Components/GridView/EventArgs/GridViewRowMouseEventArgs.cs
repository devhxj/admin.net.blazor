﻿namespace Enjoy.Blazor;

public class GridViewRowMouseEventArgs<TItem> : MouseEventArgs
{
    internal GridViewRowMouseEventArgs(object data)
    {
        if (data is TItem item)
        {
            Data = item;
        }
        else
        {
            throw new ArgumentException($"Type must be {typeof(TItem)}.");
        }
    }

    internal GridViewRowMouseEventArgs(object data, MouseEventArgs args) : this(data)
    {
        Type = args.Type;
        AltKey = args.AltKey;
        Button = args.Button;
        Buttons = args.Buttons;
        ClientX = args.ClientX;
        ClientY = args.ClientY;
        CtrlKey = args.CtrlKey;
        Detail = args.Detail;
        MetaKey = args.MetaKey;
        OffsetX = args.OffsetX;
        OffsetY = args.OffsetY;
        ScreenX = args.ScreenX;
        ScreenY = args.ScreenY;
        ShiftKey = args.ShiftKey;
    }

    [NotNull]
    public TItem Data { get; }
}
