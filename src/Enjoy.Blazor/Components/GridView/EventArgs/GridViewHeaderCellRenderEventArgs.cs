﻿namespace Enjoy.Blazor;

/// <summary>
/// 单元格渲染回调事件参数
/// </summary>
/// <typeparam name="TItem"></typeparam>
public class GridViewHeaderCellRenderEventArgs
{
    public GridViewHeaderCellRenderEventArgs(IGridViewColumn column) => Column = column;

    /// <summary>
    /// 获取 行元素HTML属性，它们将应用于 <see cref="EGridView{TItem}" /> 最终呈现的 thead 的 th 元素。
    /// </summary>
    public IDictionary<string, object> Attributes { get; private set; } = new Dictionary<string, object>();

    public IGridViewColumn Column { get; private set; }
}
