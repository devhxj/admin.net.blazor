﻿namespace Enjoy.Blazor;

/// <summary>
/// 组件列分组操作参数 
/// </summary>
public class GridViewGroupFirstRenderEventArgs
{
    public GridViewGroupFirstRenderEventArgs(GridViewGroup? group)
    {
        Group = group;
        Expanded = true;
    }

    /// <summary>
    /// 获取 当前行表示的数据项。
    /// </summary>
    public GridViewGroup? Group { get; }

    /// <summary>
    /// 获取/设置 指示此行是否默认展开
    /// </summary>
    public bool Expanded { get; set; }
}
