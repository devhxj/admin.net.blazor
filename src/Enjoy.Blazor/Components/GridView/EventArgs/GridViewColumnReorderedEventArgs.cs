﻿namespace Enjoy.Blazor;

/// <summary>
/// 列拖拽回调事件参数
/// </summary>
/// <typeparam name="TItem"></typeparam>
public class GridViewColumnReorderedEventArgs
{
    /// <summary>
    /// 构造函数
    /// </summary>
    /// <param name="column">当前列</param>
    /// <param name="oldIndex">拖拽前列索引</param>
    /// <param name="newIndex">拖拽后列索引</param>
    public GridViewColumnReorderedEventArgs(IGridViewColumn column, int oldIndex, int newIndex)
    {
        Column = column;
        OldIndex = oldIndex;
        NewIndex = newIndex;
    }

    /// <summary>
    /// 获取 当前列
    /// </summary>
    public IGridViewColumn Column { get; private set; }

    /// <summary>
    /// 获取 当前列拖拽前列索引
    /// </summary>
    public int OldIndex { get; private set; }

    /// <summary>
    /// 获取 当前列拖拽后列索引
    /// </summary>
    public int NewIndex { get; private set; }
}
