﻿namespace Enjoy.Blazor;

/// <summary>
/// 列排序回调事件参数
/// </summary>
/// <typeparam name="TItem"></typeparam>
public class GridViewColumnSortEventArgs
{
    public GridViewColumnSortEventArgs(IGridViewColumn column, Direction oldOrder)
    {
        Column = column;
        OldOrder = oldOrder;
    }

    /// <summary>
    /// 获取 当前列
    /// </summary>
    public IGridViewColumn Column { get;}

    /// <summary>
    /// 获取 旧的排序反向
    /// </summary>
    public Direction OldOrder { get;}
}
