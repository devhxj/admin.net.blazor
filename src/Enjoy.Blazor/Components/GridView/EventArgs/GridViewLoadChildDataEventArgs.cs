﻿namespace Enjoy.Blazor;

/// <summary>
/// 加载子节点数据回调事件参数 <see cref="EGridView{TItem}.OnLoadChildData" />
/// </summary>
public class GridViewLoadChildDataEventArgs<TItem>
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    public GridViewLoadChildDataEventArgs(TItem item)
    {
        Item = item;
    }

    /// <summary>
    /// 获取 当前节点数据
    /// </summary>
    public TItem Item { get; private set; }

    /// <summary>
    /// 获取/设置 子节点数据集合
    /// </summary>
    public IEnumerable<TItem>? Data { get; set; }
}
