﻿namespace Enjoy.Blazor;

public partial class EGridView<TItem>
{
    public async Task ChangPageSize(int value)
    {
        bool isFirstPage = PagerOption.Page == 0;
        bool isLastPage = PagerOption.Page == PagerOption.NumberOfPages - 1;

        PagerOption.ItemsPerPage = value;

        CalculatePager();

        if (isFirstPage)
            await FirstPage(true);
        else if (isLastPage)
            await LastPage(true);
        else
        {
            int newPage = (int)Math.Floor((decimal)(PagerOption.Skip / (PagerOption.ItemsPerPage > 0 ? PagerOption.ItemsPerPage : 10)));
            await GoToPage(newPage, true);
        }
    }

    public async Task GoToPage(int page, bool forceReload = false)
    {
        if (page < PagerOption.StartPage)
            return;

        if (page > Math.Min(PagerOption.EndPage + 1, PaginationLength))
            return;

        if (page > (PagerOption.NumberOfPages - 1))
            return;

        if (PagerOption.Page != page || forceReload)
        {
            PagerOption.Page = page;
            PagerOption.Skip = page * PagerOption.ItemsPerPage;

            await RefreshAsync(false);
        }
    }

    public async Task FirstPage(bool forceReload = false)
    {
        //if (_Skip <= 0)
        //    return;

        if (PagerOption.Page != 0 || forceReload)
        {
            PagerOption.Page = 0;
            PagerOption.Skip = 0;

            await RefreshAsync(false);
        }
    }

    public async Task PrevPage(bool forceReload = false)
    {
        //if (_Skip <= 0)
        //    return;

        var newPage = PagerOption.Page > 0
                    ? PagerOption.Page - 1
                    : 0;
        var newSkip = PagerOption.ItemsPerPage * newPage;
        if (newSkip != PagerOption.Skip || forceReload)
        {
            PagerOption.Page = newPage;
            PagerOption.Skip = newSkip;

            await RefreshAsync(false);
        }
    }

    public async Task NextPage(bool forceReload = false)
    {
        //if (_CurrentPage == _NumberOfPages - 1)
        //    return;

        var newPage = PagerOption.Page < (PagerOption.NumberOfPages - 1)
                    ? PagerOption.Page + 1
                    : PagerOption.NumberOfPages - 1;
        var newSkip = PagerOption.ItemsPerPage * newPage;
        if (newSkip != PagerOption.Skip || forceReload)
        {
            PagerOption.Page = newPage;
            PagerOption.Skip = newSkip;

            await RefreshAsync(false);
        }
    }

    public async Task LastPage(bool forceReload = false)
    {
        //if (_CurrentPage == _NumberOfPages - 1)
        //    return;

        var newPage = PagerOption.NumberOfPages - 1;
        var newSkip = PagerOption.ItemsPerPage * newPage;
        if (newSkip != PagerOption.Skip || forceReload)
        {
            PagerOption.Page = newPage;
            PagerOption.Skip = newSkip;

            await RefreshAsync(false);
        }
    }

    private void CalculatePager()
    {
        if (AllowPaging)
        {
            var pageSize = PagerOption.ItemsPerPage > 0 ? PagerOption.ItemsPerPage : 10;
            PagerOption.NumberOfPages = (int)Math.Ceiling(PagerOption.TotalRecordCount * 1.0 / pageSize);

            if (PagerOption.NumberOfPages < 1)
                PagerOption.NumberOfPages = 1;

            int visiblePages = Math.Min(PaginationLength, PagerOption.NumberOfPages);

            PagerOption.StartPage = (int)Math.Max(0, Math.Ceiling(PagerOption.Page - (visiblePages / 2.0)));
            PagerOption.EndPage = Math.Min(PagerOption.NumberOfPages - 1, PagerOption.StartPage + visiblePages - 1);

            var delta = PaginationLength - (PagerOption.EndPage - PagerOption.StartPage + 1);
            PagerOption.StartPage = Math.Max(0, PagerOption.StartPage - delta);
        }
    }

    private string GetTheme() => IsDark ? "theme--dark" : "theme--light";

    private string? GetPagingSummary()
    {
        return string.Format(I18n.T("$enjoyBlazor.gridView.pager.pageSummary"), PagerOption.Skip + 1, PagerOption.Skip + GetViewCount(), PagerOption.TotalRecordCount);
    }
}
