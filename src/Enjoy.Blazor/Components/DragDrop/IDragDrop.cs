﻿namespace Enjoy.Blazor;

public interface IDragDrop
{
    IEnumerable<string>? DragKeys { get; }

    string? Key { get; }

    void SetKey(string? key);

    Task UpdateItemAsync();
}