﻿namespace Enjoy.Blazor;

public interface IEditor<TItem>
{
    bool IsAdd { get; }

    [NotNull]
    TItem? Model { get; }
}