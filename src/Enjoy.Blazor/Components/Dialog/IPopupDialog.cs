﻿namespace Enjoy.Blazor;

/// <summary>
/// 弹出层控制对象
/// </summary>
public interface IPopupHandle
{
    /// <summary>
    /// 获取 是否全屏
    /// </summary>
    bool IsFullscreen { get; }

    /// <summary>
    /// 获取 关闭控制方法
    /// </summary>
    /// <returns></returns>
    EventCallback OnClose { get; }

    /// <summary>
    /// 设置 切换是否全屏
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    Task SetFullscreen(bool value);
}
