﻿namespace Enjoy.Blazor;

/// <summary>
/// 异步加载器接口
/// </summary>
/// <typeparam name="TData"></typeparam>
public interface IAsyncLoader<TData> where TData : new()
{
    /// <summary>
    /// 是否正在加载
    /// </summary>
    bool IsLoading { get; }

    /// <summary>
    /// 加载结果
    /// </summary>
    TData Data { get; }
}