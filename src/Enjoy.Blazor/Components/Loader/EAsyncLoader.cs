﻿using System.ComponentModel;

namespace Enjoy.Blazor;

/// <summary>
/// 异步加载器组件
/// </summary>
/// <typeparam name="TData">加载数据类型</typeparam>
public sealed class EAsyncLoader<TData> : BComponentBase, IAsyncLoader<TData>
    where TData : new()
{
    /// <summary>
    /// 加载结果
    /// </summary>
    public TData Data { get; private set; } = new();

    /// <summary>
    /// 是否正在加载
    /// </summary>
    public bool IsLoading { get; private set; } = true;

    /// <summary>
    /// 获得/设置 异步加载数据方法
    /// </summary>
    [Parameter]
    public Func<Task<TData>>? LoadData { get; set; }

    /// <summary>
    /// 获得/设置 子组件内容
    /// </summary>
    [Parameter]
    public RenderFragment<IAsyncLoader<TData>>? ChildContent { get; set; }

    /// <summary>
    /// 获得/设置 此 ECondition 需要观察的对象
    /// </summary>
    [Parameter]
    public INotifyPropertyChanged? Watch { get; init; }

    /// <summary>
    /// 获得/设置 监听变化的属性
    /// </summary>
    [Parameter]
    public IEnumerable<string>? WatchProps { get; init; }

    private bool HasWatchValue => Watch is not null && WatchProps is not null && WatchProps.Any();

    /// <summary>
    /// OnInitialized
    /// </summary>
    protected override void OnInitialized()
    {
        base.OnInitialized();

        if (HasWatchValue)
        {
            Watch!.PropertyChanged += OnWatchPropertyChanged;
        }
    }

    /// <summary>
    /// BuildRenderTree
    /// </summary>
    /// <param name="builder"></param>
    protected override void BuildRenderTree(RenderTreeBuilder builder)
    {
        if (ChildContent is not null)
        {
            builder.AddContent(0, ChildContent(this));
        }
    }

    /// <summary>
    /// OnParametersSetAsync 方法
    /// </summary>
    /// <returns></returns>
    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);
        if (firstRender)
        {
            await InternalLoadRender();
        }
    }

    /// <summary>
    /// Dispose
    /// </summary>
    /// <param name="disposing"></param>
    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            if (HasWatchValue)
            {
                Watch!.PropertyChanged -= OnWatchPropertyChanged;
            }
        }
    }

    /// <summary>
    /// 重新加载数据并渲染
    /// </summary>
    private async Task InternalLoadRender()
    {
        if (!IsLoading)
        {
            IsLoading = true;
        }
        if (LoadData is not null)
        {
            Data = await LoadData();
        }
        IsLoading = false;
        await InvokeStateHasChangedAsync();
    }

    /// <summary>
    /// 值发生变化
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private async void OnWatchPropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        if (WatchProps!.Contains(e.PropertyName))
        {
            await InternalLoadRender();
        }
    }
}
