﻿namespace Enjoy.Blazor;

/// <summary>
/// 浮动层 组件
/// </summary>
public sealed class EFloating : BComponentBase
{
    private readonly FloatingOptions _options = new();
    private readonly Dictionary<string, object?> _attributes = new();
    private bool _visible;
    private ProviderItem? _item;

    /// <summary>
    /// 获得/设置 浮动层的锚点元素
    /// </summary>
    [Parameter]
    public string? Anchor { get; set; }

    /// <summary>
    /// 获得/设置 浮动层显示状态
    /// </summary>
    [Parameter]
    public bool Visible { get; set; }

    /// <summary>
    /// 获得/设置 浮动层相对于它的锚点元素的位置
    /// </summary>
    [Parameter]
    public Placement Placement { get; set; }

    /// <summary>
    /// 获得/设置 浮动层显示层级
    /// </summary>
    [Parameter]
    public int? ZIndex { get; set; }

    /// <summary>
    /// 获得/设置 内层子组件内容 
    /// </summary>
    [Parameter]
    public RenderFragment<IFloating>? ChildContent { get; set; }

    /// <summary>
    /// 获得/设置 当与锚点元素不在同一个剪辑上下文中时是否自动隐藏
    /// </summary>
    [Parameter]
    public bool AutoHide { get; set; }

    /// <summary>
    /// 获得/设置 是否自动更新位置，默认false
    /// </summary>
    [Parameter]
    public bool AutoUpdate { get; set; }

    /// <summary>
    /// 获得/设置 浮动元素和参考元素之间的距离，例如left。
    /// </summary>
    [Parameter]
    public int? MainOffset { get; set; }

    /// <summary>
    /// 获得/设置 浮动元素和参考元素之间的距离，例如top。
    /// </summary>
    [Parameter]
    public int? CrossOffset { get; set; }

    /// <summary>
    /// 获得/设置 是否自动更改浮动元素的位置以使其处于可见状态。
    /// </summary>
    [Parameter]
    public bool UseFlip { get; set; }

    /// <summary>
    /// 获得/设置 配置需要填充的宽度，沿指定轴移动浮动元素以使其保持在视图中。
    /// </summary>
    [Parameter]
    public int? ShiftPadding { get; set; }

    /// <summary>
    /// 获得/设置 是否使用 提示箭头，浮动层必须预先设置为绝对定位
    /// </summary>
    [Parameter]
    public bool UseArrow { get; set; }

    /// <summary>
    /// 获得/设置 提示箭头的偏移量
    /// </summary>
    [Parameter]
    public int? ArrowOffset { get; set; }

    /// <summary>
    /// 获得/设置 提示箭头的样式
    /// </summary>
    [Parameter]
    public string? ArrowStyleClass { get; set; }

    /// <summary>
    /// 获得/设置 提示箭头的样式
    /// </summary>
    [Parameter]
    public string? ArrowClassStyle { get; set; }

    /// <summary>
    /// 获得/设置 组件样式类
    /// </summary>
    [Parameter]
    public string? @Class { get; set; }

    /// <summary>
    /// 获得/设置 组件样式
    /// </summary>
    [Parameter]
    public string? @Style { get; set; }

    /// <summary>
    /// 获得/设置 是否使用遮罩层
    /// </summary>
    [Parameter]
    public bool ShowOverlay { get; set; }

    /// <summary>
    /// 获得/设置 Masa弹出层 服务实例
    /// </summary>
    [Inject]
    [NotNull]
    private IPopupProvider? PopupProvider { get; set; }

    protected override void OnInitialized()
    {
        base.OnInitialized();
        _options.Anchor = Anchor;
        _options.Placement = Placement;
        _options.ZIndex = ZIndex;
        _options.ChildContent = ChildContent;
        _options.AutoHide = AutoHide;
        _options.AutoUpdate = AutoUpdate;
        _options.MainOffset = MainOffset;
        _options.CrossOffset = CrossOffset;
        _options.UseFlip = UseFlip;
        _options.ShiftPadding = ShiftPadding;
        _options.UseArrow = UseArrow;
        _options.ArrowOffset = ArrowOffset;
        _options.ArrowStyleClass = ArrowStyleClass;
        _options.ArrowClassStyle = ArrowClassStyle;
        _options.Class = @Class;
        _options.Style = Style;
        _options.ShowOverlay = ShowOverlay;

        _attributes.Add(nameof(EFloatingContent.Options), _options);
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (Visible != _visible)
        {
            if (Visible)
            {
                _visible = true;
                _item = PopupProvider.Add(typeof(EFloatingContent), _attributes, _options.Id, nameof(EFloating));
            }
            else
            {
                _visible = false;
                if (_item is not null)
                {
                    //如果是开启了自适应监听，需要先清除监听
                    if (_options.AutoHide && _options.OnClearAutoUpdate is not null)
                    {
                        await _options.OnClearAutoUpdate();
                    }

                    PopupProvider.Remove(_item);

                    _item = null;
                }
            }
        }
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);
        if (disposing)
        {
            if (_item is not null)
            {
                PopupProvider.Remove(_item);
                _item = null;
            }
        }
    }
}
