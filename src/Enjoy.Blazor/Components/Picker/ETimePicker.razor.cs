﻿namespace Enjoy.Blazor;

/// <summary>
/// TimePicker 组件基类
/// </summary>
public sealed partial class ETimePicker<TValue>
{
    private readonly TimeSpan MaxTime = new(23, 59, 59);
    private readonly Type ValueType = Nullable.GetUnderlyingType(typeof(TValue)) ?? typeof(TValue);

    /// <summary>
    /// 获得/设置 当前时间
    /// </summary>
    private TimeSpan InternalValue { get; set; }

    /// <summary>
    /// 获得/设置 原始值
    /// </summary>
    private TValue? OriginalValue { get; set; }

    /// <summary>
    /// Gets or sets the value of the input. This should be used with two-way binding.
    /// </summary>
    /// <example>
    /// @bind-Reference="model.DisplayName"
    /// </example>
    [Parameter]
    public TValue? Value
    {
        get => InternalValueParse(InternalValue);
        set => InternalValue = InternalValueConvert(value);
    }

    /// <summary>
    /// Gets or sets a callback that updates the bound value.
    /// </summary>
    [Parameter]
    public EventCallback<TValue?> ValueChanged { get; set; }

    /// <summary>
    /// 获得/设置 字符串格式，仅当 TValue 为 string 且未设置 DateTimeConvert 时使用
    /// </summary>
    [Parameter]
    public string ValueFormat { get; init; } = "hh\\:mm\\:ss";

    /// <summary>
    /// 获得/设置 日期转结果值
    /// </summary>
    [Parameter]
    public Func<TValue?,TimeSpan>? ValueConvert { get; set; }

    /// <summary>
    /// 获得/设置 结果值转日期
    /// </summary>
    [Parameter]
    public Func<TimeSpan?, TValue?>? ValueParse { get; set; }

    /// <summary>
    /// 获得/设置 取消按钮回调委托
    /// </summary>
    [Parameter]
    public EventCallback OnClose { get; set; }

    /// <summary>
    /// 获得/设置 确认按钮回调委托
    /// </summary>
    [Parameter]
    public EventCallback OnConfirm { get; set; }

    /// <summary>
    /// OnInitialized 方法
    /// </summary>
    protected override void OnInitialized()
    {
        ParameterCheck();

        base.OnInitialized();

        OriginalValue = Value;
    }

    /// <summary>
    /// 参数检查
    /// </summary>
    private bool ParameterCheck()
    {
        if (ValueType == typeof(TimeSpan))
        {
            return true;
        }

        if (ValueType == typeof(string))
        {
            //字符串类型，需检测 ValueFormat 是否可用
            try
            {
                _ = MaxTime.ToString(ValueFormat);
            }
            catch
            {
                throw new NotSupportedException($"TValue cannot be formatted as {ValueFormat}.");
            }
            return true;
        }

        throw new NotSupportedException("TValue must be of type TimeSpan, String or its nullable type.");
    }

    /// <summary>
    /// Value值转换方法
    /// </summary>
    private TimeSpan InternalValueConvert(TValue? value)
    {
        if (ValueConvert is not null)
        {
            return ValueConvert(value);
        }

        if (value is TimeSpan time)
        {
            return (time >= TimeSpan.Zero && time <= MaxTime) ? time : TimeSpan.Zero;
        }
        else if (value is string str)
        {
            if (TimeSpan.TryParseExact(str, ValueFormat, null, out var result))
            {
                return result;
            }
        }

        return TimeSpan.Zero;
    }

    /// <summary>
    /// Value值解析方法
    /// </summary>
    private TValue? InternalValueParse(TimeSpan? value)
    {
        if (ValueParse is not null)
        {
            return ValueParse(value);
        }

        if (value is null)
        {
            return default;
        }

        if (ValueType == typeof(TimeSpan))
        {
            return (TValue)((object)value);
        }

        if (ValueType == typeof(string))
        {
            return (TValue)((object)value.Value.ToString(ValueFormat));
        }

        return default;
    }

    /// <summary>
    /// 点击取消按钮回调此方法
    /// </summary>
    private async Task HandleOnClose()
    {
        Value = OriginalValue;

        if (OnClose.HasDelegate)
        {
            await OnClose.InvokeAsync();
        }
    }

    /// <summary>
    /// 点击确认按钮时回调此方法
    /// </summary>
    private async Task HandleOnConfirm()
    {
        if (ValueChanged.HasDelegate)
        {
            await ValueChanged.InvokeAsync(Value);
        }
        if (OnConfirm.HasDelegate)
        {
            await OnConfirm.InvokeAsync();
        }
    }
}
