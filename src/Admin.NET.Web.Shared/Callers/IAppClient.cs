﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IAppClient : IHttpApi
{
    /// <summary>
    /// 分页查询系统应用
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysApp/page")]
    ITask<XnRestfulResult<PageResult<AppOutput>>> PageAsync(AppPageInput body, CancellationToken token = default);

    /// <summary>
    /// 增加系统应用
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysApp/add")]
    ITask<XnRestfulResult<string>> AddAsync([JsonContent] AddAppInput body, CancellationToken token = default);

    /// <summary>
    /// 删除系统应用
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysApp/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync([JsonContent] DeleteAppInput body, CancellationToken token = default);

    /// <summary>
    /// 更新系统应用
    /// </summary>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpPost("api/sysApp/edit")]
    ITask<XnRestfulResult<string>> EditAsync([JsonContent] UpdateAppInput body, CancellationToken token = default);

    /// <summary>
    /// 获取系统应用
    /// </summary>
    /// <param name="id"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysApp/detail")]
    ITask<XnRestfulResult<AppOutput>> DetailAsync(QueryAppInput body, CancellationToken token = default);

    /// <summary>
    /// 获取系统应用列表
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysApp/list")]
    ITask<XnRestfulResult<List<AppOutput>>> ListAsync(CancellationToken token = default);

    /// <summary>
    /// 设为默认应用
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysApp/setAsDefault")]
    ITask<XnRestfulResult<string>> SetAsDefaultAsync([JsonContent] SetDefaultAppInput body, CancellationToken token = default);

    /// <summary>
    /// 修改用户状态
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysApp/changeStatus")]
    ITask<XnRestfulResult<string>> ChangeStatusAsync([JsonContent] ChangeUserAppStatusInput body, CancellationToken token = default);
}
