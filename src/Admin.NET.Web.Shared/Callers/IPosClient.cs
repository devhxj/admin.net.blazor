﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IPosClient : IHttpApi
{
    /// <summary>
    /// 分页获取职位
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysPos/page")]
    ITask<XnRestfulResult<PageResult<PosOutput>>> PageAsync(PosPageInput body, CancellationToken token = default);

    /// <summary>
    /// 获取职位列表
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysPos/list")]
    ITask<XnRestfulResult<List<PosOutput>>> ListAsync(PosPageInput body, CancellationToken token = default);

    /// <summary>
    /// 增加职位
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysPos/add")]
    ITask<XnRestfulResult<string>> AddAsync(AddPosInput body, CancellationToken token = default);

    /// <summary>
    /// 删除职位
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysPos/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync(DeletePosInput body, CancellationToken token = default);

    /// <summary>
    /// 更新职位
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysPos/edit")]
    ITask<XnRestfulResult<string>> EditAsync(UpdatePosInput body, CancellationToken token = default);

    /// <summary>
    /// 获取职位
    /// </summary>
    /// <param name="body"></param>
    /// <param name="id"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysPos/detail")]
    ITask<XnRestfulResult<PosOutput>> DetailAsync(QueryPosInput body, CancellationToken token = default);
}
