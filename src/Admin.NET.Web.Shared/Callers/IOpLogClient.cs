﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IOpLogClient : IHttpApi
{
    /// <summary>
    /// 分页查询操作日志
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysOpLog/page")]
    ITask<XnRestfulResult<PageResult<OpLogOutput>>> PageAsync(OpLogPageInput body, CancellationToken token = default);

    /// <summary>
    /// 清空操作日志
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysOpLog/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync(CancellationToken token = default);
}
