﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface ITenantClient : IHttpApi
{
    /// <summary>
    /// 分页查询租户
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysTenant/page")]
    ITask<XnRestfulResult<PageResult<TenantOutput>>> PageAsync(TenantPageInput body, CancellationToken token = default);

    /// <summary>
    /// 增加租户
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysTenant/add")]
    ITask<XnRestfulResult<string>> AddAsync([JsonContent] AddTenantInput body, CancellationToken token = default);

    /// <summary>
    /// 删除租户
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysTenant/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync([JsonContent] DeleteTenantInput body, CancellationToken token = default);

    /// <summary>
    /// 更新租户
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysTenant/edit")]
    ITask<XnRestfulResult<string>> EditAsync([JsonContent] UpdateTenantInput body, CancellationToken token = default);

    /// <summary>
    /// 获取租户
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysTenant/detail")]
    ITask<XnRestfulResult<TenantOutput>> DetailAsync(QueryTenantInput body, CancellationToken token = default);

    /// <summary>
    /// 授权租户管理员角色菜单
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysTenant/grantMenu")]
    ITask<XnRestfulResult<string>> GrantMenuAsync([JsonContent] GrantRoleMenuInput body, CancellationToken token = default);

    /// <summary>
    /// 获取租户管理员角色拥有菜单Id集合
    /// </summary>
    /// <param name="id"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysTenant/ownMenu")]
    ITask<XnRestfulResult<List<long>>> OwnMenuAsync(QueryTenantInput body, CancellationToken token = default);

    /// <summary>
    /// 重置租户管理员密码
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysTenant/resetPwd")]
    ITask<XnRestfulResult<string>> ResetPwdAsync([JsonContent] QueryTenantInput body, CancellationToken token = default);
}
