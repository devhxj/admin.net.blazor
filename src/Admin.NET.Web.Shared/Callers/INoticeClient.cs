﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface INoticeClient : IHttpApi
{
    /// <summary>
    /// 分页查询通知公告
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysNotice/page")]
    ITask<XnRestfulResult<PageResult<NoticeBase>>> PageAsync(NoticePageInput body, CancellationToken token = default);

    /// <summary>
    /// 增加通知公告
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysNotice/add")]
    ITask<XnRestfulResult<string>> AddAsync([JsonContent] AddNoticeInput body, CancellationToken token = default);

    /// <summary>
    /// 删除通知公告
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysNotice/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync([JsonContent] DeleteNoticeInput body, CancellationToken token = default);

    /// <summary>
    /// 更新通知公告
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysNotice/edit")]
    ITask<XnRestfulResult<string>> EditAsync([JsonContent] UpdateNoticeInput body, CancellationToken token = default);

    /// <summary>
    /// 获取通知公告详情
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysNotice/detail")]
    ITask<XnRestfulResult<NoticeDetailOutput>> DetailAsync(QueryNoticeInput body, CancellationToken token = default);

    /// <summary>
    /// 修改通知公告状态
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysNotice/changeStatus")]
    ITask<XnRestfulResult<string>> ChangeStatusAsync([JsonContent] ChangeStatusNoticeInput body, CancellationToken token = default);

    /// <summary>
    /// 获取接收的通知公告
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysNotice/received")]
    ITask<XnRestfulResult<PageResult<NoticeReceiveOutput>>> ReceivedAsync(NoticePageInput body, CancellationToken token = default);

    /// <summary>
    /// 未处理消息
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysNotice/unread")]
    ITask<XnRestfulResult<UnReadNoticeOutput>> UnreadAsync(NoticeInput body, CancellationToken token = default);
}
