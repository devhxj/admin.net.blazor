﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IMachineClient : IHttpApi
{
    /// <summary>
    /// 获取服务器资源信息
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysMachine/use")]
    ITask<XnRestfulResult<MachineUseOutput>> UseAsync(CancellationToken token = default);

    /// <summary>
    /// 获取服务器基本参数
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysMachine/base")]
    ITask<XnRestfulResult<MachineBaseOutput>> BaseAsync(CancellationToken token = default);

    /// <summary>
    /// 动态获取网络信息
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysMachine/network")]
    ITask<XnRestfulResult<MachineNetOutput>> NetworkAsync(CancellationToken token = default);
}
