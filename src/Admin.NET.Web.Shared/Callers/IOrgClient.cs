﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IOrgClient : IHttpApi
{
    /// <summary>
    /// 分页查询组织机构
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysOrg/page")]
    ITask<XnRestfulResult<PageResult<OrgOutput>>> PageAsync(OrgPageInput body, CancellationToken token = default);

    /// <summary>
    /// 获取组织机构列表
    /// </summary>
    /// <param name="pid"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysOrg/list")]
    ITask<XnRestfulResult<List<OrgOutput>>> ListAsync(OrgListInput body, CancellationToken token = default);

    /// <summary>
    /// 增加组织机构
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysOrg/add")]
    ITask<XnRestfulResult<string>> AddAsync([JsonContent] OrgAddInput body, CancellationToken token = default);

    /// <summary>
    /// 删除组织机构
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysOrg/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync([JsonContent] DeleteOrgInput body, CancellationToken token = default);

    /// <summary>
    /// 更新组织机构
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysOrg/edit")]
    ITask<XnRestfulResult<string>> EditAsync([JsonContent] UpdateOrgInput body, CancellationToken token = default);

    /// <summary>
    /// 获取组织机构信息
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysOrg/detail")]
    ITask<XnRestfulResult<OrgOutput>> DetailAsync(QueryOrgInput body, CancellationToken token = default);

    /// <summary>
    /// 获取组织机构树
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysOrg/tree")]
    ITask<XnRestfulResult<List<OrgTreeNode>>> TreeAsync(CancellationToken token = default);
}
