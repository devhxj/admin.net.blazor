﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IVisLogClient : IHttpApi
{
    /// <summary>
    /// 分页查询访问日志
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysVisLog/page")]
    ITask<XnRestfulResult<PageResult<VisLogOutput>>> PageAsync(VisLogPageInput body, CancellationToken token = default);

    /// <summary>
    /// 清空访问日志
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysVisLog/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync(CancellationToken token = default);
}
