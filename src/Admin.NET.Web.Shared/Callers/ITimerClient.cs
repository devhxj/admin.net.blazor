﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface ITimerClient : IHttpApi
{
    /// <summary>
    /// 分页获取任务列表
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysTimers/page")]
    ITask<XnRestfulResult<PageResult<JobOutput>>> PageAsync(JobPageInput body, CancellationToken token = default);

    /// <summary>
    /// 获取所有本地任务
    /// </summary>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysTimers/localJobList")]
    ITask<XnRestfulResult<List<TaskMethodInfo>>> LocalJobListAsync(CancellationToken token = default);

    /// <summary>
    /// 增加任务
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysTimers/add")]
    ITask<XnRestfulResult<string>> AddAsync(AddJobInput body, CancellationToken token = default);

    /// <summary>
    /// 删除任务
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysTimers/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync(DeleteJobInput body, CancellationToken token = default);

    /// <summary>
    /// 修改任务
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysTimers/edit")]
    ITask<XnRestfulResult<string>> EditAsync(UpdateJobInput body, CancellationToken token = default);

    /// <summary>
    /// 查看任务
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpGet("api/sysTimers/detail")]
    ITask<XnRestfulResult<JobOutput>> DetailAsync(QueryJobInput body, CancellationToken token = default);

    /// <summary>
    /// 停止任务
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysTimers/stop")]
    ITask<XnRestfulResult<string>> StopAsync(StopJobInput body, CancellationToken token = default);

    /// <summary>
    /// 启动任务
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysTimers/start")]
    ITask<XnRestfulResult<string>> StartAsync(AddJobInput body, CancellationToken token = default);
}
