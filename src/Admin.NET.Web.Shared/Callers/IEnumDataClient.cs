﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;
namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IEnumDataClient : IHttpApi
{
    /// <summary>
    /// 通过枚举类型获取枚举值集合
    /// </summary>
    /// <param name="enumName">枚举类型名称</param>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpGet("api/sysEnumData/list")]
    ITask<XnRestfulResult<object>> List9Async(string enumName, CancellationToken token = default);

    /// <summary>
    /// 通过实体字段类型获取相关集合（目前仅支持枚举类型）
    /// </summary>
    /// <param name="entityName">实体名称</param>
    /// <param name="fieldName">字段名称</param>
    /// <returns>Success</returns>
    /// <exception cref="ApiException">A server side error occurred.</exception>
    [HttpGet("api/sysEnumData/listByFiled")]
    ITask<XnRestfulResult<object>> ListByFiledAsync(string entityName, string fieldName, CancellationToken token = default);
}
