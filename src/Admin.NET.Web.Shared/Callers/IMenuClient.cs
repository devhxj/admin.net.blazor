﻿using Admin.NET.Web.Shared.Authentication;
using WebApiClientCore;
using WebApiClientCore.Attributes;

namespace Admin.NET.Web.Shared.Callers;

[JwtAuthentication]
public interface IMenuClient : IHttpApi
{
    /// <summary>
    /// 系统菜单列表（树表）
    /// </summary>
    /// <param name="name">名称</param>
    /// <param name="application">应用分类（应用编码）</param>
    /// <returns></returns>
    [HttpGet("api/sysMenu/list")]
    ITask<XnRestfulResult<List<MenuOutput>>> ListAsync(GetMenuListInput body, CancellationToken token = default);

    /// <summary>
    /// 增加系统菜单
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysMenu/add")]
    ITask<XnRestfulResult<string>> AddAsync([JsonContent] AddMenuInput body, CancellationToken token = default);

    /// <summary>
    /// 删除系统菜单
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysMenu/delete")]
    ITask<XnRestfulResult<string>> DeleteAsync([JsonContent] DeleteMenuInput body, CancellationToken token = default);

    /// <summary>
    /// 更新系统菜单
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysMenu/edit")]
    ITask<XnRestfulResult<string>> EditAsync([JsonContent] UpdateMenuInput body, CancellationToken token = default);

    /// <summary>
    /// 获取系统菜单
    /// </summary>
    /// <param name="body"></param>
    /// <param name="token"></param>
    /// <returns></returns>
    [HttpPost("api/sysMenu/detail")]
    ITask<XnRestfulResult<MenuOutput>> DetailAsync([JsonContent] QueryMenuInput body, CancellationToken token = default);

    /// <summary>
    /// 获取系统菜单树，用于新增、编辑时选择上级节点
    /// </summary>
    /// <param name="application">应用分类（应用编码）</param>
    /// <returns></returns>
    [HttpGet("api/sysMenu/tree")]
    ITask<XnRestfulResult<List<MenuTreeOutput>>> TreeAsync(GetMenuTreeInput body, CancellationToken token = default);

    /// <summary>
    /// 获取系统菜单树，用于给角色授权时选择
    /// </summary>
    /// <param name="application">应用分类（应用编码）</param>
    /// <returns></returns>
    [HttpGet("api/sysMenu/treeForGrant")]
    ITask<XnRestfulResult<List<MenuTreeOutput>>> TreeForGrantAsync(TreeForGrantInput body, CancellationToken token = default);

    /// <summary>
    /// 根据系统应用切换菜单
    /// </summary>
    /// <returns></returns>
    [HttpPost("api/sysMenu/change")]
    ITask<XnRestfulResult<List<AppNavNodeOutput>>> ChangeAsync([JsonContent] ChangeAppMenuInput body, CancellationToken token = default);
}
