﻿using BPM.Web.Shared.Data;

namespace BPM.Web.Shared.Service;

public interface IHttpManager
{
    void ClearClientAuthorization();

    Task PostAsync<T>(string url, T request);

    Task<XnRestfulResult<T2>> PostAsync<T1, T2>(string url, T1 request);

    Task<XnRestfulResult<T2>> GetAsync<T1, T2>(string url, T1 paras);

    Task<XnRestfulResult<T>> GetAsync<T>(string url, IEnumerable<KeyValuePair<string, object>>? paras = null);

    Task<XnRestfulResult<T>> DeleteAsync<T>(string url, IEnumerable<KeyValuePair<string, object>>? paras = null);

    Task<XnRestfulResult<T2>> PutAsync<T1, T2>(string url, T1 request);
}
