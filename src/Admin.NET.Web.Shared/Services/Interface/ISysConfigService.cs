﻿namespace Admin.NET.Web.Shared.Services.Interface;

public interface ISysConfigService
{
    Task<bool> AddAsync(ConfigOutput input);
    Task<bool> DeleteAsync(IEnumerable<ConfigOutput> input);
    Task<(int, IEnumerable<ConfigOutput>)> PageAsync(GridViewLoadDataArgs options);
    Task<bool> EditAsync(ConfigOutput input);
}
