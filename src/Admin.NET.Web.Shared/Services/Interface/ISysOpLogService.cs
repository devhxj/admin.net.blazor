﻿namespace Admin.NET.Web.Shared.Services.Interface;

public interface ISysOpLogService
{
    Task<bool> DeleteAsync();

    Task<(int, IEnumerable<OpLogOutput>)> PageAsync(GridViewLoadDataArgs options);
}
