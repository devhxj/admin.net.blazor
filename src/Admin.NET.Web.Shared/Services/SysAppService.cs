﻿using Admin.NET.Web.Shared.Callers;

namespace Admin.NET.Web.Shared.Services;

public class SysAppService : BaseService, ISysAppService
{
    private readonly IAppClient _appClient;
    public SysAppService(IAppClient client, IPopupService popup) : base(popup)
        => _appClient = client;

    public Task<(int, IEnumerable<AppOutput>)> PageAsync(GridViewLoadDataArgs options)
        => PageAsync(_appClient.PageAsync(QuickAssign<AppPageInput>(options.CustomFilters)));

    public Task<bool> DeleteAsync(IEnumerable<AppOutput> input)
        => DeleteAsync(input.Map<AppOutput, DeleteAppInput>().Select(x => _appClient.DeleteAsync(x)));

    public async Task<bool> SaveAsync(IEnumerable<AppOutput> input, bool isAdd)
    {
        foreach (var item in input)
        {
            if (isAdd)
            {
                await ExecuteAsync(_appClient.AddAsync(item.Map<AddAppInput>()));
            }
            else
            {
                await ExecuteAsync(_appClient.EditAsync(item.Map<UpdateAppInput>()));
            }
        }
        return true;
    }

    public Task<bool> SetAsDefaultAsync(AppOutput input)
        => ExecuteAsync(_appClient.SetAsDefaultAsync(input.Map<SetDefaultAppInput>()));
}

