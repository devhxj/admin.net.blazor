﻿using Admin.NET.Web.Shared.Callers;

namespace Admin.NET.Web.Shared.Services;

/// <summary>
/// 登录服务
/// </summary>
public class SysAuthService : BaseService, ISysAuthService
{
    private readonly RouterPagesProvider _routers;
    private readonly IAuthClient _client;

    public SysAuthService(IAuthClient client, IPopupService popup, RouterPagesProvider routers) : base(popup)
        => (_client, _routers) = (client, routers);

    /// <summary>
    /// 登录验证
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    public Task<XnRestfulResult<string>> LoginAsync(LoginInput input)
        => HandleErrorAsync(_client.LoginAsync(input));

    /// <summary>
    /// 获取用户信息
    /// </summary>
    /// <returns></returns>
    public async Task<XnRestfulResult<LoginOutput>> GetLoginUserAsync()
    {
        var result = await HandleErrorAsync(_client.GetLoginUserAsync());
        if (result.Success && result.Data is not null)
        {

#if DEBUG
            var index = 1;
            var list = new List<AppNavNodeOutput>();
            foreach (var x in _routers.ExecutingPages)
            {
                list.Add(new()
                {
                    Id = index++,
                    Pid = 0,
                    Name = x.Description,
                    Code = x.Name,
                    Icon = "mdi-widgets",
                    Router = x.Routes?.FirstOrDefault(),
                    Application = "debug",
                    Active = "Y",
                    OpenType = MenuOpenType.INNER,
                    Weight = MenuWeight.SUPER_ADMIN_WEIGHT
                });
            }

            result.Data.Navs = list;
            result.Data.Apps ??= new();
            result.Data.Apps.Insert(0, new()
            {
                Id = 0,
                Name = "调试菜单",
                Code = "debug",
                Icon = "mdi-apps",
                Application = "debug"
            });
#endif
        }

        return result;
    }

    /// <summary>
    /// 注销登录
    /// </summary>
    /// <returns></returns>
    public async Task LogoutAsync() => await _client.LogoutAsync();
}

