﻿using Admin.NET.Web.Shared.Callers;

namespace Admin.NET.Web.Shared.Services;

public class SysConfigService : BaseService, ISysConfigService
{
    private readonly IConfigClient _configClient;
    public SysConfigService(IConfigClient configClient, IPopupService popup) : base(popup)
      => _configClient = configClient;

    public Task<bool> AddAsync(ConfigOutput input)
        => ExecuteAsync(_configClient.AddAsync(input.Map<AddConfigInput>()));

    public Task<bool> DeleteAsync(IEnumerable<ConfigOutput> input)
        => DeleteAsync(input.Map<ConfigOutput, DeleteConfigInput>().Select(x => _configClient.DeleteAsync(x)));

    public Task<(int, IEnumerable<ConfigOutput>)> PageAsync(GridViewLoadDataArgs options)
        => PageAsync(_configClient.PageAsync(QuickAssign<ConfigPageInput>(options.CustomFilters)));

    public Task<bool> EditAsync(ConfigOutput input)
        => ExecuteAsync(_configClient.EditAsync(input.Map<UpdateConfigInput>()));
}

