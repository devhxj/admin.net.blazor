﻿namespace Admin.NET.Web.Shared.Services;

public static class FavoriteService
{
    public static List<int> GetDefaultFavoriteMenuList() => new() { 5, 2, 15 };
}
