﻿using Admin.NET.Web.Shared.Callers;

namespace Admin.NET.Web.Shared.Services;

public class SysMenuService : BaseService, ISysMenuService
{
    private readonly RouterPagesProvider _routers;
    private readonly IMenuClient _menuClient;
    public SysMenuService(IMenuClient menuClient, IPopupService popup, RouterPagesProvider routers) : base(popup)
    {
        _menuClient = menuClient;
        _routers = routers;
    }

    public async Task<List<AppNavNodeOutput>?> ChangeAsync(string? application)
    {
        if (application == "debug")
        {
            var index = 1;
            var list = new List<AppNavNodeOutput>();
            foreach (var x in _routers.ExecutingPages)
            {
                list.Add(new()
                {
                    Id = index++,
                    Pid = 0,
                    Name = x.Description,
                    Code = x.Name,
                    Icon = "mdi-widgets",
                    Router = x.Routes?.FirstOrDefault(),
                    Application = "debug",
                    Active = "Y",
                    OpenType = MenuOpenType.INNER,
                    Weight = MenuWeight.SUPER_ADMIN_WEIGHT
                });
            }

            return list;
        }

        return await QueryAsync(_menuClient.ChangeAsync(new() { Application = application }));
    }

    public Task<List<MenuOutput>?> ListAsync(GetMenuListInput input)
        => QueryAsync(_menuClient.ListAsync(input));

    public Task<List<MenuTreeOutput>?> TreeListAsync(string? application)
        => QueryAsync(_menuClient.TreeAsync(new() { Application = application }));

    public Task<bool> DeleteAsync(IEnumerable<MenuOutput> input)
        => DeleteAsync(input.Map<MenuOutput, DeleteMenuInput>().Select(x => _menuClient.DeleteAsync(x)));

    public async Task<bool> SaveAsync(IEnumerable<MenuOutput> input, bool isAdd)
    {
        foreach (var item in input)
        {
            if (isAdd)
            {
                await ExecuteAsync(_menuClient.AddAsync(item.Map<AddMenuInput>()));
            }
            else
            {
                await ExecuteAsync(_menuClient.EditAsync(item.Map<UpdateMenuInput>()));
            }
        }
        return true;
    }



    public Task<bool> AddAsync(MenuOutput input)
        => ExecuteAsync(_menuClient.AddAsync(input.Map<AddMenuInput>()));

    public Task<bool> EditAsync(MenuOutput input)
        => ExecuteAsync(_menuClient.EditAsync(input.Map<UpdateMenuInput>()));

    public Task<MenuOutput?> DetailAsync(QueryMenuInput input)
        => QueryAsync(_menuClient.DetailAsync(input));
}

