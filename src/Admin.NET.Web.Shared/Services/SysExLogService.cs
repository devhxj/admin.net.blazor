﻿using Admin.NET.Web.Shared.Callers;

namespace Admin.NET.Web.Shared.Services;

public class SysExLogService : BaseService, ISysExLogService
{
    private readonly IExLogClient _exLogClient;
    public SysExLogService(IExLogClient exLogClient, IPopupService popup) : base(popup)
        => _exLogClient = exLogClient;

    public Task<(int, IEnumerable<ExLogOutput>)> PageAsync(GridViewLoadDataArgs options)
        => PageAsync(_exLogClient.PageAsync(QuickAssign<ExLogPageInput>(options.CustomFilters)));
    public Task<bool> DeleteAsync()
        => ExecuteAsync(_exLogClient.DeleteAsync());
}

