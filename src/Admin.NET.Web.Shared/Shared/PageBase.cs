﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System.Reflection;

namespace Admin.NET.Web.Shared.Shared;

public class PageBase : ComponentBase
{
    [CascadingParameter(Name = "CultureName")]
    protected string? Culture { get; set; }

    [Parameter]
    public string HeightStyle { get; set; } = "400px;";

    [Parameter]
    public string HomeUrl { get; set; } = "/";

    [Inject]
    [NotNull]
    protected IJSRuntime? JS { get; set; }

    [Inject]
    [NotNull]
    protected NavigationManager? Navigation { get; set; }

    [Inject]
    [NotNull]
    protected I18n? I18n { get; set; }

    protected string T(string? key, params object[] args)
    {
        return I18n.T(key, args: args);
    }

    protected void HandleOnReturnHome(MouseEventArgs args)
    {
        if (string.IsNullOrWhiteSpace(HomeUrl))
        {
            HomeUrl = "/";
        }

        Navigation.NavigateTo(HomeUrl);
    }
}
