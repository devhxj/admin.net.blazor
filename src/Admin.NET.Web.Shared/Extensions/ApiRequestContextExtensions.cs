﻿using System.Reflection;
using WebApiClientCore;

namespace Admin.NET.Web.Shared.Extensions;

public static class ApiRequestContextExtensions
{
    public static bool IsAllowAnonymous(this ApiRequestContext context)
    {
        return context
            .ActionDescriptor
            .Member
            .GetCustomAttributes<AllowAnonymousAttribute>()
            .Any();
    }
}