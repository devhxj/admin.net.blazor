﻿document.addEventListener("DOMContentLoaded", async function () {
    const RenderMode = { Server: 'Server', WebAssembly: 'WebAssembly', Automatic: 'Automatic' };
    const iframe = document.createElement('iframe');
    const element = document.getElementById('blazor-root-app');
    const renderMode = element.dataset.renderMode;
    const loadingLogo = element.dataset.loadingLogo;
    const loadingText = element.dataset.loadingText;
    const appName = renderMode != RenderMode.Server ? 'webAssembly' : 'server';
    const initBlazorScript = (callback) => {
        let script = document.createElement('script');
        script.id = appName;
        script.onload = callback;
        script.setAttribute('autostart', 'false');
        script.src = `_framework/blazor.${appName}.js`;
        document.head.appendChild(script);
    };
    const addRootComponent = (delay, count) => {
        return new Promise((resolve, reject) => {
            if (count < 1) {
                reject();
                return;
            }
            Blazor.rootComponents.add(element, appName, {}).then(() => console.info(`${appName} initialization completed.`))
                .catch(error => {
                    console.info(error);
                    window.setTimeout(() => addRootComponent(delay, count - 1).then(() => resolve()), delay);
                });
            resolve();
        });
    };
    window.showAppOverlay = () => {
        if (!document.body.dataset.appOverlay) {
            document.body.dataset.appOverlay = 'true';
            document.body.insertAdjacentHTML('afterbegin', `
            <div class='app-overlay' id='app-overlay'>
                <div class='logo'>
                    <div class='one common'></div>
                    <div class='two common'></div>
                    <div class='three common'></div>
                    <div class='four common'></div>
                    <div class='five common'></div>
                    <div class='six common'></div>
                    <div class='seven common'></div>
                    <div class='eight common'></div>
                </div>
                <div class='intro'>
                    <img src='${loadingLogo}'/>
                    <span>${loadingText}</span>
                </div>
                <div class='bar'>
                    <div class='progress'></div>
                </div>
            </div>`);
        }
    };
    window.hideAppOverlay = () => {
        var handler = window.setTimeout(() => {
            if (document.body.dataset.appOverlay) {
                delete document.body.dataset.appOverlay;
                var loader = document.getElementById("app-overlay");
                if (loader) loader.remove();
            }
            if (self != top && top.hideAppOverlay) top.hideAppOverlay();
            window.clearTimeout(handler);
        }, 300);
    };
    window.startBlazorApp = () => Blazor.start().then(() => addRootComponent(300, 20)).catch(error => console.info(error));
    window.notifyAppIsReady = () => {
        console.info(`${appName} is ready.`);
        if (renderMode == RenderMode.Server && self != top)
            top.startBlazorApp();//启动主页面的WebAssembly
        else if (renderMode == RenderMode.Automatic && self == top)
            iframe.contentWindow.Blazor._internal.navigationManager
                .listenForNavigationEvents((uri, state, intercepted) => {
                    window.showAppOverlay();
                    element.style.display = 'block';//显示加载完成的WebAssembly
                    Blazor.navigateTo(uri, false, true);//拦截server的路由，指向切换WebAssembly
                    iframe.style.display = 'none'; //清除并释放server模式iframe
                    iframe.src = 'about:blank';
                    try { iframe.contentWindow.document.write(''); } catch { }
                    iframe.remove();
                    window.hideAppOverlay();
                    return;
                });
    }
    //注册blazor
    window.showAppOverlay();
    if (renderMode == RenderMode.Automatic) {
        initBlazorScript(() => { console.info("Automatic render start."); });
        //用iframe加载server并显示
        iframe.style = 'width:100%;height:100%;position:absolute;z-index:99999;';
        iframe.setAttribute('src', '/_host/server');
        iframe.setAttribute('frameborder', '0');
        iframe.setAttribute('scrolling', 'no');
        iframe.setAttribute('allowfullscreen', 'true');
        iframe.setAttribute('webkitallowfullscreen', 'true');
        iframe.setAttribute('mozallowfullscreen', 'true');
        element.insertAdjacentElement('beforebegin', iframe);
    }
    else initBlazorScript(() => window.startBlazorApp());
});