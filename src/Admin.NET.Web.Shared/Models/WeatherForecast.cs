﻿using System.ComponentModel;

namespace Admin.NET.Web.Shared.Models;

[DisplayName("天气数据")]
public class WeatherForecast
{
    [Description("编号")]
    public Guid Id { get; set; }

    [Description("非空字符串")]
    public string StringType { get; set; } = string.Empty;

    [Description("可空字符串")]
    public string? StringNull { get; set; }

    [Description("非空日期时间")]
    public DateTime DateTimeType { get; set; }

    [Description("可空日期时间")]
    public DateTime? DateTimeNull { get; set; }

    [Description("非空时间戳")]
    public DateTimeOffset DateTimeOffsetType { get; set; }

    [Description("可空时间戳")]
    public DateTimeOffset? DateTimeOffsetNull { get; set; }

    [Description("非空布尔")]
    public bool BooleanType { get; set; }

    [Description("可空布尔")]
    public bool? BooleanNull { get; set; }

    [Description("非空短整")]
    public short Int16Type { get; set; }

    [Description("可空短整")]
    public short? Int16Null { get; set; }

    [Description("非空整数")]
    public int Int32Type { get; set; }

    [Description("可空整数")]
    public int? Int32Null { get; set; }

    [Description("非空长整")]
    public long Int64Type { get; set; }

    [Description("可空长整")]
    public long? Int64Null { get; set; }

    [Description("非空单精度")]
    public float SingleType { get; set; }

    [Description("可空单精度")]
    public float? SingleNull { get; set; }

    [Description("非空双精度")]
    public double DoubleType { get; set; }

    [Description("可空双精度")]
    public double? DoubleNull { get; set; }

    [Description("非空十进制")]
    public decimal DecimalType { get; set; }

    [Description("可空十进制")]
    public decimal? DecimalNull { get; set; }

    [Description("非空枚举")]
    public Operate EnumType { get; set; }

    [Description("可空枚举")]
    public Operate? EnumNull { get; set; }

    public List<WeatherForecast>? Children  { get; set; }

    public override string ToString()
    {
        return Id.ToString();
    }
}

[Description("测试枚举")]
public enum Operate
{
    [Description("添加")]
    Add,

    [Description("修改")]
    Update,

    [Description("删除")]
    Delete
}