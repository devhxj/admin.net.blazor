﻿namespace Admin.NET.Web.Shared.Models;

public class WeatherForecastService
{
    private static readonly Random _random = new();
    private static readonly List<WeatherForecast> _data = BuildBogus(50);
    public Task<IEnumerable<WeatherForecast>> GetForecastAsync() => Task.FromResult<IEnumerable<WeatherForecast>>(_data);

    public async Task<(int, IEnumerable<WeatherForecast>)> GetForecastAsync(GridViewLoadDataArgs args)
    {
        var query = _data.AsQueryable();
        query = QueryableExtension.Where(query, args.Filter);
        query = QueryableExtension.OrderBy(query, args.Orders);

        var count = query.Count();

        if (args.AllowPaging)
            query = query.Skip(args.Skip);

        if (args.AllowPaging)
            query = query.Take(args.Top);

        return await Task.FromResult<(int, IEnumerable<WeatherForecast>)>((count, query));
    }

    public Task<IEnumerable<WeatherForecast>> GetChildForecastAsync(WeatherForecast item)
    {
        var items = BuildBogus(_random.Next(5, 10));
        return Task.FromResult(items.AsEnumerable());
    }

    public Task<bool> SaveAsync(IEnumerable<WeatherForecast> items, bool isAdd)
    {
        if (isAdd)
        {
            _data.AddRange(items);
        }

        return Task.FromResult(true);
    }

    public Task<bool> DeleteAsync(IEnumerable<WeatherForecast> items)
    {
        foreach (var item in items)
        {
            _data.Remove(item);
        }

        return Task.FromResult(true);
    }

    public Task<IEnumerable<string>> GetPermissionsAsync()
    {
        //var property = typeof(Pages.Index).GetProperty("Authorities", BindingFlags.Public | BindingFlags.Static);
        //var permissions = property?.GetPropertyValue(null) as IEnumerable<Permission>;
        //var codes = permissions?.Select(x => x.Code) ?? Enumerable.Empty<string>();

        //return Task.FromResult(codes);

        return Task.FromResult(new string[] { "1001","1002","1003" }.AsEnumerable());
    }

    private static List<WeatherForecast> BuildBogus(int size)
    {
        var list = new List<WeatherForecast>();
        for (var i = 0; i < size; i++)
        {
            list.Add(new()
            {
                Id = Guid.NewGuid(),
                StringType = "StringType" + i,
                StringNull = "StringType" + i,
                DateTimeType = DateTime.Now,
                DateTimeNull = DateTime.Now,
                DateTimeOffsetType = DateTime.Now,
                DateTimeOffsetNull = DateTime.Now,
                BooleanType = size % 3 == 0,
                BooleanNull = size % 7 == 0,
                Int16Type = (short)(size / 3),
                Int16Null = (short)(size / 2),
                Int32Type = size / 3,
                Int32Null = size / 4,
                Int64Type = size / 5,
                Int64Null = size / 6,
                SingleType = (float)(size * 1.0 / 3),
                SingleNull = (float)(size * 1.0 / 3),
                DoubleType = size * 1.0 / 4,
                DoubleNull = size * 1.0 / 9,
                DecimalType = size % 7,
                DecimalNull = size % 5,
                EnumType = (Operate)(size % 3),
                EnumNull = (Operate)(size % 3),
                Children = null,
            });
        }
        return list;
    }
}
