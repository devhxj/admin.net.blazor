﻿namespace Admin.NET.Web.Shared.Misc;

/// <summary>
/// 公开配置项
/// </summary>
public class PublicOptions
{
    public string ApiBaseAddress { get; set; } = "http://localhost:9001";

    public SpaRenderMode SpaRenderMode { get; set; }

    public bool IsDevelopment { get; set; }
}
