﻿namespace Admin.NET.Application;

/// <summary>
/// 验证码输出参数
/// </summary>
public class ClickWordCaptchaResult
{
    /// <summary>
    /// 
    /// </summary>
    public string RepCode { get; set; } = "0000";

    /// <summary>
    /// 
    /// </summary>
    public string RepMsg { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public RepData RepData { get; set; } = new RepData();

    /// <summary>
    /// 
    /// </summary>
    public bool Error { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public bool Success { get; set; } = true;
}

/// <summary>
/// 
/// </summary>
public class RepData
{
    public string CaptchaId { get; set; }

    public string ProjectCode { get; set; }

    public string CaptchaType { get; set; }

    public string CaptchaOriginalPath { get; set; }

    public string CaptchaFontType { get; set; }

    public string CaptchaFontSize { get; set; }

    public string SecretKey { get; set; }

    public string OriginalImageBase64 { get; set; }

    public List<PointPosModel> Point { get; set; } = new List<PointPosModel>();

    public string JigsawImageBase64 { get; set; }
    public List<string> WordList { get; set; } = new List<string>();

    public string PointList { get; set; }

    public string PointJson { get; set; }

    public string Token { get; set; }

    public bool Result { get; set; }

    public string CaptchaVerification { get; set; }
}