﻿namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface IClickWordCaptcha
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<ClickWordCaptchaResult> CheckCode(ClickWordCaptchaInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="code"></param>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="point"></param>
    /// <returns></returns>
    Task<ClickWordCaptchaResult> CreateCaptchaImage(string code, int width, int height, int point = 3);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="number"></param>
    /// <returns></returns>
    string RandomCode(int number);
}