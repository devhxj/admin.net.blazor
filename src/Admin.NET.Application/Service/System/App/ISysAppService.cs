﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysAppService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task AddApp(AddAppInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task DeleteApp(DeleteAppInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<AppOutput?> GetApp([FromQuery] QueryAppInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<List<AppOutput>> GetAppList();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    Task<List<AppNavNodeOutput>> GetLoginApps(long userId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<AppOutput>> QueryAppPageList([FromQuery] AppPageInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task SetAsDefault(SetDefaultAppInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateApp(UpdateAppInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task ChangeUserAppStatus(ChangeUserAppStatusInput input);
}