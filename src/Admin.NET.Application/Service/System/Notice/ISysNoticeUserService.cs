﻿namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysNoticeUserService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="noticeId"></param>
    /// <param name="noticeUserIdList"></param>
    /// <param name="noticeUserStatus"></param>
    /// <returns></returns>
    Task Add(long noticeId, List<long> noticeUserIdList, NoticeUserStatus noticeUserStatus);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="noticeId"></param>
    /// <returns></returns>
    Task<List<SysNoticeUser>> GetNoticeUserListByNoticeId(long noticeId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="noticeId"></param>
    /// <param name="userId"></param>
    /// <param name="status"></param>
    /// <returns></returns>
    Task Read(long noticeId, long userId, NoticeUserStatus status);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="noticeId"></param>
    /// <param name="noticeUserIdList"></param>
    /// <param name="noticeUserStatus"></param>
    /// <returns></returns>
    Task Update(long noticeId, List<long> noticeUserIdList, NoticeUserStatus noticeUserStatus);
}