﻿namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysOnlineUserService
{
    /// <summary>
    /// 分页获取在线用户信息
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<OnlineUserOutput>> QueryOnlineUserPageList(OnlineUserPageInput input);

    /// <summary>
    /// 获取在线用户信息
    /// </summary>
    /// <returns></returns>
    Task<List<OnlineUserOutput>> List();

    /// <summary>
    /// 强制下线
    /// </summary>
    /// <param name="onlineUser">在线用户信息</param>
    /// <returns></returns>
    Task ForceExist(OnlineUserOutput onlineUser);
}