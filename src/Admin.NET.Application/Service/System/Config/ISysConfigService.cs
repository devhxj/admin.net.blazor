﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysConfigService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task AddConfig(AddConfigInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task DeleteConfig(DeleteConfigInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<ConfigOutput> GetConfig([FromQuery] QueryConfigInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<List<ConfigOutput>> GetConfigList();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<ConfigOutput>> QueryConfigPageList([FromQuery] ConfigPageInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateConfig(UpdateConfigInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<bool> GetDemoEnvFlag();

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<bool> GetCaptchaOpenFlag();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="code"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    Task UpdateConfigCache(string code, string value);
}