﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysDictTypeService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task AddDictType(AddDictTypeInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task ChangeDictTypeStatus(ChangeStateDictTypeInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task DeleteDictType(DeleteDictTypeInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<List<DictTreeOutput>> GetDictTree();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<DictTypeOutput> GetDictType([FromQuery] QueryDictTypeInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<DictDataOutput>> GetDictTypeDropDown([FromQuery] DropDownDictTypeInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<List<DictTypeOutput>> GetDictTypeList();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<DictTypeOutput>> QueryDictTypePageList([FromQuery] DictTypePageInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateDictType(UpdateDictTypeInput input);
}