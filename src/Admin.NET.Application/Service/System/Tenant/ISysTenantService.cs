﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysTenantService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task AddTenant(AddTenantInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task DeleteTenant(DeleteTenantInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<TenantOutput> GetTenant([FromQuery] QueryTenantInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task GrantMenu(GrantRoleMenuInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="newTenant"></param>
    /// <returns></returns>
    Task InitNewTenant(SysTenant newTenant);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<long>> OwnMenu([FromQuery] QueryTenantInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<TenantOutput>> QueryTenantPageList([FromQuery] TenantPageInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task ResetUserPwd(QueryTenantInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateTenant(UpdateTenantInput input);
}