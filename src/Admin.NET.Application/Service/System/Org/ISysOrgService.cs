﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysOrgService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task AddOrg(OrgAddInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task DeleteOrg(DeleteOrgInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dataScopeType"></param>
    /// <param name="orgId"></param>
    /// <returns></returns>
    Task<List<long>> GetDataScopeListByDataScopeType(int dataScopeType, long orgId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<OrgOutput> GetOrg([FromQuery] QueryOrgInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<OrgOutput>> GetOrgList([FromQuery] OrgListInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<List<OrgTreeNode>> GetOrgTree();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<OrgOutput>> QueryOrgPageList([FromQuery] OrgPageInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateOrg(UpdateOrgInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<List<long>> GetAllDataScopeIdList();

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<List<long>> GetUserDataScopeIdList();
}