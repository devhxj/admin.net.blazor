﻿using Microsoft.AspNetCore.Mvc;

namespace Admin.NET.Application;

/// <summary>
/// 
/// </summary>
public interface ISysRoleService
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task AddRole(AddRoleInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task DeleteRole(DeleteRoleInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="roleId"></param>
    /// <returns></returns>
    Task<string> GetNameByRoleId(long roleId);

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    Task<List<RoleOutput>> GetRoleDropDown();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<SysRole> GetRoleInfo([FromQuery] QueryRoleInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<dynamic> GetRoleList([FromQuery] RoleInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="roleIdList"></param>
    /// <param name="orgId"></param>
    /// <returns></returns>
    Task<List<long>> GetUserDataScopeIdList(List<long> roleIdList, long orgId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    Task<List<RoleOutput>> GetUserRoleList(long userId);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task GrantData(GrantRoleDataInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task GrantMenu(GrantRoleMenuInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<long>> OwnData([FromQuery] QueryRoleInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<List<long>> OwnMenu([FromQuery] QueryRoleInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task<PageResult<SysRole>> QueryRolePageList([FromQuery] RolePageInput input);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    Task UpdateRole(UpdateRoleInput input);
}