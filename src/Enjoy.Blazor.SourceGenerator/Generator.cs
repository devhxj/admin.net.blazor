﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Enjoy.Blazor.SourceGenerator
{
    [Generator]
    public class Generator : ISourceGenerator
    {
        public void Execute(GeneratorExecutionContext context)
        {
            context.AddSource(nameof(Constants.AuthorizeResourceAttribute), SourceText.From(Constants.AuthorizeResourceAttribute, Encoding.UTF8));
            context.AddSource(nameof(Constants.AuthorizeResourcesAttribute), SourceText.From(Constants.AuthorizeResourcesAttribute, Encoding.UTF8));

            if (context.SyntaxReceiver is not SyntaxReceiver receiver)
            {
                return;
            }

            var completes = new List<string>();
            foreach (var page in receiver.PageClasses)
            {
                if (completes.Contains(page.Identifier.Text))
                {
                    return;
                }

                var sources = new StringBuilder();
                var classes = receiver.PageClasses.Where(x => x.Identifier.Text == page.Identifier.Text);
                foreach (var @class in classes) 
                {
                    var model = context.Compilation.GetSemanticModel(@class.SyntaxTree);
                    foreach (var list in @class.AttributeLists)
                    {
                        foreach (var attribute in list.Attributes)
                        {
                            if (attribute is null || attribute.ArgumentList is null)
                            {
                                continue;
                            }

                            var attributeName = attribute.Name.ToString();
                            if (attributeName == Constants.AuthorizeSingle)
                            {
                                var resource = GetSingleResource(model, attribute);
                                if (!string.IsNullOrWhiteSpace(resource))
                                {
                                    sources.AppendLine(resource);
                                }
                            }
                            else if (attributeName == Constants.AuthorizeMultiple)
                            {
                                //System.Diagnostics.Debugger.Launch();
                                var resource = GetMultipleResource(model, attribute);
                                if (!string.IsNullOrWhiteSpace(resource))
                                {
                                    sources.Append(resource);
                                }
                            }
                        }
                    }
                }

                if (sources.Length > 0)
                {
                    var @namespace = GetNamespace(page);
                    var name = page.Identifier.Text;
                    var text = $@"using Enjoy.Blazor;

{(string.IsNullOrWhiteSpace(@namespace) ? string.Empty : $"namespace {@namespace}")}
{{
    public partial class {name}
    {{
{sources}
    }}
}}";

                    context.AddSource($"{name}Resource", SourceText.From(text, Encoding.UTF8));
                }

                completes.Add(page.Identifier.Text);
            }
        }

        public void Initialize(GeneratorInitializationContext context)
        {
            //System.Diagnostics.Debugger.Launch();
            context.RegisterForSyntaxNotifications(() => new SyntaxReceiver());
        }

        /// <summary>
        /// determine the namespace the class/enum/struct is declared in, if any
        /// </summary>
        /// <param name="syntax"></param>
        /// <returns></returns>
        private static string GetNamespace(BaseTypeDeclarationSyntax syntax)
        {
            // If we don't have a namespace at all we'll return an empty string
            // This accounts for the "default namespace" case
            string nameSpace = string.Empty;

            // Get the containing syntax node for the type declaration
            // (could be a nested type, for example)
            SyntaxNode? potentialNamespaceParent = syntax.Parent;

            // Keep moving "out" of nested classes etc until we get to a namespace
            // or until we run out of parents
            while (potentialNamespaceParent != null &&
                    potentialNamespaceParent is not NamespaceDeclarationSyntax
                    && potentialNamespaceParent is not FileScopedNamespaceDeclarationSyntax)
            {
                potentialNamespaceParent = potentialNamespaceParent.Parent;
            }

            // Build up the final namespace by looping until we no longer have a namespace declaration
            if (potentialNamespaceParent is BaseNamespaceDeclarationSyntax namespaceParent)
            {
                // We have a namespace. Use that as the type
                nameSpace = namespaceParent.Name.ToString();

                // Keep moving "out" of the namespace declarations until we 
                // run out of nested namespace declarations
                while (true)
                {
                    if (namespaceParent.Parent is not NamespaceDeclarationSyntax parent)
                    {
                        break;
                    }

                    // Add the outer namespace as a prefix to the final namespace
                    nameSpace = $"{namespaceParent.Name}.{nameSpace}";
                    namespaceParent = parent;
                }
            }

            // return the final namespace
            return nameSpace;
        }

        /// <summary>
        /// 解析单个资源定义
        /// </summary>
        /// <returns></returns>
        private static string? GetSingleResource(SemanticModel model, AttributeSyntax attribute)
        {
            if (attribute.ArgumentList is null || attribute.ArgumentList.Arguments.Count < 2)
            {
                return null;
            }

            var arguments = attribute.ArgumentList.Arguments;
            var arg01 = model.GetConstantValue(arguments[0].Expression).Value;
            var arg02 = model.GetConstantValue(arguments[1].Expression).Value;
            return $"        public readonly string _{arg01} = \"{arg02}\";";
        }

        /// <summary>
        /// 解析多个资源定义
        /// </summary>
        /// <returns></returns>
        private static string? GetMultipleResource(SemanticModel model, AttributeSyntax attribute)
        {
            if (attribute.ArgumentList is null || attribute.ArgumentList.Arguments.Count < 1)
            {
                return null;
            }

            var builder = new StringBuilder();
            for (var index = 0; index < attribute.ArgumentList.Arguments.Count; index++)
            {
                var value = model
                    .GetConstantValue(attribute.ArgumentList.Arguments[index].Expression)
                    .Value?
                    .ToString();

                if (value is not null && !string.IsNullOrWhiteSpace(value))
                {
                    var values = value.Split('|');
                    if (values.Length > 1)
                    {
                        var arg01 = values[0];
                        var arg02 = values[1];

                        builder.AppendLine($"        public readonly string _{arg01} = \"{arg02}\";");
                    }
                }
            }

            return builder.ToString();
        }
    }
}