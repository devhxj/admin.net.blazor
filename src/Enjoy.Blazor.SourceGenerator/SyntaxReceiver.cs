﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System.Collections.Generic;
using System.Linq;

namespace Enjoy.Blazor.SourceGenerator
{
    internal class SyntaxReceiver : ISyntaxReceiver
    {
        /// <summary>
        /// 需要生成的页面类
        /// </summary>
        internal List<ClassDeclarationSyntax> PageClasses { get; } = new();

        public void OnVisitSyntaxNode(SyntaxNode syntaxNode)
        {
            if (syntaxNode is ClassDeclarationSyntax @class && @class.AttributeLists.Count > 0)
            {
                if (@class.AttributeLists.SelectMany(x => x.Attributes).Any(IsAuthorizeAttribute))
                {
                    PageClasses.Add(@class);
                }
            }
        }

        private static bool IsAuthorizeAttribute(AttributeSyntax syntax)
        {
            var name = syntax.Name.ToString();
            return name == Constants.AuthorizeSingle || name == Constants.AuthorizeMultiple;
        }
    }
}