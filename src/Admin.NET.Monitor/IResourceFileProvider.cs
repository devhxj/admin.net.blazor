﻿using Microsoft.Extensions.FileProviders;

namespace Admin.NET.Monitor;

internal interface IResourceFileProvider : IFileProvider
{
}
