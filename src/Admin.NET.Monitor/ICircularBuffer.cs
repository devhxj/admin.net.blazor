﻿namespace Admin.NET.Monitor;

public interface ICircularBuffer<T> : IEnumerable<T>
{
    void Add(T item);
}
