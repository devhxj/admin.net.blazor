﻿namespace Admin.NET.Monitor;

internal class ProfileSpan
{
    public IEnumerable<KeyValuePair<string, string?>> Baggage { get; internal set; } = Enumerable.Empty<KeyValuePair<string, string?>>();

    public string DisplayName { get; internal set; } = string.Empty;

    public TimeSpan Duration { get; internal set; }

    public string Id { get; internal set; } = string.Empty;

    public string? ParentId { get; internal set; }

    public DateTime StartTimeUtc { get; internal set; }

    public string ActivitySourceName { get; internal set; } = string.Empty;

    public IEnumerable<KeyValuePair<string, object?>> Tags { get; internal set; } = Enumerable.Empty<KeyValuePair<string, object?>>();
}
