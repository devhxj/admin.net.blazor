﻿using System.Collections.Concurrent;

namespace Admin.NET.Monitor;

internal class ProfileSession
{
    private readonly ConcurrentBag<ProfileSpan> _spans = new();

    public IEnumerable<KeyValuePair<string, string?>> Baggage { get; internal set; } = Enumerable.Empty<KeyValuePair<string, string?>>();

    public string DisplayName { get; internal set; } = string.Empty;

    public TimeSpan Duration { get; internal set; }

    public string RootId { get; internal set; } = string.Empty;

    public IEnumerable<ProfileSpan> Spans => _spans.ToArray();

    public DateTime StartTimeUtc { get; internal set; }

    public IEnumerable<KeyValuePair<string, object?>> Tags { get; internal set; } = Enumerable.Empty<KeyValuePair<string, object?>>();

    public string TraceId { get; internal set; } = string.Empty;

    public void AddSpan(ProfileSpan span)
    {
        _spans.Add(span);
    }
}
