﻿using System.Collections.Immutable;
using System.Diagnostics;

namespace Admin.NET.Monitor.Filters;

public class UrlPathFilter : IProfileFilter
{
    private ImmutableArray<PathString> _paths;

    public UrlPathFilter(params PathString[] paths)
    {
        _paths = paths.ToImmutableArray();
    }

    public bool Filtering(Activity activity)
        => _paths.Any(path => activity.DisplayName.StartsWith(path));
}
