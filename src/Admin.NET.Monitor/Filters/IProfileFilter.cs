﻿using System.Diagnostics;

namespace Admin.NET.Monitor.Filters;

public interface IProfileFilter
{
    bool Filtering(Activity activity);
}
