﻿using System.Diagnostics;

namespace Admin.NET.Monitor.Filters;

public class ProfilePathFilter : IProfileFilter
{
    public bool Filtering(Activity activity)
        => activity.DisplayName.StartsWith("/profiler/");
}
