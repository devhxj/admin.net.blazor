using Admin.NET.Web.Shared;
using Admin.NET.Web.Shared.Extensions;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

var builder = WebAssemblyHostBuilder.CreateDefault(args);

using var httpClient = new HttpClient();
var apiBaseAddress = await httpClient.GetStringAsync(Path.Combine(builder.HostEnvironment.BaseAddress, "ApiBaseAddress"));
if (string.IsNullOrWhiteSpace(apiBaseAddress))
{
    apiBaseAddress = builder.HostEnvironment.BaseAddress;
}

builder.RootComponents.RegisterForJavaScript<App>("webAssembly");
builder.Services.RegisterAppServices(apiBaseAddress);
await builder.Build().RunAsync();
