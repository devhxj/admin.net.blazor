﻿using Omu.ValueInjecter;
using Omu.ValueInjecter.Injections;
using System.Linq.Expressions;

namespace Admin.NET.Contract;

/// <summary>
/// DTO扩展方法
/// </summary>
public static class DtoExtensions
{
    /// <summary>
    /// 转换成目标对象
    /// </summary>
    /// <typeparam name="TDestination">目标对象</typeparam>
    /// <param name="source">要转换的对象</param>
    /// <returns></returns>
    public static TDestination Map<TDestination>(this IDto? source)
        where TDestination : class, new()
        => (source is null) ? new() : Mapper.Map<TDestination>(source);

    /// <summary>
    /// 转换成目标对象
    /// </summary>
    /// <typeparam name="TSource">自身类型</typeparam>
    /// <typeparam name="TDestination">目标类型</typeparam>
    /// <param name="source">要转换的对象</param>
    /// <returns></returns>
    public static IEnumerable<TDestination> Map<TSource, TDestination>(this IEnumerable<TSource>? source)
        where TSource : class, IDto
        where TDestination : class, new()
    {
        if (source is null)
            return Enumerable.Empty<TDestination>();

        return source.Select(x => Mapper.Map<TSource, TDestination>(x));
    }

    /// <summary>
    /// 转换成目标对象
    /// </summary>
    /// <typeparam name="TDestination">目标对象</typeparam>
    /// <param name="source">要转换的对象</param>
    /// <returns></returns>
    public static TDestination? NullableMap<TDestination>(this IDto? source)
        where TDestination : class
        => (source is null) ? default: Mapper.Map<TDestination>(source);

    /// <summary>
    /// 创建目标对象，并从指定对象中拷贝同名属性值
    /// </summary>
    /// <param name="source">要拷贝的数据源</param>
    /// <param name="ignoredProps">忽略的属性</param>
    /// <returns></returns>
    public static TDestination Assign<TDestination>(object? source, string[]? ignoredProps = null)
        where TDestination : class, IDto, new()
    {
        var dest = new TDestination();

        if (source is not null)
        {
            dest.InjectFrom(new LoopInjection(ignoredProps ?? Array.Empty<string>()), source);
        }

        return dest;
    }

    /// <summary>
    /// 创建目标对象，并从多个对象中拷贝同名属性值
    /// </summary>
    /// <param name="source">要拷贝的数据源</param>
    /// <returns></returns>
    public static TDestination Assign<TDestination>(params object?[] sources)
        where TDestination : class, IDto, new()
    {
        var dest = new TDestination();

        foreach (var source in sources)
        {
            if (source is not null)
            {
                dest.InjectFrom(new LoopInjection(), source);
            }
        }

        return dest;
    }

    /// <summary>
    /// 设置属性值
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="data">目标对象</param>
    /// <param name="expression">属性值表达式</param>
    /// <param name="value">目标值</param>
    public static TItem SetValue<TItem, TValue>(this TItem data, Expression<Func<TItem, TValue>> expression, TValue value)
        where TItem : class
    {
        var member = expression.Body as MemberExpression
            ?? (MemberExpression)((UnaryExpression)expression.Body).Operand;
        var body = Expression.Assign(member, Expression.Constant(value));
        var lambda = Expression.Lambda<Action<TItem>>(body, expression.Parameters);
        lambda.Compile().Invoke(data);
        return data;
    }

    /// <summary>
    /// 设置属性值
    /// </summary>
    /// <typeparam name="TItem"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    /// <param name="data">目标对象</param>
    /// <param name="expression">属性值表达式</param>
    /// <param name="value">目标值</param>
    public static TItem? NullableSetValue<TItem, TValue>(this TItem? data, Expression<Func<TItem, TValue>> expression, TValue value)
        where TItem : class
    {
        if (data is not null)
        {
            return SetValue(data, expression, value);
        }

        return data;
    }
}

