﻿namespace Admin.NET.Contract;

/// <summary>
/// 
/// </summary>
/// <typeparam name="TData"></typeparam>
/// <typeparam name="TKey"></typeparam>
public class NavigationNode<TData, TKey> where TKey : IEquatable<TKey>
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="key"></param>
    /// <param name="indent"></param>
    public NavigationNode(TKey key, int indent)
    {
        Key = key;
        Children = Enumerable.Empty<NavigationNode<TData, TKey>>();
        Indent = indent;
    }

    /// <summary>
    /// 获得 当前菜单所在层次 从 0 开始
    /// </summary>
    public int Indent { get; private set; }

    public string? Title { get; set; }

    public string? SubTitle { get; set; }

    public string? Icon { get; set; }

    public TKey Key { get; set; }

    public bool Expanded { get; set; }

    public TData? Data { get; set; }

    public IEnumerable<NavigationNode<TData, TKey>> Children { get; set; }

    public NavigationNode<TData, TKey>? Parent { get; set; }

    /// <summary>
    /// 获得/设置 是否选中当前菜单触发器  默认 false 未选中 
    /// </summary>
    public bool IsHoverToggle { get; set; }

    /// <summary>
    /// 获得/设置 是否选中当前菜单子菜单  默认 false 未选中 
    /// </summary>
    public bool IsHoverList { get; set; }

    /// <summary>
    /// 获得 是否选中当前节点
    /// </summary>
    public bool Visible => IsHoverToggle || IsHoverList;
}
