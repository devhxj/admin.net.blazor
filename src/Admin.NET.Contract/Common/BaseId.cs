﻿namespace Admin.NET.Contract;

/// <summary>
/// 约定业务号实体
/// </summary>
public class BaseId<TKey> : IDto where TKey : struct, IEquatable<TKey>
{
    /// <summary>
    /// 主键Id
    /// </summary>
    [DisplayName("业务号")]
    [Required(ErrorMessage = "业务号不能为空")]
    //[DataValidation(ValidationTypes.Numeric)]
    public virtual TKey Id { get; set; }
}