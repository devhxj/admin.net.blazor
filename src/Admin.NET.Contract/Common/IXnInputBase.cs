﻿namespace Admin.NET.Contract;

/// <summary>
/// 通用输入扩展参数（带权限）
/// </summary>
public interface IXnInputBase
{
    /// <summary>
    /// 授权菜单
    /// </summary>
    public List<long> GrantMenuIdList { get; set; }

    /// <summary>
    /// 授权角色
    /// </summary>
    public List<long> GrantRoleIdList { get; set; }

    /// <summary>
    /// 授权数据
    /// </summary>
    public List<long> GrantOrgIdList { get; set; }
}