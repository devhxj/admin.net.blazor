namespace Admin.NET.Contract.System;

/// <summary>
/// 请求日志参数
/// </summary>
public class OpLogPageInput : PageInputBase, IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 是否执行成功（Y-是，N-否）
    /// </summary>
    [DisplayName("是否成功")]
    public YesOrNot? Success { get; set; }

    /// <summary>
    /// 请求方式
    /// </summary>
    [DisplayName("请求方式")]
    public string? ReqMethod { get; set; }
}