﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 异常日志参数
/// </summary>
public class ExLogPageInput : PageInputBase, IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 类名
    /// </summary>
    [DisplayName("类名")]
    public string? ClassName { get; set; }

    /// <summary>
    /// 方法名
    /// </summary>
    [DisplayName("方法名")]
    public string? MethodName { get; set; }

    /// <summary>
    /// 异常信息
    /// </summary>
    [MaxLength(2000)]
    [DisplayName("异常信息")]
    public string? ExceptionMsg { get; set; }
}