﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 访问日志参数
/// </summary>
public class VisLogOutput : IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 是否执行成功（Y-是，N-否）
    /// </summary>
    [DisplayName("是否成功")]
    public YesOrNot Success { get; set; }

    /// <summary>
    /// 具体消息
    /// </summary>
    [DisplayName("具体消息")]
    public string? Message { get; set; }

    /// <summary>
    /// IP
    /// </summary>
    [DisplayName("IP地址")]
    public string? Ip { get; set; }

    /// <summary>
    /// 地址
    /// </summary>
    [DisplayName("地址")]
    public string? Location { get; set; }

    /// <summary>
    /// 浏览器
    /// </summary>
    [DisplayName("浏览器")]
    public string? Browser { get; set; }

    /// <summary>
    /// 操作系统
    /// </summary>
    [DisplayName("操作系统")]
    public string? Os { get; set; }

    /// <summary>
    /// 访问类型
    /// </summary>
    [DisplayName("访问类型")]
    public LoginType VisType { get; set; }

    /// <summary>
    /// 访问时间
    /// </summary>
    [DisplayName("访问时间")]
    public DateTimeOffset? VisTime { get; set; }

    /// <summary>
    /// 访问人
    /// </summary>
    [DisplayName("访问人")]
    public string? Account { get; set; }
}