﻿namespace Admin.NET.Contract.System;

public class DropDownDictTypeInput : BaseId<long>
{
    /// <summary>
    /// 编码
    /// </summary>
    [Required(ErrorMessage = "字典类型编码不能为空")]
    public string? Code { get; set; }
}
