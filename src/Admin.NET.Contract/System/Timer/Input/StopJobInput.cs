﻿namespace Admin.NET.Contract.System;

public class StopJobInput : IDto
{
    public string? JobName { get; set; }
}
