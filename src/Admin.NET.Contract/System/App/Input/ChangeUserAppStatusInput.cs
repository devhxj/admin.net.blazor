﻿namespace Admin.NET.Contract.System;

public class ChangeUserAppStatusInput : BaseId<long>
{
    [DisplayName("状态")]
    public CommonStatus Status { get; set; }
}
