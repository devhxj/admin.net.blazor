﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 分页用，参数自己添加
/// </summary>
public class AppPageInput : PageInputBase, IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    public virtual string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public virtual string? Code { get; set; }
}