﻿namespace Admin.NET.Contract.System;

public class UpdateAppInput : BaseId<long>
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [DisplayName("编码")]
    public string? Code { get; set; }

    /// <summary>
    /// 图标
    /// </summary>
    [DisplayName("图标")]
    [MaxLength(20)]
    public string? Icon { get; set; }

    /// <summary>
    /// 是否默认激活（Y-是，N-否）,只能有一个系统默认激活
    /// <br/>用户登录后默认展示此系统菜单
    /// </summary>
    [DisplayName("是否激活")]
    public string Active { get; set; } = "N";


    [DisplayName("状态")]
    public CommonStatus Status { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    [DisplayName("排序")]
    public int Sort { get; set; }
}
