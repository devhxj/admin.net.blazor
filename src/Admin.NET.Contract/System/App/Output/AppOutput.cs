﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 系统应用参数
/// </summary>
public class AppOutput : BaseId<long>
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [DisplayName("编码")]
    public string? Code { get; set; }

    /// <summary>
    /// 图标
    /// </summary>
    [DisplayName("图标")]
    public string? Icon { get; set; }

    /// <summary>
    /// 是否默认
    /// </summary>
    [DisplayName("是否默认")]
    public string? Active { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    [DisplayName("状态")]
    public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

    /// <summary>
    /// 排序
    /// </summary>
    [DisplayName("排序")]
    public int Sort { get; set; }
}

