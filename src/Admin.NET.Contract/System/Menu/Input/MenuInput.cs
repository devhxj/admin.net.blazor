﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 菜单参数
/// </summary>
public class MenuInput : IDto
{
    /// <summary>
    /// 菜单类型（字典 0目录 1菜单 2按钮）
    /// </summary>
    public MenuType Type { get; set; }

    /// <summary>
    /// 路由地址
    /// </summary>
    public string? Router { get; set; }

    /// <summary>
    /// 权限标识
    /// </summary>
    public string? Permission { get; set; }

    /// <summary>
    /// 打开方式（字典 0无 1组件 2内链 3外链）
    /// </summary>
    public MenuOpenType OpenType { get; set; }
}
