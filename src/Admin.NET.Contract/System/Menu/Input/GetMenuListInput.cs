﻿namespace Admin.NET.Contract.System;

public class GetMenuListInput : IDto
{
    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// 应用分类（应用编码）
    /// </summary>
    public string? Application { get; set; }
}
