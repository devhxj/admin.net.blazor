﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 菜单树（列表形式）
/// </summary>
public class MenuOutput : BaseId<long>, INotifyPropertyChanged
{
    private long _pid;
    private string? _router;
    private string? _redirect;
    private MenuType _menuType = MenuType.DIR;
    private MenuOpenType _openType = MenuOpenType.NONE;

    /// <summary>
    /// 父Id
    /// </summary>
    [DisplayName("上级菜单")]
    public long Pid
    {
        get
        {
            if (Type == MenuType.DIR)
            {
                return 0;
            }
            return _pid;
        }
        set => _pid = value;
    }

    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("菜单名称")]
    [Required]
    public string? Name { get; set; }

    /// <summary>
    /// 菜单编号
    /// </summary>
    [DisplayName("菜单编号")]
    [Required]
    public string? Code { get; set; }

    /// <summary>
    /// 应用分类（应用编码）
    /// </summary>
    [DisplayName("所属应用")]
    [Required]
    public string? Application { get; set; }

    /// <summary>
    /// 图标
    /// </summary>
    [DisplayName("图标")]
    public string? Icon { get; set; }

    /// <summary>
    /// 路由地址
    /// </summary>
    [DisplayName("路由地址")]
    public string? Router
    {
        get
        {
            if (Type != MenuType.MENU)
            {
                return null;
            }
            return _router;
        }
        set => _router = value;
    }

    /// <summary>
    /// 重定向地址
    /// </summary>
    [DisplayName("定向地址")]
    public string? Redirect
    {
        get
        {
            if (Type != MenuType.MENU || OpenType != MenuOpenType.OUTER)
            {
                return null;
            }
            return _redirect;
        }
        set => _redirect = value;
    }

    /// <summary>
    /// 权限标识
    /// </summary>
    [DisplayName("权限标识")]
    public string? Permission { get; set; }

    /// <summary>
    /// 菜单类型（字典 0目录 1菜单 2按钮）
    /// </summary>
    [DisplayName("菜单类型")]
    [Required]
    public MenuType Type
    {
        get => _menuType;
        set
        {
            _menuType = value;
            NotifyPropertyChanged(nameof(Type));
        }
    }

    /// <summary>
    /// 打开方式（字典 0无 2内链 3外链）
    /// </summary>
    [DisplayName("打开方式")]
    [Required]
    public MenuOpenType OpenType
    {
        get
        {
            if (Type == MenuType.MENU)
            {
                return _openType == MenuOpenType.NONE ? MenuOpenType.INNER : _openType;
            }
            return MenuOpenType.NONE;
        }
        set
        {
            _openType = value;
            NotifyPropertyChanged(nameof(OpenType));
        }
    }

    /// <summary>
    /// 是否可见（Y-是，N-否）
    /// </summary>
    [DisplayName("是否可见")]
    public string? Visible { get; set; }

    /// <summary>
    /// 权重（字典 1系统权重 2业务权重）
    /// </summary>
    [DisplayName("权重")]
    public MenuWeight Weight { get; set; } = MenuWeight.DEFAULT_WEIGHT;

    /// <summary>
    /// 排序
    /// </summary>
    [DisplayName("排序")]
    public int Sort { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("备注")]
    public string? Remark { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public event PropertyChangedEventHandler? PropertyChanged;

    /// <summary>
    /// 通知属性发生改变
    /// </summary>
    /// <param name="propertyName"></param>
    public virtual void NotifyPropertyChanged(string propertyName)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
