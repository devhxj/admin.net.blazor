﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 登录用户角色参数
/// </summary>
public class RoleOutput : BaseId<long>
{
    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("名称")]
    [Required, MaxLength(20)]
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [DisplayName("编码")]
    [Required, MaxLength(50)]
    public string? Code { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    [DisplayName("排序")]
    public int Sort { get; set; }

    /// <summary>
    /// 数据范围类型（字典 1全部数据 2本部门及以下数据 3本部门数据 4仅本人数据 5自定义数据）
    /// </summary>
    [DisplayName("数据范围")]
    public DataScopeType DataScopeType { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("备注")]
    [MaxLength(100)]
    public string? Remark { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    [DisplayName("状态")]
    public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

    /// <summary>
    /// 角色类型-集团角色_0、加盟商角色_1、门店角色_2
    /// </summary>
    [DisplayName("角色类型")]
    public RoleTypeEnum RoleType { get; set; }
}