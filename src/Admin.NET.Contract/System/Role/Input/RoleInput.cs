﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 角色参数
/// </summary>
public class RoleInput : IDto
{
    /// <summary>
    /// 角色类型-集团角色_0、加盟商角色_1、门店角色_2
    /// </summary>
    public RoleTypeEnum RoleType { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    public string? Code { get; set; }
}
