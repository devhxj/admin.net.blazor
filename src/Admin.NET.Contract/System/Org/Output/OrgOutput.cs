﻿using System.Text.Json.Serialization;
namespace Admin.NET.Contract.System;

/// <summary>
/// 组织机构参数
/// </summary>
public class OrgOutput : BaseId<long>
{
    [DisplayName("机构类型")]
    [Required]
    public string? OrgType { get; set; }

    [DisplayName("机构类型")]
    [Required]
    [JsonIgnore]
    public OrgTypeEnum? OrgTypeWrp
    {
        get
        {
            if (string.IsNullOrWhiteSpace(OrgType))
            {
                return null;
            }
            if (Enum.TryParse<OrgTypeEnum>(OrgType, out var result))
            {
                return result;
            }

            return null;
        }
        set => OrgType = value.ToString();
    }

    /// <summary>
    /// 父Id
    /// </summary>
    [DisplayName("上级机构")]
    [Required]
    public long Pid { get; set; }

    /// <summary>
    /// 父Ids
    /// </summary>
    [DisplayName("父Ids")]
    public string? Pids { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    [DisplayName("机构名称")]
    [Required]
    public string? Name { get; set; }

    /// <summary>
    /// 编码
    /// </summary>
    [DisplayName("唯一编码")]
    [Required]
    public string? Code { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    [DisplayName("电话")]
    public string? Tel { get; set; }

    /// <summary>
    /// 排序
    /// </summary>
    [DisplayName("排序")]
    public int Sort { get; set; }

    /// <summary>
    /// 备注
    /// </summary>
    [DisplayName("备注")]
    public string? Remark { get; set; }

    /// <summary>
    /// 状态（字典 0正常 1停用 2删除）
    /// </summary>
    [DisplayName("状态")]
    public int Status { get; set; }
}
