﻿namespace Admin.NET.Contract.System;

public class OrgPageInput : PageInputBase, IDto
{
    /// <summary>
    /// 机构类型-品牌_1、总店(加盟/直营)_2、直营店_3、加盟店_4
    /// </summary>
    public string? OrgType { get; set; }

    public long? Id { get; set; }

    /// <summary>
    /// 父Id
    /// </summary>
    public long? Pid { get; set; }

    /// <summary>
    /// 名称
    /// </summary>
    public string? Name { get; set; }
}