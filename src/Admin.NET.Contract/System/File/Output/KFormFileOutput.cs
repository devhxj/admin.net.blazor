﻿namespace Admin.NET.Contract.System;

/// <summary>
/// k-form-design所需参数要求
/// </summary>
public class KFormFileOutput
{
    public int Code { get; set; }
    public KFormFileIrem Data { get; set; } = new();
}
