﻿namespace Admin.NET.Contract.System;

public class KFormFileIrem
{
    /// <summary>
    /// 文件ID
    /// </summary>
    public long FileId { get; set; }

    /// <summary>
    /// 文件下载地址
    /// </summary>
    public string? Url { get; set; }
}