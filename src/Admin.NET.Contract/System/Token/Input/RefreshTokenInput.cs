﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 
/// </summary>
public class RefreshTokenInput : IDto
{
    [Required(ErrorMessage = "Token不能为空")]
    public string? AccessToken { get; set; }


    [Required(ErrorMessage = "RefreshToken不能为空")]
    public string? RefreshToken { get; set; }
}