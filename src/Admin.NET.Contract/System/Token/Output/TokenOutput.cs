﻿namespace Admin.NET.Contract.System;

public class TokenOutput
{
    public required string AccessToken { get; set; }
    public required string RefreshToken { get; set; }
    public DateTime RefreshTokenExpiryTime { get; set; }
}
