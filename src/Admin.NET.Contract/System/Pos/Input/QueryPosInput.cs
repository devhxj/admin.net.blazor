﻿namespace Admin.NET.Contract.System;

public class QueryPosInput : BaseId<long>
{
    /// <summary>
    /// 编码
    /// </summary>
    [DisplayName("编码")]
    public string? Code { get; set; }
}