﻿namespace Admin.NET.Contract.System;
/// <summary>
/// 服务器资源信息
/// </summary>
public class MachineUseOutput : IDto
{
    [DisplayName("总内存")]
    public string? TotalRam { get; set; }

    [DisplayName("内存使用率")]
    public double RamRate { get; set; }
    [DisplayName("cpu使用率")]
    public double CpuRate { get; set; }
    [DisplayName("运行时间")]
    public string? RunTime { get; set; }
}
