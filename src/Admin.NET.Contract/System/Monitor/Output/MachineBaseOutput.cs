﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 服务器基本参数
/// </summary>
public class MachineBaseOutput : IDto
{
    [DisplayName("总内存")]
    public string? TotalRam { get; set; }

    [DisplayName("内存使用率")]
    public double RamRate { get; set; }
    [DisplayName("cpu使用率")]
    public double CpuRate { get; set; }
    [DisplayName("运行时间")]
    public string? RunTime { get; set; }
    [DisplayName("外网IP")]
    public string? WanIp { get; set; }
    [DisplayName("上下行流量统计")]
    public string? SendAndReceived { get; set; }
    [DisplayName("局域网IP")]
    public string? LanIp { get; set; }
    [DisplayName("Mac地址")]
    public string? IpMac { get; set; }
    [DisplayName("HostName")]
    public string? HostName { get; set; }
    [DisplayName("系统名称")]
    public string? SystemOs { get; set; }
    [DisplayName("系统架构")]
    public string? OsArchitecture { get; set; }
    [DisplayName("CPU核心数")]
    public string? ProcessorCount { get; set; }
    [DisplayName(".NET和Furion版本")]
    public string? FrameworkDescription { get; set; }
    [DisplayName("网络速度")]
    public string? NetworkSpeed { get; set; }
}
