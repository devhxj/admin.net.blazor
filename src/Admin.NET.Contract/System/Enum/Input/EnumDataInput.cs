﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 枚举输入参数
/// </summary>
public class EnumDataInput : BaseId<long>
{
    /// <summary>
    /// 枚举类型名称
    /// </summary>
    [Required(ErrorMessage = "枚举类型不能为空")]
    public string? EnumName { get; set; }
}
