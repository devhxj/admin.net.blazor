﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 枚举输出参数
/// </summary>
public class EnumDataOutput : BaseId<long>
{
    /// <summary>
    /// 字典Id
    /// </summary>
    public int Code { get; set; }

    /// <summary>
    /// 字典值
    /// </summary>
    public string? Value { get; set; }
}