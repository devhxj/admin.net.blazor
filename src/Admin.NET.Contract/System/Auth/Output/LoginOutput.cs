﻿namespace Admin.NET.Contract.System;

/// <summary>
/// 用户登录输出参数
/// </summary>
public class LoginOutput : BaseId<long>
{
    /// <summary>
    /// 账号
    /// </summary>
    [DisplayName("账号")]
    [Required]
    public string? Account { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    [DisplayName("昵称")]
    public string? NickName { get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    [DisplayName("姓名")]
    [Required]
    public string? Name { get; set; }

    /// <summary>
    /// 头像
    /// </summary>
    [DisplayName("头像")]
    public string? Avatar { get; set; }

    /// <summary>
    /// 生日
    /// </summary>
    [DisplayName("生日")]
    [Required]
    public DateTimeOffset? Birthday { get; set; }

    /// <summary>
    /// 性别(字典 1男 2女)
    /// </summary>
    [DisplayName("性别")]
    [Required]
    public Sex Sex { get; set; }

    /// <summary>
    /// 邮箱
    /// </summary>
    [DisplayName("邮箱")]
    [Required]
    public string? Email { get; set; }

    /// <summary>
    /// 手机
    /// </summary>
    [DisplayName("手机")]
    [Required]
    public string? Phone { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    [DisplayName("电话")]
    public string? Tel { get; set; }

    /// <summary>
    /// 管理员类型（1超级管理员 2管理员 3普通账号）
    /// </summary>
    [DisplayName("管理员类型")]
    public int AdminType { get; set; }

    /// <summary>
    /// 最后登陆IP
    /// </summary>
    [DisplayName("最后登陆IP")]
    public string? LastLoginIp { get; set; }

    /// <summary>
    /// 最后登陆时间
    /// </summary>
    [DisplayName("最后登录时间")]
    public DateTimeOffset? LastLoginTime { get; set; }

    /// <summary>
    /// 最后登陆地址
    /// </summary>
    [DisplayName("最后登录地址")]
    public string? LastLoginAddress { get; set; }

    /// <summary>
    /// 最后登陆所用浏览器
    /// </summary>
    [DisplayName("最后登录所用浏览器")]
    public string? LastLoginBrowser { get; set; }

    /// <summary>
    /// 最后登陆所用系统
    /// </summary>
    [DisplayName("最后登录所用系统")]
    public string? LastLoginOs { get; set; }

    public EmpOutput LoginEmpInfo { get; set; } = new();

    /// <summary>
    /// 角色信息
    /// </summary>
    [DisplayName("角色信息")]
    public List<RoleOutput>? Roles { get; set; }

    /// <summary>
    /// 权限信息
    /// </summary>
    [DisplayName("权限信息")]
    public List<string>? Permissions { get; set; }

    /// <summary>
    /// 系统所有权限信息
    /// </summary>
    [DisplayName("系统所有权限信息")]
    public List<string>? AllPermissions { get; set; }

    /// <summary>
    /// 具备应用信息
    /// </summary>
    [DisplayName("具备应用信息")]
    public List<AppNavNodeOutput>? Apps { get; set; }

    /// <summary>
    /// 登录菜单信息
    /// </summary>
    [DisplayName("登录菜单信息")]
    public List<AppNavNodeOutput>? Navs { get; set; }

    /// <summary>
    /// 数据范围（机构）信息
    /// </summary>
    [DisplayName("数据范围")]
    public List<long>? DataScopes { get; set; }
}
