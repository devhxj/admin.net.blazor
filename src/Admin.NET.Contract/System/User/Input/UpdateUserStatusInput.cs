﻿namespace Admin.NET.Contract.System;

public class UpdateUserStatusInput : BaseId<long>
{
    /// <summary>
    /// 状态-正常_0、停用_1、删除_2
    /// </summary>
    public CommonStatus Status { get; set; }
}
