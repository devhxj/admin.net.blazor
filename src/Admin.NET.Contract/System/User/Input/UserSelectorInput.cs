﻿namespace Admin.NET.Contract.System;

public class UserSelectorInput : IDto
{
    public string? Name { get; set; }
}
