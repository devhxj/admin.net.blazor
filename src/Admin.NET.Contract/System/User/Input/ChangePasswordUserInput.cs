﻿namespace Admin.NET.Contract.System;

public class ChangePasswordUserInput : BaseId<long>
{
    /// <summary>
    /// 密码
    /// </summary>
    [Required(ErrorMessage = "旧密码不能为空")]
    public string? Password { get; set; }

    /// <summary>
    /// 新密码
    /// </summary>
    [Required(ErrorMessage = "新密码不能为空")]
    [StringLength(32, MinimumLength = 5, ErrorMessage = "密码需要大于5个字符")]
    public string? NewPassword { get; set; }
}
