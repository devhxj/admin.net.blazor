﻿namespace Admin.NET.Contract.System;

public class UpdateUserInput : BaseId<long>
{
    /// <summary>
    /// 账号
    /// </summary>
    [Required(ErrorMessage = "账号名称不能为空")]
    public string? Account { get; set; }

    /// <summary>
    /// 昵称
    /// </summary>
    [Required(ErrorMessage = "昵称不能为空")]
    public string? NickName { get; set; }

    /// <summary>
    /// 姓名
    /// </summary>
    [Required(ErrorMessage = "姓名不能为空")]
    public string? Name { get; set; }

    /// <summary>
    /// 生日
    /// </summary>
    public DateTime? Birthday { get; set; }

    /// <summary>
    /// 性别-男_1、女_2
    /// </summary>
    public int Sex { get; set; }

    /// <summary>
    /// 邮箱
    /// </summary>
    [Required(ErrorMessage = "邮箱不能为空")]
    public string? Email { get; set; }

    /// <summary>
    /// 手机
    /// </summary>
    [Required(ErrorMessage = "手机不能为空")]
    public string? Phone { get; set; }

    /// <summary>
    /// 电话
    /// </summary>
    public string? Tel { get; set; }

    /// <summary>
    /// 员工信息
    /// </summary>
    public EmpOutput2 SysEmpParam { get; set; } = new EmpOutput2();
}