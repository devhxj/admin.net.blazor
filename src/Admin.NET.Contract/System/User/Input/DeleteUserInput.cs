﻿namespace Admin.NET.Contract.System;

public class DeleteUserInput : BaseId<long>
{
    /// <summary>
    /// 员工信息
    /// </summary>
    public EmpOutput2 SysEmpParam { get; set; } = new EmpOutput2();
}
