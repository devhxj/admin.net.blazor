﻿namespace Admin.NET.Contract.System;
/// <summary>
/// 系统通知公告详情参数
/// </summary>
public class NoticeDetailOutput : NoticeBase
{
    /// <summary>
    /// 通知到的用户Id集合
    /// </summary>
    public List<string> NoticeUserIdList { get; set; } = new();

    /// <summary>
    /// 通知到的用户阅读信息集合
    /// </summary>
    public List<NoticeUserRead> NoticeUserReadInfoList { get; set; } = new();
}
