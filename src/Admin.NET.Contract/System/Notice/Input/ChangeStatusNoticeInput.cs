﻿namespace Admin.NET.Contract.System;

public class ChangeStatusNoticeInput : BaseId<long>
{
    /// <summary>
    /// 状态（字典 0草稿 1发布 2撤回 3删除）
    /// </summary>
    [Required(ErrorMessage = "状态不能为空")]
    public NoticeStatus Status { get; set; }
}