﻿namespace Admin.NET.Contract;

/// <summary>
/// 登录类型
/// </summary>
public enum LoginType
{
    /// <summary>
    /// 登陆
    /// </summary>
    [Description("登录")]
    LOGIN = 0,

    /// <summary>
    /// 登出
    /// </summary>
    [Description("登出")]
    LOGOUT = 1,

    /// <summary>
    /// 注册
    /// </summary>
    [Description("注册")]
    REGISTER = 2,

    /// <summary>
    /// 改密
    /// </summary>
    [Description("改密")]
    CHANGEPASSWORD = 3,

    /// <summary>
    /// 三方授权登陆
    /// </summary>
    [Description("授权登录")]
    AUTHORIZEDLOGIN = 4
}
