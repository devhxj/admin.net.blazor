﻿namespace Admin.NET.Contract;

/// <summary>
/// 角色类型
/// </summary>
public enum RoleTypeEnum
{
    /// <summary>
    /// 不限
    /// </summary>
    [Description("不限")]
    NoNE = 0,

    /// <summary>
    /// 集团角色
    /// </summary>
    [Description("集团角色")]
    GROUP = 1,

    /// <summary>
    /// 加盟角色
    /// </summary>
    [Description("加盟角色")]
    JOIN = 2,

    /// <summary>
    /// 门店角色
    /// </summary>
    [Description("门店角色")]
    STORE = 3
}
